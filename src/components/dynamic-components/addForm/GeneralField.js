import React, { useEffect, useState } from "react";
import { Input, Card, Form, DatePicker, Select, Checkbox, Button, Modal, notification } from "antd";
import { useIntl } from "react-intl";
import InputNumber from "components/bananacode-components/InputNumber";
import { getDestinyCombo, getOriginCombo, getExpenseCombo, getRenta, getIVA, getCCSS, trxInsert } from 'redux/actions/trx';
import { connect } from 'react-redux';
import moment from 'moment';
import { Link } from 'react-router-dom'
const rules = {
  amount: [
    {
      required: true,
      message: "profesional.services.input.number",
    },
  ],
  description: [
    {
      required: true,
      message: "Por favor ingrese una descripción",
    },
  ],
  date: [
    {
      required: true,
      message: "Por favor ingrese una fecha",
    },
  ],
  origin_msg: [
    {
      required: true,
      message: "Por favor seleccione una fuente a afectar",
    },
  ],
  destiny_origin: [
    {
      required: true,
      message: "Por favor seleccione una fuente a afectar",
    },
  ],
  category: [
    {
      required: true,
      message: "Por favor seleccione una categoria",
    },
  ],
  frequency: [
    {
      required: false,
      message: "Por favor seleccione la frecuencia",
    },
  ],
  bill: [
    {
      required: false,
      message: "Por favor ingrese el numero de factura/clave",
    },
  ],
  IVA: [
    {
      required: false,
      message: "Por favor ingrese si aplica IVA",
    },
  ],
  supplier_customer: [
    {
      required: true,
      message: "Por favor ingrese el dato solicitado",
    },
  ]/*,
  state: [
    {
      required: true,
      message: "Por favor ingrese un estado",
    },
  ],*/
};
const dateFormat = "YYYY-MM-DD";
const { Option } = Select;
const GeneralField = (props) => {
  const { getDestinyCombo, getOriginCombo, profile, getExpenseCombo, getRenta, renta, getIVA, iva, getCCSS, ccss, data, editable, values, trxInsert } = props
  const [newForm] = Form.useForm();
  const [visible, setVisible] = useState(false);
  const [buttomEnable, setButtonEnable] = useState(true);
  const intl = useIntl();
  const formatRules = (specificRules) => {
    const newRules = specificRules.map((rule) => {
      return {
        required: rule.required,
        message: intl.formatMessage({ id: rule.message }),
      };
    });
    return newRules;
  };
  useEffect(() => {
    switch (props.viewtype) {
      case "Revenue":
      case "RevenuePSB":
        getDestinyCombo({
          combo_type: 'destiny',
          profile_id: profile
        });
        getOriginCombo({
          combo_type: 'origin',
          profile_id: profile
        })
        break;
      case "Expenses":
        getDestinyCombo({
          combo_type: 'destiny',
          profile_id: profile
        });
        getExpenseCombo({
          combo_type: 'Expense',
        })
        break;
      case "COGS":
      case "SG&A":
      case "CAPEX":
      case "Taxation":
        getDestinyCombo({
          combo_type: 'destiny',
          user: 8,
          profile_id: profile
        });
        break;
      default:
        return <h1>Valor invalido</h1>;
    }
  }, []);
  let destinyData = [];
  destinyData = props.destinyCat;
  let originData = [];
  originData = props.originCat;
  let expenseCat = [];
  expenseCat = props.expenseCombo;
  let title;
  props.editable ? title = 'Editar' : title = 'Agregar'
  let [showIVA, setshowIVA] = useState(true);
  function onChange(e) {
    if (e.target.checked) {
      setshowIVA(showIVA = false);
    } else {
      setshowIVA(showIVA = true)
    }
  }
  const sendSGA = () => {
    let formInfor = newForm.getFieldsValue()
    let newDate = moment(formInfor.trx_date).format('YYYY-MM-DD')
    formInfor['trx_date'] = newDate;
    formInfor['transaction_type'] = 'SG&A';
    formInfor['profile_id'] = profile;
    formInfor['exp_type'] = 3;
    trxInsert(formInfor);
  };
  function FormAlineacion() {
    return <>
      <Form
        layout="vertical"
        form={newForm}
        name="SGA-FORM"
        onFinish={() => sendSGA}
      >
        <Card title={title + " Gastos de venta, generales y administrativos (SG&A)"}>
          <div className="ant-row">
            <div className="ant-col ant-col-6">
              <Form.Item name="amount" label="Monto" rules={rules.amount}>
                <InputNumber min="1" style={{ width: "75%" }}></InputNumber>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item name="trx_date" label="Fecha de pago" rules={rules.date}>
                <DatePicker
                  style={{ width: "75%" }}
                  format={dateFormat}
                ></DatePicker>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item
                name="destination_id"
                label="Fuente del egreso"
                rules={rules.destiny_origin}
              >
                <Select style={{ width: "75%" }}>
                  (destinyData.length === 0) ?
                  <Option key={1} value={1}>{<Link to={'/dashboards/settings/categories'}>Agrega categorias</Link>}</Option> :
                  {destinyData.map((options) => {
                    return <Option key={options.id} value={options.id}>{options.description}</Option>
                  })}
                </Select>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item
                name="supplier"
                label="Proveedor"
                rules={rules.supplier_customer}
              >
                <Input style={{ width: "75%" }} />
              </Form.Item>
            </div>
          </div>
          <div className="ant-row">
            <div className="ant-col ant-col-24">
              <Form.Item
                name="description"
                label="Descripción"
                rules={rules.description}
              >
                <Input.TextArea rows={4} />
              </Form.Item>
            </div>
          </div>
          <Button
            type="primary"
            onClick={sendSGA}
          > Agregar movimiento </Button>
        </Card>
      </Form>
    </>
  }
  function actiongetRenta() {
    let values = {};
    const start_date = document.getElementById('rentaSdate').value;
    const end_date = document.getElementById('rentaEdate').value;
    values['profile_id'] = profile;
    values['start_date'] = start_date;
    values['end_date'] = end_date;
    if (start_date === '' || end_date === '') {
      notification['error']({
        message: 'Notificación',
        description: 'Las fechas son requeridas',
      });

    } else if (new Date(end_date) < new Date(start_date)) {
      notification['error']({
        message: 'Notificación',
        description: 'La fecha inicial debe ser previa a la fecha final',
      });
    } else {
      getRenta(values);
      setButtonEnable(false)
    }
  }
  function actiongetIVA() {
    let values = {};
    const start_date = document.getElementById('IVAInicio').value;
    const end_date = document.getElementById('IVAFin').value;
    values['profile_id'] = profile;
    values['start_date'] = start_date;
    values['end_date'] = end_date;
    if (start_date === '' || end_date === '') {
      notification['error']({
        message: 'Notificación',
        description: 'Las fechas son requeridas',
      });
    } else if (new Date(end_date) < new Date(start_date)) {
      notification['error']({
        message: 'Notificación',
        description: 'La fecha inicial debe ser previa a la fecha final',
      });
    } else {
      getIVA(values);
      setButtonEnable(false)
    }
  }
  function actiongetCCSS() {
    let values = {};
    const start_date = document.getElementById('CCSSInicio').value;
    const end_date = document.getElementById('CCSSFinal').value;
    values['profile_id'] = profile;
    values['start_date'] = start_date;
    values['end_date'] = end_date;
    if (start_date === '' || end_date === '') {
      notification['error']({
        message: 'Notificación',
        description: 'Las fechas son requeridas',
      });
    } else if (new Date(end_date) < new Date(start_date)) {
      notification['error']({
        message: 'Notificación',
        description: 'La fecha inicial debe ser previa a la fecha final',
      });
    } else {
      getCCSS(values)
      setButtonEnable(false)
    }
    ;
  }
  switch (props.viewtype) {
    case "Revenue":
      return (
        <Card title={title + " ingreso"}>
          <div className="ant-row">
            <div className="ant-col ant-col-6">
              <Form.Item
                name="amount"
                label="Monto"
                rules={formatRules(rules.amount)}
              >
                <InputNumber min="1" style={{ width: "75%" }}></InputNumber>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item
                name="trx_date"
                label="Fecha de ingreso"
                rules={rules.date}
              >
                <DatePicker
                  style={{ width: "75%" }}
                  format={dateFormat}
                ></DatePicker>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item
                name="origin_id"
                label="Origen de ingreso"
                rules={rules.origin_msg}
              >
                <Select style={{ width: "75%" }}>
                  (originData.length === 0) ?
                  <Option key={0} value={0}>{<Link to={'/dashboards/settings/categories'}>Agrega categorias</Link>}</Option> :
                  {originData.map((options) => {
                    return <Option key={options.id} value={options.id}>{options.description}</Option>
                  })}
                </Select>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item
                name="destination_id"
                label="Destino del ingreso"
                rules={rules.destiny_origin}
              >
                <Select style={{ width: "75%" }}>
                  (destinyData.length === 0) ?
                  <Option key={0} value={0}>{<Link to={'/dashboards/settings/categories'}>Agrega categorias</Link>}</Option> :
                  {destinyData.map((options) => {
                    return <Option key={options.id} value={options.id}>{options.description}</Option>
                  })}
                </Select>
              </Form.Item>
            </div>
          </div>
          <div className="ant-row">
            <div className="ant-col ant-col-24">
              <Form.Item
                name="description"
                label="Descripción"
                rules={rules.description}
              >
                <Input.TextArea rows={4} />
              </Form.Item>
            </div>
          </div>
        </Card>
      );
    case "Expenses":
      return (
        <Card title={title + " egreso"}>
          <div className="ant-row">
            <div className="ant-col ant-col-6">
              <Form.Item name="amount" label="Monto" rules={rules.amount}>
                <InputNumber min="1" style={{ width: "75%" }}></InputNumber>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item
                name="trx_date"
                label="Fecha de egreso"
                rules={rules.date}
              >
                <DatePicker
                  style={{ width: "75%" }}
                  format={dateFormat}
                ></DatePicker>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item
                name="destination_id"
                label="Fuente a afectar"
                rules={rules.destiny_origin}
              >
                <Select style={{ width: "75%" }}>
                  (destinyData.length === 0) ?
                  <Option key={0} value={0}>{<Link to={'/dashboards/settings/categories'}>Agrega categorias</Link>}</Option> :
                  {destinyData.map((options) => {
                    return <Option key={options.id} value={options.id}>{options.description}</Option>
                  })}
                </Select>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item
                name="id_exp_cat"
                label="Categoria del egreso"
                rules={rules.category}
              >
                <Select style={{ width: "75%" }}>
                  (expenseCat.length === 0) ?
                  <Option key={0} value={0}>{<Link to={'/dashboards/settings/categories'}>Agrega categorias</Link>}</Option> :
                  {expenseCat.map((options) => {
                    return <Option key={options.id} value={options.id}>{options.description}</Option>
                  })}
                </Select>
              </Form.Item>
            </div>
          </div>
          <div className="ant-row">
            <div className="ant-col ant-col-24">
              <Form.Item
                name="description"
                label="Descripción"
                rules={rules.description}
              >
                <Input.TextArea rows={4} />
              </Form.Item>
            </div>
          </div>
        </Card>
      );
    case "RevenuePSB":
      return (
        <Card title={title + " ingreso"}>
          <div className="ant-row">
            <div className="ant-col ant-col-6">
              <Form.Item name="amount" label="Monto" rules={rules.amount}>
                <InputNumber min="1" style={{ width: "75%" }}></InputNumber>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item
                name="trx_date"
                label="Fecha de ingreso"
                rules={rules.date}
              >
                <DatePicker
                  style={{ width: "75%" }}
                  format={dateFormat}
                ></DatePicker>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item
                name="origin_id"
                label="Origen de ingreso"
                rules={rules.destiny_origin}
              >
                <Select style={{ width: "75%" }}>
                  (originData.length === 0) ?
                  <Option key={1} value={1}>{<Link to={'/dashboards/settings/categories'}>Agrega categorias</Link>}</Option> :
                  {originData.map((options) => {
                    return <Option key={options.id} value={options.id}>{options.description}</Option>
                  })}
                </Select>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item
                name="destination_id"
                label="Destino del ingreso"
                rules={rules.destiny_origin}
              >
                <Select style={{ width: "75%" }}>
                  (destinyData.length === 0) ?
                  <Option key={1} value={1}>{<Link to={'/dashboards/settings/categories'}>Agrega categorias</Link>}</Option> :
                  {destinyData.map((options) => {
                    return <Option key={options.id} value={options.id}>{options.description}</Option>
                  })}
                </Select>
              </Form.Item>
            </div>
          </div>
          <div className="ant-row">
            <div className="ant-col ant-col-24">
              <Form.Item
                name="description"
                label="Descripción"
                rules={rules.description}
              >
                <Input.TextArea rows={4} />
              </Form.Item>
            </div>
          </div>
          <div className="ant-row">
            <div className="ant-col ant-col-6">
              <Checkbox onChange={onChange}>¿Aplica IVA?</Checkbox> 
              <br></br><br></br>
              <Form.Item
                hidden={showIVA}
                name="iva_percentage"
              >
                <Select style={{ width: "75%" }}>
                  <Option key={1} value={1}> 1% </Option>
                  <Option key={2} value={2}> 2%</Option>
                  <Option key={4} value={4}> 4% </Option>
                  <Option key={8} value={8}>8% </Option>
                  <Option key={13} value={13}> 13% </Option>
                </Select>
              </Form.Item>
            </div>
          </div>
        </Card>
      );
    case "COGS":
      return (
        <Card title={title + " costos de los bienes vendidos (COGS)"}>
          <div className="ant-row">
            <div className="ant-col ant-col-6">
              <Form.Item name="amount" label="Monto" rules={rules.amount}>
                <InputNumber min="1" style={{ width: "75%" }}></InputNumber>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item name="trx_date" label="Fecha de pago" rules={rules.date}>
                <DatePicker
                  style={{ width: "75%" }}
                  format={dateFormat}
                ></DatePicker>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item
                name="destination_id"
                label="Fuente del egreso"
                rules={rules.destiny_origin}
              >
                <Select style={{ width: "75%" }}>
                  (destinyData.length === 0) ?
                  <Option key={1} value={1}>{<Link to={'/dashboards/settings/categories'}>Agrega categorias</Link>}</Option> :
                  {destinyData.map((options) => {
                    return <Option key={options.id} value={options.id}>{options.description}</Option>
                  })}
                </Select>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item
                name="supplier"
                label="Proveedor"
                rules={rules.supplier_customer}
              >
                <Input style={{ width: "75%" }} />
              </Form.Item>
            </div>
          </div>
          <div className="ant-row">
            <div className="ant-col ant-col-24">
              <Form.Item
                name="description"
                label="Descripción"
                rules={rules.description}
              >
                <Input.TextArea rows={4} />
              </Form.Item>
            </div>
          </div>
          <Form.Item
            name="bill_number"
            label="Numero de clave (factura)"
            rules={rules.bill}
          >
            <Input></Input>
          </Form.Item>
          <div className="ant-col ant-col-6">
            <Checkbox onChange={onChange}>¿Aplica IVA?</Checkbox>
            <br></br><br></br>
            <Form.Item
              hidden={showIVA}
              name="iva_percentage"
            >
              <Select style={{ width: "75%" }}>
                <Option key={1} value={1}> 1% </Option>
                <Option key={2} value={2}> 2%</Option>
                <Option key={4} value={4}> 4% </Option>
                <Option key={8} value={8}>8% </Option>
                <Option key={13} value={13}> 13% </Option>
              </Select>
            </Form.Item>
          </div>
        </Card>
      );
    case "SG&A":
      return <>
        <Card title={title + " gastos de venta, generales y administrativos (SG&A)"}>
          <div className="ant-row">
            <div className="ant-col ant-col-6">
              <Form.Item name="amount" label="Monto" rules={rules.amount}>
                <InputNumber min="1" style={{ width: "75%" }}></InputNumber>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item name="trx_date" label="Fecha de pago" rules={rules.date}>
                <DatePicker
                  style={{ width: "75%" }}
                  format={dateFormat}
                ></DatePicker>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item
                name="destination_id"
                label="Fuente del egreso"
                rules={rules.destiny_origin}
              >
                <Select style={{ width: "75%" }}>
                  (destinyData.length === 0) ?
                  <Option key={1} value={1}>{<Link to={'/dashboards/settings/categories'}>Agrega categorias</Link>}</Option> :
                  {destinyData.map((options) => {
                    return <Option key={options.id} value={options.id}>{options.description}</Option>
                  })}
                </Select>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item
                name="supplier"
                label="Proveedor"
                rules={rules.supplier_customer}
              >
                <Input style={{ width: "75%" }} />
              </Form.Item>
            </div>
          </div>
          <div className="ant-row">
            <div className="ant-col ant-col-24">
              <Form.Item
                name="description"
                label="Descripción"
                rules={rules.description}
              >
                <Input.TextArea rows={4} />
              </Form.Item>
            </div>
          </div>
          <Form.Item
            name="bill_number"
            label="Numero de clave (factura)"
            rules={rules.bill}
          >
            <Input></Input>
          </Form.Item>
          <div className="ant-col ant-col-6">
            <Checkbox onChange={onChange}>¿Aplica IVA?</Checkbox>
            <br></br><br></br>
            <Form.Item
              hidden={showIVA}
              name="iva_percentage"
            >
              <Select style={{ width: "75%" }}>
                <Option key={1} value={1}> 1% </Option>
                <Option key={2} value={2}> 2%</Option>
                <Option key={4} value={4}> 4% </Option>
                <Option key={8} value={8}>8% </Option>
                <Option key={13} value={13}> 13% </Option>
              </Select>
            </Form.Item>
          </div>
        </Card>
      </>
    case "CAPEX":
      return (
        <Card title={title + " gastos al capital (CAPEX)"}>
          <div className="ant-row">
            <div className="ant-col ant-col-6">
              <Form.Item name="amount" label="Monto" rules={rules.amount}>
                <InputNumber min="1" style={{ width: "75%" }}></InputNumber>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item name="trx_date" label="Fecha de pago" rules={rules.date}>
                <DatePicker
                  style={{ width: "75%" }}
                  format={dateFormat}
                ></DatePicker>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item
                name="destination_id"
                label="Fuente del egreso"
                rules={rules.destiny_origin}
              >
                <Select style={{ width: "75%" }}>
                  (destinyData.length === 0) ?
                  <Option key={1} value={1}>{<Link to={'/dashboards/settings/categories'}>Agrega categorias</Link>}</Option> :
                  {destinyData.map((options) => {
                    return <Option key={options.id} value={options.id}>{options.description}</Option>
                  })}
                </Select>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item
                name="supplier"
                label="Proveedor"
                rules={rules.supplier_customer}
              >
                <Input style={{ width: "75%" }} />
              </Form.Item>
            </div>
          </div>
          <div className="ant-row">
            <div className="ant-col ant-col-24">
              <Form.Item
                name="description"
                label="Descripción"
                rules={rules.description}
              >
                <Input.TextArea rows={4} />
              </Form.Item>
            </div>
          </div>
          <Form.Item
            name="bill_number"
            label="Numero de clave (factura)"
            rules={rules.bill}
          >
            <Input></Input>
          </Form.Item>
          <div className="ant-col ant-col-6">
            <Checkbox onChange={onChange}>¿Aplica IVA?</Checkbox>
            <br></br><br></br>
            <Form.Item
              hidden={showIVA}
              name="iva_percentage"
            >
              <Select style={{ width: "75%" }}>
                <Option key={1} value={1}> 1% </Option>
                <Option key={2} value={2}> 2%</Option>
                <Option key={4} value={4}> 4% </Option>
                <Option key={8} value={8}>8% </Option>
                <Option key={13} value={13}> 13% </Option>
              </Select>
            </Form.Item>
          </div>
        </Card>
      );
    case "CXC":
      return (
        <Card title={title + " cuentas por cobrar (CXC)"}>
          <div className="ant-row">
            <div className="ant-col ant-col-6">
              <Form.Item name="amount" label="Monto" rules={rules.amount}>
                <InputNumber min="1" style={{ width: "75%" }}></InputNumber>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item
                name="client_supplier"
                label="Cliente"
                rules={rules.supplier_customer}
              >
                <Input style={{ width: "75%" }} />
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item name="trx_date" label="Fecha de cobro" rules={rules.date}>
                <DatePicker
                  style={{ width: "75%" }}
                  format={dateFormat}
                ></DatePicker>
              </Form.Item>
            </div>
          </div>
          <div className="ant-row">
            <div className="ant-col ant-col-24">
              <Form.Item
                name="description"
                label="Descripción"
                rules={rules.description}
              >
                <Input.TextArea rows={4} />
              </Form.Item>
            </div>
          </div>
        </Card>
      );
    case "CXP":
      return (
        <Card title={title + " cuentas por pagar (CXP)"}>
          <div className="ant-row">
            <div className="ant-col ant-col-6">
              <h1>{props.viewtype.parametro2}</h1>
              <Form.Item name="amount" label="Monto" rules={rules.amount}>
                <InputNumber min="1" style={{ width: "75%" }}></InputNumber>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item
                name="client_supplier"
                label="Proveedor"
                rules={rules.supplier_customer}
              >
                <Input style={{ width: "75%" }} />
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item name="trx_date" label="Fecha de cobro" rules={rules.date}>
                <DatePicker
                  style={{ width: "75%" }}
                  format={dateFormat}
                ></DatePicker>
              </Form.Item>
            </div>
          </div>
          <div className="ant-row">
            <div className="ant-col ant-col-24">
              <Form.Item
                name="description"
                label="Descripción"
                rules={rules.description}
              >
                <Input.TextArea rows={4} />
              </Form.Item>
            </div>
          </div>
        </Card>
      );
    case "Payroll":
      let children = [];
      for (let i = 0; i < data.length; i++) {
        children.push(<Option key={data[i].collaborator_id} value={data[i].collaborator_id} >{data[i].collab_name}</Option>)
      }
      return (
        <Card title={title + " planilla"}>
          <div className="ant-row">
            <div className="ant-col ant-col-8">
              <Form.Item name="amount" label="Monto" rules={rules.amount} initialValue={editable && values.amount}>
                <InputNumber
                  min="1"
                  style={{ width: "75%" }}
                ></InputNumber>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-8">
              <Form.Item
                name={editable ? "collaborator_id" : "collaborators"}
                label="Colaborador"
                rules={rules.supplier_customer}
                initialValue={editable && values.collaborator_id}
              >
                <Select
                  mode={!editable && "multiple"}
                  style={{ width: "75%" }}
                >
                  {children}
                </Select>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-8">
              <Form.Item name="trx_date" label="Fecha" rules={rules.date} initialValue={editable && moment(values.trx_date)}>
                <DatePicker
                  style={{ width: "75%" }}
                  format={dateFormat}
                ></DatePicker>
              </Form.Item>
            </div>
          </div>
        </Card>
      );
    case "Taxation":
      const showModalRenta = () => {
        setVisible(true)
        newForm.setFieldsValue({
          amount: renta.pagoRentaTotal,
          description: `Renta del Periodo ${document.getElementById('rentaSdate').value} al ${document.getElementById('rentaEdate').value}`,
          supplier: 'Hacienda RENTA'
        })
      }
      const showModalCCSS = () => {
        setVisible(true)
        newForm.setFieldsValue({
          amount: ccss.montoAPagar,
          description: `CCSS DEL PERIODO ${document.getElementById('CCSSInicio').value} al ${document.getElementById('CCSSFinal').value} `,
          supplier: 'CCSS'
        })
      }
      const showModalIVA = () => {
        setVisible(true)
        newForm.setFieldsValue({
          amount: iva.ivatotal,
          description: `IVA DEL PERIODO ${document.getElementById('IVAInicio').value} al ${document.getElementById('IVAFin').value} `,
          supplier: 'HACIENDA IVA'
        })
      }
      if (profile === 2) {
        return <>
          <Card title="Alineación tributaria y CCSS">
            <div className="ant-row">
              <div className="ant-col ant-col-8">
                <Button style={{ width: "75%" }} className="mr-2" type={'primary'} onClick={actiongetRenta}>
                  Calcular renta
                </Button>
                <DatePicker
                  style={{ width: "75%" }}
                  format={dateFormat}
                  placeholder={'Fecha de incio'}
                  name={'rentaSdate'}
                  id={'rentaSdate'}
                ></DatePicker>
                <DatePicker
                  style={{ width: "75%" }}
                  format={dateFormat}
                  placeholder={'Fecha final'}
                  name={'rentaEdate'}
                  id={'rentaEdate'}
                ></DatePicker>
                <Card style={{ width: "75%" }}>
                  <h4>Ingresos: ₡ {renta.rentaBruta} </h4>
                  <h4>Gastos: ₡ {renta.gastos} </h4>
                  <h4>Utilidades:  ₡{renta.Balance} </h4>
                  <h3>Monto de renta pagar: ₡ {renta.pagoRentaTotal} </h3>
                </Card>
                <Button style={{ width: "75%" }} className="mr-2" disabled={buttomEnable} onClick={showModalRenta} >
                  Agregar renta
                </Button>
              </div>
              <div className="ant-col ant-col-8">
                <Button style={{ width: "75%" }} className="mr-2" type={'primary'} onClick={actiongetIVA}>
                  Calcular IVA
                </Button>
                <DatePicker
                  style={{ width: "75%" }}
                  format={dateFormat}
                  placeholder={'Fecha de inicio'}
                  name={'IVAInicio'}
                  id={'IVAInicio'}
                ></DatePicker>
                <DatePicker
                  style={{ width: "75%" }}
                  format={dateFormat}
                  placeholder={'Fecha final'}
                  name={'IVAFin'}
                  id={'IVAFin'}
                ></DatePicker>
                <Card style={{ width: "75%" }}>
                  <h4>IVA por ingresos: ₡ {iva.aporteCreditos} </h4>
                  <h4>Descuento por IVA pagado: ₡ {iva.aporteDebitos} </h4>
                  <h3>Monto de IVA pagar: ₡ {iva.ivatotal} </h3>
                </Card>
                <Button style={{ width: "75%" }} className="mr-2" disabled={buttomEnable} onClick={showModalIVA}>
                  Agregar IVA
                </Button>
              </div>
              <div className="ant-col ant-col-8">
                <Button style={{ width: "75%" }} className="mr-2" type={'primary'} onClick={actiongetCCSS}>
                  Calcular CCSS
                </Button>
                <DatePicker
                  style={{ width: "75%" }}
                  format={dateFormat}
                  placeholder={'Fecha de inicio'}
                  name={'CCSSInicio'}
                  id={'CCSSInicio'}
                ></DatePicker>
                <DatePicker
                  style={{ width: "75%" }}
                  format={dateFormat}
                  placeholder={'Fecha final'}
                  name={'CCSSFinal'}
                  id={'CCSSFinal'}
                ></DatePicker>
                <Card style={{ width: "75%" }}>
                  <h3>Monto a pagar: {ccss.montoAPagar} </h3>
                </Card>
                <Button style={{ width: "75%" }} className="mr-2" disabled={buttomEnable} onClick={showModalCCSS}>
                  Agregar CCSS
                </Button>
              </div>
            </div>
          </Card>
          <Modal
            getContainer={false}
            visible={visible}
            width={1000}
            footer={null}
            forceRenderen={false}
            onCancel={() => setVisible(false)}
          >
            < FormAlineacion />
          </Modal>
        </>
      } else if (profile === 3) {
        return (
          <Card title="Alineación tributaria y CCSS">
            <div className="ant-row">
              <div className="ant-col ant-col-8">
                <Button style={{ width: "75%" }} className="mr-2" type={'primary'} onClick={actiongetRenta}>
                  Calcular renta
                </Button>
                <DatePicker
                  style={{ width: "75%" }}
                  format={dateFormat}
                  placeholder={'Fecha de incio'}
                  name={'rentaSdate'}
                  id={'rentaSdate'}
                ></DatePicker>
                <DatePicker
                  style={{ width: "75%" }}
                  format={dateFormat}
                  placeholder={'Fecha final'}
                  name={'rentaEdate'}
                  id={'rentaEdate'}
                ></DatePicker>
                <Card style={{ width: "75%" }}>
                  <h4>Ingresos: ₡ {renta.rentaBruta} </h4>
                  <h4>Gastos: ₡ {renta.gastos} </h4>
                  <h4>Utilidades:  ₡{renta.Balance} </h4>
                  <h3>Monto de Renta pagar: ₡ {renta.pagoRentaTotal} </h3>
                </Card>
                <Button style={{ width: "75%" }} className="mr-2">
                  Agregar renta
                </Button>
              </div>
              <div className="ant-col ant-col-8">
                <Button style={{ width: "75%" }} className="mr-2" type={'primary'} onClick={actiongetIVA}>
                  Calcular IVA
                </Button>
                <DatePicker
                  style={{ width: "75%" }}
                  format={dateFormat}
                  placeholder={'Fecha de inicio'}
                  name={'IVAInicio'}
                  id={'IVAInicio'}
                ></DatePicker>
                <DatePicker
                  style={{ width: "75%" }}
                  format={dateFormat}
                  placeholder={'Fecha final'}
                  name={'IVAFin'}
                  id={'IVAFin'}
                ></DatePicker>
                <Card style={{ width: "75%" }}>
                  <h4>IVA por ingresos: ₡ {iva.aporteCreditos} </h4>
                  <h4>Descuento por IVA pagado: ₡ {iva.aporteDebitos} </h4>
                  <h3>Monto de IVA pagar: ₡ {iva.ivatotal} </h3>
                </Card>
                <Button style={{ width: "75%" }} className="mr-2">
                  Agregar IVA
                </Button>
              </div>
              <div className="ant-col ant-col-8">
                <Button style={{ width: "75%" }} className="mr-2" type={'primary'} onClick={actiongetCCSS}>
                  Calcular CCSS
                </Button>
                <DatePicker
                  style={{ width: "75%" }}
                  format={dateFormat}
                  placeholder={'Fecha de inicio'}
                  name={'CCSSInicio'}
                  id={'CCSSInicio'}
                ></DatePicker>
                <DatePicker
                  style={{ width: "75%" }}
                  format={dateFormat}
                  placeholder={'Fecha final'}
                  name={'CCSSFinal'}
                  id={'CCSSFinal'}
                ></DatePicker>
                <Card style={{ width: "75%" }}>
                  <h4>Monto de aporte patronal: ₡ {ccss.montoPatrono} </h4>
                  <h4>Monto de aporte trabajador: ₡ {ccss.montoTrabajador} </h4>
                  <h3>Monto a pagar: ₡ {ccss.montoAPagar} </h3>
                </Card>
                <Button style={{ width: "75%" }} className="mr-2">
                  Agregar CCSS
                </Button>
              </div>
            </div>
          </Card>
        );
      }
      break;
    case "Collaborator":
      return (
        <Card title={title + " colaborador"}>
          <div className="ant-row">
            <div className="ant-col ant-col-8">
              <Form.Item
                name="name"
                label="Nombre"
                rules={rules.supplier_customer}
              >
                <Input style={{ width: "75%" }} />
              </Form.Item>
            </div>
          </div>
        </Card>
      )
    case "blogPost":
      return (
        <Card title="Nuevo post">
          <div className="ant-row">
            <div className="ant-col ant-col-24">
              <Form.Item name="titulo" label="Titulo" rules={rules.description}>
                <Input.TextArea rows={1} />
              </Form.Item>
            </div>
          </div>
          <div className="ant-row">
            <div className="ant-col ant-col-24">
              <Form.Item
                name="contenido"
                label="Contenido"
                rules={rules.description}
              >
                <Input.TextArea rows={4} />
              </Form.Item>
            </div>
          </div>
        </Card>
      );
    case "ChangeStatCxC":
      return (
        <Card title={"Confirmar ingreso cobrado"}>
          <div className="ant-row">
            <div className="ant-col ant-col-6">
              <Form.Item
                name="origin_id"
                label="Origen de ingreso"
                rules={rules.destiny_origin}
              >
                <Select style={{ width: "75%" }}>
                  {originData.map((options) => {
                    return <Option key={options.id} value={options.id}>{options.description}</Option>
                  })}
                </Select>
              </Form.Item>
            </div>
            <div className="ant-col ant-col-6">
              <Form.Item
                name="destination_id"
                label="Destino del ingreso"
                rules={rules.destiny_origin}
              >
                <Select style={{ width: "75%" }}>
                  {destinyData.map((options) => {
                    return <Option key={options.id} value={options.id}>{options.description}</Option>
                  })}
                </Select>
              </Form.Item>
            </div>
          </div>
        </Card>
      );
    default:
      return <h1>Valor invalido</h1>;
  }
};
const mapStateToProps = ({ trx }) => {
  const { destinyCat, originCat, expenseCombo, renta, iva, ccss } = trx;
  return { destinyCat, originCat, expenseCombo, renta, iva, ccss };
};
const mapDispatchToProps = {
  getDestinyCombo, getOriginCombo, getExpenseCombo, getRenta, getIVA, getCCSS, trxInsert
};
export default connect(mapStateToProps, mapDispatchToProps)(GeneralField);
