import React, { useEffect } from "react";
import PageHeaderAlt from "components/layout-components/PageHeaderAlt";
import Flex from "components/shared-components/Flex";
import { Form, Button, Spin, notification } from "antd";
import { useHistory } from "react-router-dom";
import * as constants from 'constants/TransactionInfoConstant';
import moment from 'moment';
import GeneralField from "./GeneralField";

import {
  trxInsert,
  showAuthMessage,
  updateTrx,
  resetShowMessage
} from 'redux/actions/trx';
import { connect } from "react-redux";

const ProductForm = (props) => {

  const viewInfo = props.componentsView;
  const profileType = props.profileType;

  let { message, errorCode, resetShowMessage, loading } = props

  const [form] = Form.useForm();
  let history = useHistory();

  const {
    trxInsert, updateTrx

  } = props

  const buttomOpntion = props.editableForm ? 'Editar' : 'Agregar'

  const onAdd = (values) => {
    
   
    let newDate = moment(values.trx_date).format('YYYY-MM-DD')
      
    values['trx_date'] = newDate;
    values['transaction_type'] = viewInfo;
    values['profile_id'] = profileType;
    values['transactionId'] = props.transactionId

    values['exp_type'] = props.exp_type;
    values['cx_type'] = props.cx_type;

    if (props.editableForm) {
      updateTrx(values);
    } else {
      trxInsert(values);
    }

  };

  const cancelTransaction = () => {
    const base = 'dashboards';
    let profilePath = '';
    let type = '';
    switch (profileType) {
      case constants.TYPE_PROFILE_PERSONAL_TRX:
        profilePath = 'personal';
        break;

      case constants.TYPE_PROFILE_PROFESSIONAL_TRX:
        profilePath = 'professional-services';
        break;

      case constants.TYPE_PROFILE_BUSINESS_TRX:
        profilePath = 'business';
        break;

      default:
        break;
    }

    switch (viewInfo) {
      case constants.TYPE_PERSONAL_REVENUE_TRX:
      case constants.TYPE_PROFESSIONAL_BUSINESS_REVENUE_TRX:
        type = 'revenue';
        break;

      case constants.TYPE_PERSONAL_EXPENSES_TRX:
        type = 'expense';
        break;

      case constants.TYPE_PROFESSIONAL_BUSINESS_COGS_TRX:
        type = 'cogs';
        break;

      case constants.TYPE_PROFESSIONAL_BUSINESS_SGA_TRX:
        type = 'sga';
        break;

      case constants.TYPE_PROFESSIONAL_BUSINESS_CAPEX_TRX:
        type = 'capex';
        break;

      case constants.TYPE_PROFESSIONAL_BUSINESS_CXC_TRX:
        type = 'cxc';
        break;

      case constants.TYPE_PROFESSIONAL_BUSINESS_CXP_TRX:
        type = 'cxp';
        break;

      case constants.TYPE_PROFESSIONAL_BUSINESS_PAYROLL_TRX:
        type = 'payroll';
        break;

      case constants.TYPE_PROFESSIONAL_BUSINESS_TAXATION_TRX:
        type = 'taxation';
        break;

      default:
        break;
    }

    history.push(`${base}/${profilePath}/${type}/${type}-detail`);
  }

  let showButton = (viewInfo === "Taxation" ? true : false);


  useEffect(() => {
    if (message) {
      form.resetFields();
      notification['success']({
        message: 'Notificación',
        description: message,
      });
      resetShowMessage();
    }

    if (errorCode) {
      notification['error']({
        message: 'Notificación',
        description: errorCode,
      });
      resetShowMessage();
    }

  });

  return (
    <>
      <Spin spinning={loading}>
        <Form
          layout="vertical"
          form={form}
          name="advanced_search"
          onFinish={onAdd}
          className="ant-advanced-search-form"
        >
        <PageHeaderAlt className="border-bottom" overlap>
          <div className="container">
            <Flex
              className="py-2"
              mobileFlex={false}
              justifyContent="between"
              alignItems="center"
            >

            </Flex>
          </div>

          <GeneralField
            viewtype={viewInfo}
            editable={props.editableForm}
            profile={profileType}
          />

          <div className="container"  >
            <Flex
              className="py-2"
              mobileFlex={false}
              justifyContent="between"
              alignItems="center"
            >
              <Button className="mr-2"
                hidden={showButton}
                onClick={cancelTransaction}
              >Volver</Button>
              <Button
                type="primary"
                htmlType="submit"
                hidden={showButton}
              >
                  {buttomOpntion}
                </Button>
              </Flex>
            </div>
          </PageHeaderAlt>
        </Form>
      </Spin>
    </>
  );
};

const mapStateToProps = ({ trx }) => {
  const { message, showMessage, errorCode, loading } = trx;
  return { message, showMessage, errorCode, loading }
}

const mapDispatchToProps = {
  trxInsert,
  updateTrx,
  showAuthMessage,
  resetShowMessage
}



export default connect(mapStateToProps, mapDispatchToProps)(ProductForm);