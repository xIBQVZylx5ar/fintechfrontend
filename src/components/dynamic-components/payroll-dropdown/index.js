import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Card, Table, Button, Popconfirm, notification } from 'antd';
import { DeleteOutlined, EditOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import { useHistory } from "react-router-dom";

import { getAllPayrolls, payrollDelete, getUniquePayroll, resetShowMessage } from "redux/actions/payroll";
import {  TYPE_PROFILE_PROFESSIONAL_TRX }  from 'constants/TransactionInfoConstant.js'
import Flex from "components/shared-components/Flex";

const PayrollList = (props) => {
    const questionTitle = '¿Deseas borrar esta planilla?';
    const { 
        message, 
        errorCode, 
        payrolls, 
        payrollDelete,
        getAllPayrolls,
        getUniquePayroll,
        resetShowMessage 
    } = props;

    let history = useHistory();

    useEffect(() => {
		if (message) {
			notification['success']({
				message: 'Notificación',
				description: message,
			});
			resetShowMessage();
            getAllPayrolls({
                profile_id: TYPE_PROFILE_PROFESSIONAL_TRX,
                
            })
		}
		
		if (errorCode) {
			notification['error']({
				message: 'Notificación',
				description: errorCode,
			});
			resetShowMessage();
		}
	})

    const editPayroll = (id) => {
        getUniquePayroll({
            payroll_id: id
        })
        history.push('./edit-payroll')
    }

    const deletePayroll = (id) => {

        const payload = {
           'payroll_id': id,
        }

        payrollDelete(payload);
     }

    // las columnas generales 
   const resumeRown = [
        { title: 'Mes', dataIndex: 'month', key: 'month' },
        { title: 'Año', dataIndex: 'year', key: 'year' },
        { title: 'Total', dataIndex: 'total', key: 'total' },
    ];
    /// el resumen de los datos de las columnas generales
    const resumeDetail = [];
    payrolls.map(function (completed) {
        
        return resumeDetail.push({
            key: (completed.month + completed.year.toString()).toString(),
            month: completed.monthLabel,
            year: completed.year,
            total: '₡' + completed.total,
            payrolls: completed.payrolls
        });

    });
    const expandedRowRender = (resumeDetail) => {
        const datos = resumeDetail;
        const payrollHistory = [];
        let columns;

        datos.payrolls.map(function (payroll) {

            return payrollHistory.push({
               key: payroll.payroll_id,
               DATE: payroll.trx_date,
               AMOUNT: '₡' + payroll.amount,
               COLLABORATOR: payroll.collab_name
            })
        });

        columns = [
            { title: 'Monto', dataIndex: 'AMOUNT', key: 'AMOUNT', align: "center" },
            { title: 'Fecha', dataIndex: 'DATE', key: 'DATE', align: "center" },
            { title: 'Colaborador', dataIndex: 'COLLABORATOR', key: 'COLLABORATOR', align: "center"},
            { title: 'Acciones', key: 'actions', render: (payroll) => (
                <Flex
                    justifyContent="center"
                > 
                    <Button
                        className="mr-1"
                        type="primary"
                        icon={<EditOutlined  />}
                        onClick={() => editPayroll(payroll.key)}
                    ></Button>
                    <Popconfirm title={questionTitle} onConfirm={() => deletePayroll(payroll.key)}  icon={<QuestionCircleOutlined style={{ color: 'red' }}/>}>
                        <Button
                            className="ml-2"
                            icon={<DeleteOutlined />}
                            type="primary"
                            danger
                        ></Button>
                    </Popconfirm>
                </Flex>
            ), align: "center"}
        ]
         return (
            <Card>
                <Table 
                    columns={columns}
                    dataSource={payrollHistory}
                />
            </Card>
         )
    }
    return (
        <Table 
            columns={resumeRown}
            dataSource={resumeDetail}
            expandable={{ expandedRowRender }}
            pagination={true}
        />
    )
}

const mapStateToProps = ({ payroll }) => {
    const { message, errorCode, payrolls, data } = payroll;
    return { message, errorCode, payrolls, data }
}

const mapDispatchToProps = {
    payrollDelete,
    getAllPayrolls,
    getUniquePayroll,
    resetShowMessage
}

export default connect(mapStateToProps, mapDispatchToProps)(PayrollList);