import React, { useEffect } from 'react';
import { getUniqueTrx, trxDelete, resetShowMessage, getAllTrx, updateTrx } from 'redux/actions/trx';
import { connect } from 'react-redux';
import { Card, Table, Button, Popconfirm, notification, Modal, Form, Row, Col } from 'antd';
import { DeleteOutlined, EditOutlined, QuestionCircleOutlined, CheckOutlined } from '@ant-design/icons';
import GeneralField from "../addForm/GeneralField";
import moment from 'moment';
import * as constants from 'constants/TransactionInfoConstant';
import ModalChanceStatCx from 'components/dynamic-components/modal-chance-stat-cx';
function TransactionList(props) {
   const [visible, setVisible] = React.useState(false);
   const [visibleModalCx] = React.useState(false);
   const [valuesModalCx, setvaluesModalCx] = React.useState(false);
   const [confirmLoading, setConfirmLoading] = React.useState(false);
   const [isModalVisible, setIsModalVisible] = React.useState(false);
   const [form] = Form.useForm();
   const [toEditData, setToEditData] = React.useState([])
   const { transactions, trxDelete, resetShowMessage, message, type_list, getAllTrx, updateTrx, profile, errorCode } = props;

   let toEdit = [];
   let editObject = []
   transactions.map(function (info) {

      if (info.transactions.length > 0) {
         for (let index = 0; index < info.transactions.length; index++) {
            editObject.push(info.transactions[index])
         }
      }
   })


   function showModal(trx, type_list) {
      setVisible(true);
      switch (type_list) {
         case constants.TYPE_PERSONAL_REVENUE_TRX:
         case constants.TYPE_PROFESSIONAL_BUSINESS_REVENUE_TRX:
            toEdit = editObject.find(({ revenue_id }) => revenue_id === trx);      
            break;
         case constants.TYPE_PERSONAL_EXPENSES_TRX:
         case constants.TYPE_PROFESSIONAL_BUSINESS_CAPEX_TRX:
         case constants.TYPE_PROFESSIONAL_BUSINESS_COGS_TRX:
         case constants.TYPE_PROFESSIONAL_BUSINESS_SGA_TRX:
            toEdit = editObject.find(({ id_expense}) => id_expense === trx);
         break;    
         case constants.TYPE_PROFESSIONAL_BUSINESS_CXC_TRX:
         case constants.TYPE_PROFESSIONAL_BUSINESS_CXP_TRX:
            toEdit = editObject.find(({ cx_id}) => cx_id === trx);
         break;
         default:
            break;
      }
      
      setToEditData(toEdit);
   };
   


   switch (type_list) {
      case constants.TYPE_PERSONAL_REVENUE_TRX:
      case constants.TYPE_PROFESSIONAL_BUSINESS_REVENUE_TRX:
         form.setFieldsValue({
            amount: toEditData.amount,
            origin_id: toEditData.origin_id,
            destination_id: toEditData.destination_id,
            description: toEditData.description,
            trx_date: moment(toEditData.trx_date).parseZone()
         });
         break;
      case constants.TYPE_PERSONAL_EXPENSES_TRX:
         form.setFieldsValue({
            amount: toEditData.amount,
            destination_id: toEditData.destination_id,
            id_exp_cat: toEditData.id_exp_cat,
            description: toEditData.description,
            trx_date: moment(toEditData.trx_date).parseZone()
         });
         break
      case constants.TYPE_PROFESSIONAL_BUSINESS_COGS_TRX:
         form.setFieldsValue({
            amount: toEditData.amount,
            destination_id: toEditData.destination_id,
            description: toEditData.description,
            trx_date: moment(toEditData.trx_date).parseZone(),
            bill_number: toEditData.bill_number,
            iva_percentage: toEditData.iva_percentage,
            supplier: toEditData.supplier
         });
         break
      case constants.TYPE_PROFESSIONAL_BUSINESS_SGA_TRX:
      case constants.TYPE_PROFESSIONAL_BUSINESS_CAPEX_TRX:
         form.setFieldsValue({
            amount: toEditData.amount,
            destination_id: toEditData.destination_id,
            description: toEditData.description,
            trx_date: moment(toEditData.trx_date).parseZone(),
            bill_number: toEditData.bill_number,
            iva_percentage: toEditData.iva_percentage,
            supplier: toEditData.supplier
         });
         break
      case constants.TYPE_PROFESSIONAL_BUSINESS_CXC_TRX:
      case constants.TYPE_PROFESSIONAL_BUSINESS_CXP_TRX:
         form.setFieldsValue({
            amount: toEditData.amount,
            description: toEditData.description,
            trx_date: moment(toEditData.trx_date).parseZone(),
            client_supplier: toEditData.client_supplier
         });
         break;
      default:
         break;
   };
   
   const showModalCx = (id, cxType) => {

      setvaluesModalCx({
         id,
         cxType,
         profile_id: profile,
         transaction_type: type_list
      });
      //setvisibleModalCx(true);
      setIsModalVisible(true);
   }
   const handleOk = () => {
      setConfirmLoading(true);
      setTimeout(() => {
         setVisible(false);
         setConfirmLoading(false);
      }, 2000);
   };
   const onEdit = () => {
      let values = form.getFieldsValue();

      values['transaction_type'] = type_list;
      switch (type_list) {
         case constants.TYPE_PERSONAL_REVENUE_TRX:
         case constants.TYPE_PROFESSIONAL_BUSINESS_REVENUE_TRX:
            values['id'] = toEditData.revenue_id
            break;
         case constants.TYPE_PERSONAL_EXPENSES_TRX:
         case constants.TYPE_PROFESSIONAL_BUSINESS_CAPEX_TRX:
         case constants.TYPE_PROFESSIONAL_BUSINESS_COGS_TRX:
         case constants.TYPE_PROFESSIONAL_BUSINESS_SGA_TRX:
            values['id'] = toEditData.id_expense
            break;
         case constants.TYPE_PROFESSIONAL_BUSINESS_CXC_TRX:
         case constants.TYPE_PROFESSIONAL_BUSINESS_CXP_TRX:
            values['id'] = toEditData.cx_id
               break;
         default:
            break;
      }
      values['profile_id'] = profile;
      updateTrx(values);
   }
   const handleCancel = () => {
      setVisible(false);
   };
   const handleCancelCx = () => {
      setIsModalVisible(false);
   };
   const questionTitle = '¿Deseas borrar esta transacción?';
   let trx_type = type_list;
   // las columnas generales 
   const resumeRown = [
      { title: 'Mes', dataIndex: 'month', key: 'month' },
      { title: 'Año', dataIndex: 'year', key: 'year' },
      { title: 'Total', dataIndex: 'total', key: 'total' },
   ];
   /// el resumen de los datos de las columnas generales
   const resumeDetail = [];
   transactions.map(function (completed) {
      return resumeDetail.push({
         key: (completed.month + completed.year.toString()).toString(),
         month: completed.monthLabel,
         year: completed.year,
         total: '₡' + completed.total,
         trx: completed.transactions
      });
   });
   const deleteTrx = (id) => {
      const payload = {
         'transactionId': id,
         'transactionType': trx_type,
         'profile_id': profile
      }
      trxDelete(payload);
      const today = new Date();
      getAllTrx({
         transaction_type: type_list,
         profile_id: profile,
         start_date: new Date(today.getFullYear(), '01', '01'),
         end_date: new Date(today.getFullYear(), '12', '31'),
      });
   }
   useEffect(() => {
      if (message) {
         notification['success']({
            message: 'Notificación',
            description: message,
         });
         resetShowMessage();
         handleOk();
         handleCancelCx();
         const today = new Date();
         getAllTrx({
            transaction_type: type_list,
            profile_id: profile,
            start_date: new Date(today.getFullYear(), '01', '01'),
            end_date: new Date(today.getFullYear(), '12', '31'),
         });
      }
      if (errorCode) {
         notification['error']({
            message: 'Notificación',
            description: errorCode,
         });
         resetShowMessage();
      }
   });
   function expandedRowRender(resumeDetail) {
      let trxHistory = [];
      const datos = resumeDetail;
      let columns;
      switch (type_list) {
         case constants.TYPE_PERSONAL_REVENUE_TRX:
            datos.trx.map(function (getTRX) {
               return trxHistory.push({
                  key: getTRX.revenue_id,
                  DATE: getTRX.trx_date,
                  AMOUNT: '₡' + getTRX.amount,
                  DESCRIPTION: getTRX.description,
                  origin: getTRX.origin,
                  destination: getTRX.destination
               })
            });
            columns = [
               { title: 'Monto', dataIndex: 'AMOUNT', key: 'AMOUNT', align: "center" },
               { title: 'Fecha', dataIndex: 'DATE', key: 'DATE', align: "center" },
               { title: 'Descripcion', dataIndex: 'DESCRIPTION', key: 'DESCRIPTION', align: "center" },
               { title: 'Destino', dataIndex: 'destination', key: 'destination', align: "center" },
               { title: 'Origen', dataIndex: 'origin', key: 'origin', align: "center" },
               {
                  title: 'Acciones', key: 'actions', render: (revenue_id) => (
                     <div>
                        <Button
                           type="primary"
                           icon={<EditOutlined />}
                           onClick={() => showModal(revenue_id.key, type_list)}
                        ></Button>
                        <Popconfirm title={questionTitle} onConfirm={() => deleteTrx(revenue_id.key)} icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                           <Button
                              icon={<DeleteOutlined />}
                              type="primary"
                              danger
                           ></Button>
                        </Popconfirm>
                     </div>
                  ), align: "center"
               }
            ];
            break;
         case constants.TYPE_PROFESSIONAL_BUSINESS_REVENUE_TRX:
            datos.trx.map(function (getTRX) {
               return trxHistory.push({
                  key: getTRX.revenue_id,
                  DATE: getTRX.trx_date,
                  AMOUNT: '₡' + getTRX.amount,
                  DESCRIPTION: getTRX.description,
                  origin: getTRX.origin,
                  destination: getTRX.destination
               })
            });
            columns = [
               { title: 'Monto', dataIndex: 'AMOUNT', key: 'AMOUNT', align: "center" },
               { title: 'Fecha', dataIndex: 'DATE', key: 'DATE', align: "center" },
               { title: 'Descripcion', dataIndex: 'DESCRIPTION', key: 'DESCRIPTION', align: "center" },
               { title: 'Destino', dataIndex: 'destination', key: 'destination', align: "center" },
               { title: 'Origen', dataIndex: 'origin', key: 'origin', align: "center" },
               {
                  title: 'Acciones', key: 'actions', render: (revenue_id) => (
                     <div>
                        <Button
                           type="primary"
                           icon={<EditOutlined />}
                           onClick={() => showModal(revenue_id.key, type_list)}
                        ></Button>
                        <Popconfirm title={questionTitle} onConfirm={() => deleteTrx(revenue_id.key)} icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                           <Button
                              icon={<DeleteOutlined />}
                              type="primary"
                              danger
                           ></Button>
                        </Popconfirm>
                     </div>
                  ), align: "center"
               }
            ];
            break;
         case constants.TYPE_PERSONAL_EXPENSES_TRX:
            datos.trx.map(function (getTRX) {
               return trxHistory.push({
                  key: getTRX.id_expense,
                  DATE: getTRX.trx_date,
                  AMOUNT: '₡' + getTRX.amount,
                  DESCRIPTION: getTRX.description,
                  category: getTRX.category,
                  destination: getTRX.destination
               })
            });
            columns = [
               { title: 'Monto', dataIndex: 'AMOUNT', key: 'AMOUNT', align: "center" },
               { title: 'Fecha', dataIndex: 'DATE', key: 'DATE', align: "center" },
               { title: 'Descripcion', dataIndex: 'DESCRIPTION', key: 'DESCRIPTION', align: "center" },
               { title: 'Categoria', dataIndex: 'category', key: 'category', align: "center" },
               { title: 'Fuente', dataIndex: 'destination', key: 'destination', align: "center" },
               {
                  title: 'Acciones', key: 'actions', render: (id_expense) => (
                     <div>
                        <Button
                           type="primary"
                           icon={<EditOutlined />}
                           onClick={() => showModal(id_expense.key, type_list)}
                        ></Button>
                        <Popconfirm title={questionTitle} onConfirm={() => deleteTrx(id_expense.key)} icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                           <Button
                              icon={<DeleteOutlined />}
                              type="primary"
                              danger
                           ></Button>
                        </Popconfirm>
                     </div>
                  ), align: "center"
               }
            ];
            break;
         case constants.TYPE_PROFESSIONAL_BUSINESS_COGS_TRX:
            datos.trx.map(function (getTRX) {
               return trxHistory.push({
                  key: getTRX.id_expense,
                  DATE: getTRX.trx_date,
                  AMOUNT: '₡' + getTRX.amount,
                  DESCRIPTION: getTRX.description,
                  supplier: getTRX.supplier,
                  destination: getTRX.destiny,
                  iva: getTRX.iva_percentage
               })
            });
            columns = [
               { title: 'Monto', dataIndex: 'AMOUNT', key: 'AMOUNT', align: "center" },
               { title: 'Fecha', dataIndex: 'DATE', key: 'DATE', align: "center" },
               { title: 'Descripcion', dataIndex: 'DESCRIPTION', key: 'DESCRIPTION', align: "center" },
               { title: 'Proveedor', dataIndex: 'supplier', key: 'supplier', align: "center" },
               { title: 'Fuente', dataIndex: 'destination', key: 'destination', align: "center" },
               { title: 'IVA', dataIndex: 'iva', key: 'iva', align: "center" },
               {
                  title: 'Acciones', key: 'actions', render: (id_expense) => (
                     <div>
                        <Button
                           type="primary"
                           icon={<EditOutlined />}
                           onClick={() => showModal(id_expense.key, type_list)}
                        ></Button>
                        <Popconfirm title={questionTitle} onConfirm={() => deleteTrx(id_expense.key)} icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                           <Button
                              icon={<DeleteOutlined />}
                              type="primary"
                              danger
                           ></Button>
                        </Popconfirm>
                     </div>
                  ), align: "center"
               }
            ];
            break;
         case constants.TYPE_PROFESSIONAL_BUSINESS_SGA_TRX:
            datos.trx.map(function (getTRX) {
               return trxHistory.push({
                  key: getTRX.id_expense,
                  DATE: getTRX.trx_date,
                  AMOUNT: '₡' + getTRX.amount,
                  DESCRIPTION: getTRX.description,
                  supplier: getTRX.supplier,
                  destination: getTRX.destiny,
                  iva: getTRX.iva_percentage
               })
            });
            columns = [
               { title: 'Monto', dataIndex: 'AMOUNT', key: 'AMOUNT', align: "center" },
               { title: 'Fecha', dataIndex: 'DATE', key: 'DATE', align: "center" },
               { title: 'Descripcion', dataIndex: 'DESCRIPTION', key: 'DESCRIPTION', align: "center" },
               { title: 'Proveedor', dataIndex: 'supplier', key: 'supplier', align: "center" },
               { title: 'Fuente', dataIndex: 'destination', key: 'destination', align: "center" },
               { title: 'IVA', dataIndex: 'iva', key: 'iva', align: "center" },
               {
                  title: 'Acciones', key: 'actions', render: (id_expense) => (
                     <div>
                        <Button
                           type="primary"
                           icon={<EditOutlined />}
                           onClick={() => showModal(id_expense.key, type_list)}
                        ></Button>
                        <Popconfirm title={questionTitle} onConfirm={() => deleteTrx(id_expense.key)} icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                           <Button
                              icon={<DeleteOutlined />}
                              type="primary"
                              danger
                           ></Button>
                        </Popconfirm>
                     </div>
                  ), align: "center"
               }
            ];
            break;
         case constants.TYPE_PROFESSIONAL_BUSINESS_CXC_TRX:
         case constants.TYPE_PROFESSIONAL_BUSINESS_CXP_TRX:
            datos.trx.map(function (getTRX) {
               return trxHistory.push({
                  key: getTRX.cx_id,
                  DATE: getTRX.trx_date,
                  AMOUNT: '₡' + getTRX.amount,
                  DESCRIPTION: getTRX.description
               })
            });
            columns = [
               { title: 'Monto', dataIndex: 'AMOUNT', key: 'AMOUNT', align: "center" },
               { title: 'Fecha', dataIndex: 'DATE', key: 'DATE', align: "center" },
               { title: 'Descripcion', dataIndex: 'DESCRIPTION', key: 'DESCRIPTION', align: "center" },
               {
                  title: 'Acciones', key: 'actions', render: (cx_id) => (
                     <div>
                        <Button
                           icon={<CheckOutlined />}
                           onClick={() => showModalCx(cx_id.key, (constants.TYPE_PROFESSIONAL_BUSINESS_CXC_TRX ? constants.TYPE_CXC : constants.TYPE_CXP))}
                        ></Button>
                        <Button
                           type="primary"
                           icon={<EditOutlined />}
                           onClick={() => showModal(cx_id.key, type_list)}
                        ></Button>
                        <Popconfirm title={questionTitle} onConfirm={() => deleteTrx(cx_id.key)} icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                           <Button
                              icon={<DeleteOutlined />}
                              type="primary"
                              danger
                           ></Button>
                        </Popconfirm>
                     </div>
                  ), align: "center"
               }
            ];
            break;
         case constants.TYPE_PROFESSIONAL_BUSINESS_CAPEX_TRX:
            datos.trx.map(function (getTRX) {
               return trxHistory.push({
                  key: getTRX.id_expense,
                  DATE: getTRX.trx_date,
                  AMOUNT: '₡' + getTRX.amount,
                  DESCRIPTION: getTRX.description,
                  supplier: getTRX.supplier,
                  destination: getTRX.destiny,
                  iva: getTRX.iva_percentage
               })
            });
            columns = [
               { title: 'Monto', dataIndex: 'AMOUNT', key: 'AMOUNT', align: "center" },
               { title: 'Fecha', dataIndex: 'DATE', key: 'DATE', align: "center" },
               { title: 'Descripcion', dataIndex: 'DESCRIPTION', key: 'DESCRIPTION', align: "center" },
               { title: 'Proveedor', dataIndex: 'supplier', key: 'supplier', align: "center" },
               { title: 'Fuente', dataIndex: 'destination', key: 'destination', align: "center" },
               { title: 'IVA', dataIndex: 'iva', key: 'iva', align: "center" },
               {
                  title: 'Acciones', key: 'actions', render: (id_expense) => (
                     <div>
                        <Button
                           type="primary"
                           icon={<EditOutlined />}
                           onClick={() => showModal(id_expense.key, type_list)}
                        ></Button>
                        <Popconfirm title={questionTitle} onConfirm={() => deleteTrx(id_expense.key)} icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                           <Button
                              icon={<DeleteOutlined />}
                              type="primary"
                              danger
                           ></Button>
                        </Popconfirm>
                     </div>
                  ), align: "center"
               }
            ];
            break;
         default:
            break;
      }
      return (
         <Card>
            <Table
               columns={columns}
               dataSource={trxHistory}
               pagination={true} />
         </Card>
      );
   };

   let new_values
   const newVal = () => {
      new_values = form.getFieldsValue();
      form.setFieldsValue(new_values);
   }
   return (
      <div>
         <Table
            className="components-table-demo-nested"
            columns={resumeRown}
            expandable={{ expandedRowRender }}
            dataSource={resumeDetail}
            pagination={true}
         />
         <Modal
            getContainer={false}
            visible={visible}
            confirmLoading={confirmLoading}
            width={1000}
            footer={null}
            forceRenderen={false}
            onCancel={() => setVisible(false)}
         >
            <>
               <Form
                  layout="vertical"
                  form={form}
                  name="modal-form"
                  //onFinish={onEdit}
                  className="modal-form"
                  //initialValues= {defaultValues}
                  forcerenderen={"true"}
                  onValuesChange={newVal}
               >
                  <GeneralField
                     viewtype={type_list}
                     editable={true}
                     profile={profile}
                  />
                  <Row>
                     <Col span={12} align={"left"}>
                        <Button key="back" onClick={handleCancel} >
                           Cancelar
                        </Button>
                     </Col>
                     <Col span={12} align={"right"}>
                        <Button onClick={onEdit} type="primary">
                           Actualizar
                        </Button>
                     </Col>
                  </Row>
               </Form>
            </>
         </Modal>
         {
            isModalVisible ? (
               <Modal
                  visible={isModalVisible}
                  //confirmLoading={confirmLoading}
                  width={1000}
                  footer={null}
                  forceRenderen
                  onCancel={() => handleCancelCx()}
               >
                  <ModalChanceStatCx visibleBool={visibleModalCx} valuesData={valuesModalCx}></ModalChanceStatCx>
               </Modal>
            ) : null
         }
      </div>
   );
}
const mapStateToProps = ({ trx }) => {
   const { message, showMessage, redirect, transactions, transaction, errorCode } = trx;
   return { message, showMessage, redirect, transactions, transaction, errorCode };
};
const mapDispatchToProps = {
   getUniqueTrx, trxDelete, resetShowMessage, getAllTrx, updateTrx
};
export default connect(mapStateToProps, mapDispatchToProps)(TransactionList);
