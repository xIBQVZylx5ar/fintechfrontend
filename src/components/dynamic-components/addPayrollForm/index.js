import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import PageHeaderAlt from "components/layout-components/PageHeaderAlt";
import Flex from "components/shared-components/Flex";
import { Form, Button } from "antd";
import { notification  } from 'antd';

import GeneralField from "../addForm/GeneralField";
import { TYPE_PROFESSIONAL_BUSINESS_PAYROLL_TRX, TYPE_PROFESSIONAL_BUSINESS_COLLABORATOR_TRX } from "constants/TransactionInfoConstant";
import {
    payrollInsert,
    payrollUpdate,
    collaboratorInsert,
    getAllCollaborators,
    resetShowMessage
} from 'redux/actions/payroll';
import { connect } from "react-redux";

const ProductForm = (props) => {
  
  const viewInfo = props.componentsView;
  //const profileType = props.profileType

  let { 
      editableForm, 
      profileType,
      message, 
      errorCode, 
      collaborators, 
      data,
      resetShowMessage, 
      payrollInsert,
      payrollUpdate,
      collaboratorInsert,
      getAllCollaborators
    } = props

  const [form] = Form.useForm();
  let history = useHistory()

  const buttomOpntion = editableForm? 'Editar' : 'Agregar'

  const onAdd = (values) => {
    
    switch(viewInfo){
      case TYPE_PROFESSIONAL_BUSINESS_PAYROLL_TRX: 
        values['profile_id'] = profileType;
        
        if(editableForm){
          values['payroll_id'] = data.payroll_id;
          values['collaborator_id'] = values.collaborator_id;
          payrollUpdate(values);
        }else{
          values['collaborators'] = JSON.stringify(values.collaborators)
          payrollInsert(values);
        }
        break;
      case TYPE_PROFESSIONAL_BUSINESS_COLLABORATOR_TRX:
        collaboratorInsert(values)
        break;
      default:
        break;
    }
    history.push("./")
  };

  const onCancel = () => {
      return history.push("./")
  }

  useEffect(() => {
    if (message) {
      form.resetFields();
      notification['success']({
        message: 'Notificación',
        description: message,
      });
      resetShowMessage();
    }
    
    if (errorCode) {
      notification['error']({
        message: 'Notificación',
        description: errorCode,
      });
      resetShowMessage();
    }
  });

  useEffect(()=>{
    getAllCollaborators();
  }, [])

  return (
    <>
      <Form
        layout="vertical"
        form={form}
        name="advanced_search"
        onFinish={onAdd}
        className="ant-advanced-search-form"
      >        
          <PageHeaderAlt className="border-bottom" overlap>
            <div className="container">
              <Flex
                className="py-2"
                mobileFlex={false}
                justifyContent="between"
                alignItems="center"
              >

              </Flex>
            </div>

            <GeneralField
              viewtype={viewInfo}
              editable={editableForm}
              data={collaborators}
              values={data}
            />

            <div className="container"  >
              <Flex
                className="py-2"
                mobileFlex={false}
                justifyContent="between"
                alignItems="center"
              >
                <Button className="mr-2"
                    onClick={onCancel}
                  //hidden={showButton}
                >Cancelar</Button>
                <Button
                  type="primary"
                  htmlType="submit"
                  //hidden={showButton}
                >
                  {buttomOpntion}
                </Button>
              </Flex>
            </div>
          </PageHeaderAlt>

      </Form>
    </>
  );
};

const mapStateToProps = ({ payroll }) => {
  const { message, errorCode, collaborators, data } = payroll;
  return { message, errorCode, collaborators, data }
}

const mapDispatchToProps = {
    payrollInsert,
    payrollUpdate,
    collaboratorInsert,
    getAllCollaborators,
    resetShowMessage,
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductForm);