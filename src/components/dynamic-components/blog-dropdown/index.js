import React, { useState, useEffect } from 'react';
import { connect } from "react-redux";
import { Card, Table, Button, Form, notification, Input, Popconfirm } from 'antd';
import { EditOutlined, DeleteOutlined, QuestionCircleOutlined } from '@ant-design/icons';

import { blogUpdate, blogDelete, getAllBlogs, resetShowMessage } from 'redux/actions/blog';
import Flex from 'components/shared-components/Flex';

const BlogList = (props) => {
    const [updatePost, setUpdatePost] = useState("");
    const questionTitle = '¿Deseas borrar esta publicación?';

    const { 
        blogUpdate, 
        blogDelete, 
        getAllBlogs,
		resetShowMessage,
		message, 
		errorCode,
        posts 
    } = props;

    const [formUpdate] = Form.useForm();

    const rules = {
		content: [
			{
			  required: true,
			  message: "Por favor ingrese un contenido",
			},
		],
	}

    useEffect(() => {
		if (message) {
			formUpdate.resetFields();
			notification['success']({
				message: 'Notificación',
				description: message,
			});
			resetShowMessage();
			getAllBlogs();
		}
		
		if (errorCode) {
			formUpdate.resetFields()
			notification['error']({
				message: 'Notificación',
				description: errorCode,
			});
			resetShowMessage();
		}
	})

    const editBlog = (values) => {
		const value = Object.keys(values)[0];
		const payload = {
			postId: updatePost,
			content: values[value]
		}
		blogUpdate(payload)
		setUpdatePost("")
	}

	const deleteBlog = (id) => {	
		const payload = {
			postId: id
		}
		blogDelete(payload)
	}

    const resumeDetail = []
    posts.map((post)=> {
        return resumeDetail.push({
            key: post.post_id,
            content: post.content,
            total: post.comments.length,
            comments: post.comments
        })
    })

    let resumeRown = [
        { title: 'Contenido', key: 'content', render: (post) => (
            <div>
                {updatePost === post.key ?
                <Form 
                    layout="vertical"
                    form={formUpdate}
                    name="advanced_search"
                    onFinish={editBlog}
                    className="ant-advanced-search-form"
                >
                    <Form.Item
                        className="mb-1"
                        name={"content-"+post.key}
                        rules={rules.content} 
                        initialValue={post.content}
                    >
                        <Input.TextArea rows={4}/>
                    </Form.Item>

                    <Flex
                        className="py-2"
                        mobileFlex={false}
                        justifyContent="between"
                        alignItems="center"
                    >
                        <Button
                            className="mr-1"
                            type="primary"
                            htmlType="submit"
                        >
                            Guardar
                        </Button>
                        
                        <Button 
                            className="ml-1"
                            type="primary"
                            onClick={()=>setUpdatePost("")}
                            danger
                        >
                            Cancelar
                        </Button>
                    </Flex>
                </Form>
                :
                    <span>{post.content}</span>
                }
            </div>
    
        ), align: "center" },
        { title: 'Comentarios', dataIndex: 'total', key: 'total', align: 'center'},
        { title: 'Acciones', key: 'actions', render: (post) => (
            <Flex
                justifyContent="center"
            > 
                <Button
                    className="mr-1"
                    type="primary"
                    icon={<EditOutlined  />}
                    onClick={() => setUpdatePost(post.key)}
                ></Button>
                <Popconfirm title={questionTitle} onConfirm={() => deleteBlog(post.key)}  icon={<QuestionCircleOutlined style={{ color: 'red' }}/>}>
                    <Button
                        className="ml-2"
                        icon={<DeleteOutlined />}
                        type="primary"
                        danger
                    ></Button>
                </Popconfirm>
            </Flex>
        ), align: "center"
        }
    ];

    const expandedRowRender = (resumeDetail) => {
        const datos = resumeDetail;
        const blogHistory = [];
        let columns;

        datos.comments.map(comment => {
            return blogHistory.push({
                key: comment.id_comment,
                user_id: comment.user_id,
                content: comment.content
            })
        })

        columns = [
            { title: "Id de Usuario", dataIndex: "user_id" , key: "user_id", align: "center"},
            { title: "Contenido de Comentario", dataIndex: "content", key: "content", align: "center"}
        ]

        return (
            <Card>
                <Table
                    columns={columns}
                    dataSource={blogHistory}
                />
            </Card>
        )
    }

    return (
        <Card>
            <Table 
                columns={resumeRown}
                dataSource={resumeDetail}
                pagination={true}
                expandable={{ expandedRowRender }}
            />
        </Card>
    )
}

const mapStateToProps = ({ blog }) => {
    const { message, showMessage, errorCode, posts } = blog;
    return { message, showMessage, errorCode, posts }
}

const mapDispatchToProps = {
    blogUpdate,
    blogDelete,
    getAllBlogs,
    resetShowMessage
}

export default connect(mapStateToProps, mapDispatchToProps)(BlogList) 