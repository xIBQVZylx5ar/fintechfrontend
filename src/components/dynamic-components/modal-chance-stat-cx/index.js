import React, { useEffect } from "react";
import { updateTrx, getOriginCombo, getDestinyCombo } from "redux/actions/trx";
import { connect } from "react-redux";
import { Button, Card, Form, Row, Col, Select, Input } from "antd";
import {
  TYPE_PROFESSIONAL_BUSINESS_CXC_TRX,
  TYPE_PROFESSIONAL_BUSINESS_CXP_TRX,
} from "constants/TransactionInfoConstant";

const ModalChanceStatCx = (props) => {
  const {
    valuesData,
    updateTrx,
    getOriginCombo,
    getDestinyCombo,
    originCat,
    destinyCat,
    loading,
  } = props;
  const [form] = Form.useForm();
  const { Option } = Select;
  const { id, profile_id, transaction_type } = valuesData;

  useEffect(() => {
    switch (transaction_type) {
      case TYPE_PROFESSIONAL_BUSINESS_CXC_TRX:
        getOriginCombo({
          combo_type: "origin",
          user: 8,
          profile_id: profile_id,
        });
        getDestinyCombo({
          combo_type: "destiny",
          user: 8,
          profile_id: profile_id,
        });
        break;

      case TYPE_PROFESSIONAL_BUSINESS_CXP_TRX:
        getDestinyCombo({
          combo_type: "destiny",
          user: 8,
          profile_id: profile_id,
        });
        break;
      default:
        break;
    }
  }, []);

  const rules = {
    destiny_origin: [
      {
        required: true,
        message: "Por favor seleccione una fuente a afectar",
      },
    ],
    bill: [
      {
        required: false,
        message: "Por favor ingrese el numero de factura/clave",
      },
    ],
  };

  const onEdit = (values) => {
    values["id"] = id;
    values["profile_id"] = profile_id;
    values["transaction_type"] = transaction_type;
    values["state"] = true;

    updateTrx(values);
  };

  return (
    <>
      <Form
        layout="vertical"
        form={form}
        name="modal-form"
        onFinish={onEdit}
        className="modal-form"
        //initialValues= {defaultValues}
        forcerenderen={"true"}
        //onValuesChange={newVal}
      >
        {transaction_type === TYPE_PROFESSIONAL_BUSINESS_CXC_TRX ? (
          <Card title={"Confirmar ingreso cobrado"}>
            <div className="ant-row">
              <div className="ant-col ant-col-6">
                <Form.Item
                  name="origin_id"
                  label="Origen de ingreso"
                  rules={rules.destiny_origin}
                >
                  {originCat ? (
                    <Select style={{ width: "75%" }} disabled={loading}>
                      {originCat.map((options) => {
                        return (
                          <Option key={options.id} value={options.id}>
                            {options.description}
                          </Option>
                        );
                      })}
                    </Select>
                  ) : null}
                </Form.Item>
              </div>
              <div className="ant-col ant-col-6">
                <Form.Item
                  name="destination_id"
                  label="Destino del ingreso"
                  rules={rules.destiny_origin}
                >
                  {destinyCat ? (
                    <Select style={{ width: "75%" }} disabled={loading}>
                      {destinyCat.map((options) => {
                        return (
                          <Option key={options.id} value={options.id}>
                            {options.description}
                          </Option>
                        );
                      })}
                    </Select>
                  ) : null}
                </Form.Item>
              </div>
            </div>
          </Card>
        ) : (
          <Card title={"Confirmar egreso pagado"}>
            <div className="ant-row">
              <div className="ant-col ant-col-6">
                <Form.Item
                  name="destination_id"
                  label="Fuente del egreso"
                  rules={rules.destiny_origin}
                >
                  <Select style={{ width: "75%" }} disabled={loading}>
                    {destinyCat.map((options) => {
                      return (
                        <Option key={options.id} value={options.id}>
                          {options.description}
                        </Option>
                      );
                    })}
                  </Select>
                </Form.Item>
              </div>
              <div className="ant-col ant-col-6">
                <Form.Item
                  name="exp_type"
                  label="Tipo de egreso"
                  rules={rules.destiny_origin}
                >
                  <Select style={{ width: "100%" }} disabled={loading}>
                    <Option key={4} value={4}>
                      {"Gastos al capital (CAPEX)"}
                    </Option>
                    <Option key={2} value={2}>
                      {"Costos de los bienes vendidos (COGS)"}
                    </Option>
                    <Option key={3} value={3}>
                      {"Gastos de venta, generales y administrativos (SG&A)"}
                    </Option>
                  </Select>
                </Form.Item>
              </div>
            </div>

            <div className="ant-row"></div>
            <Form.Item
              name="bill_number"
              label="Numero de clave(factura)"
              rules={rules.bill}
            >
              <Input disabled={loading}></Input>
            </Form.Item>
          </Card>
        )}

        <Row>
          <Col span={24} align={"right"}>
            <Button htmlType="submit" type="primary">
              Confirmar
            </Button>
          </Col>
        </Row>
      </Form>
    </>
  );
};

const mapStateToProps = ({ trx }) => {
  const { message, showMessage, errorCode, originCat, destinyCat, loading } =
    trx;
  return { message, showMessage, errorCode, originCat, destinyCat, loading };
};

const mapDispatchToProps = {
  updateTrx,
  getOriginCombo,
  getDestinyCombo,
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalChanceStatCx);
