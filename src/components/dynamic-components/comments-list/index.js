import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Card, Collapse, Form, Input, Button, notification, Popconfirm, Typography } from "antd";
import { RightOutlined, EditOutlined, DeleteOutlined, QuestionCircleOutlined } from "@ant-design/icons";
import moment from "moment";

import { commentInsert, commentUpdate, commentDelete, resetShowMessage } from "redux/actions/comments";
import { getAllBlogs } from "redux/actions/blog";
import Flex from "components/shared-components/Flex";

const CommentList = (props) => {
    const [updateComment, setUpdateComment] = useState("");
    const questionTitle = '¿Desea eliminar este comentario?';
    const {
        commentUpdate,
        commentDelete,
        resetShowMessage,
        message,
        errorCode,
        posts,
        getAllBlogs
    } = props;

    const { Panel } = Collapse;
    const { Text } = Typography

    const [formUpdate] = Form.useForm();

    const rules = {
        content: [
            {
                required: true,
                message: "Por favor ingrese un comentario",
            },
        ],
    }

    const onUpdateClick = (id) => {
        return setUpdateComment(id)
    }

    const editComment = (values) => {
        const value = Object.keys(values)[0];

        const payload = {
            commentId: updateComment,
            postId: values.postId,
            content: values[value]
        }

        commentUpdate(payload)

        setUpdateComment("")
    }

    const deleteComment = (id) => {
        const payload = {
            commentId: id
        }

        commentDelete(payload)
    }

    useEffect(() => {
        if (message) {
            notification['success']({
                message: 'Notificación',
                description: message,
            });
            resetShowMessage();
            getAllBlogs();
        }

        if (errorCode) {
            notification['error']({
                message: 'Notificación',
                description: errorCode,
            });
            resetShowMessage();
        }
    })

    const convertTitle = (content) => {
        let title = content.substr(0, 30)
        return title.length >= 30 ? title + "..." : title
    }

    return (
        <div>
            {posts.map(post => (
                <Card key={post.post_id}>
                    <div className="px-3">
                        <h1>{convertTitle(post.content)}</h1>
                        <h4>{post.content}</h4>
                        <span>{moment(post.created_at).format('YYYY-MM-DD')}</span>
                    </div>
                    <Collapse
                        accordion={false}
                        defaultActiveKey={'faq-1-0'}
                        bordered={false}
                        expandIcon={({ isActive }) => <RightOutlined className="text-primary" type="right" rotate={isActive ? 90 : 0} />}
                    >
                        <Panel header={`Comentarios (${post.comments && post.comments.length})`}>
                            <FormComment
                                {...props}
                                post={post}
                                rules={rules}
                            />
                            {post.comments.map(comment => (
                                <Card key={comment.id_comment}>
                                    <Flex
                                        className="py-1"
                                        justifyContent="between"
                                        alignItems="center"
                                        key={comment.id_comment}
                                    >

                                        {updateComment === comment.id_comment ?
                                            <>

                                                <Form
                                                    layout="vertical"
                                                    form={formUpdate}
                                                    name={`update_comment-${comment.id_comment}`}
                                                    className="ant-advanced-search-form mr-3"
                                                    style={{ flex: 1 }}
                                                    onFinish={(e) => editComment({ ...e, postId: post.post_id })}
                                                >
                                                    <Form.Item
                                                        className="mb-1"
                                                        name={"comment-" + comment.id_comment}
                                                        rules={rules.content}
                                                        initialValue={comment.content || ""}
                                                    >
                                                        <Input.TextArea />
                                                    </Form.Item>

                                                    <Flex
                                                        className="py-2"
                                                        mobileFlex={false}
                                                        alignItems="center"
                                                    >
                                                        <Button
                                                            className="mr-1"
                                                            type="primary"
                                                            htmlType="submit"
                                                        >
                                                            Guardar
                                                        </Button>

                                                        <Button
                                                            className="ml-1"
                                                            type="primary"
                                                            onClick={() => setUpdateComment("")}
                                                            danger
                                                        >
                                                            Cancelar
                                                        </Button>
                                                    </Flex>
                                                </Form>
                                            </>
                                            :
                                            <div>
                                                <div className="mb-2">
                                                    <h4>{comment.user.name}</h4>
                                                    <Text>{comment.content}</Text>
                                                </div>
                                                <span>{moment(comment.created_at).format('YYYY-MM-DD')}</span>
                                            </div>
                                        }
                                        { comment.user_id == localStorage.getItem('user_id') &&
                                        <Flex className="ml-3">
                                            <Button
                                                className="mr-1"
                                                type="primary"
                                                icon={<EditOutlined />}
                                                onClick={() => onUpdateClick(comment.id_comment)}
                                            ></Button>
                                            <Popconfirm title={questionTitle} onConfirm={() => deleteComment(comment.id_comment)} icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                                                <Button
                                                    className="ml-2"
                                                    icon={<DeleteOutlined />}
                                                    type="primary"
                                                    danger
                                                ></Button>
                                            </Popconfirm>
                                        </Flex>
                                        }
                                    </Flex>
                                </Card>
                            ))}

                        </Panel>

                    </Collapse>
                </Card>
            ))}
        </div>
    )
}

const FormComment = (props) => {
    const {
        post,
        rules,
        commentInsert,
        resetShowMessage,
        message,
        errorCode,
        getAllBlogs
    } = props;

    const [formComment] = Form.useForm()

    const commentAdd = (values) => {
        commentInsert(values)
        formComment.resetFields()
    }

    useEffect(() => {
        if (message) {
            notification['success']({
                message: 'Notificación',
                description: message,
            });
            resetShowMessage();
            getAllBlogs();
        }

        if (errorCode) {
            notification['error']({
                message: 'Notificación',
                description: errorCode,
            });
            resetShowMessage();
        }
    })

    return (
        <Form
            layout="vertical"
            form={formComment}
            name="advanced_search"
            onFinish={(e) => commentAdd({ ...e, postId: post.post_id })}
            className="ant-advanced-search-form"
        >
            <Flex>
                <Form.Item
                    style={{ flex: 1 }}
                    name="content"
                    rules={rules.content}

                    className="mr-2"
                >
                    <Input />
                </Form.Item>
                <Button
                    type="primary"
                    htmlType="submit"
                >Responder</Button>
            </Flex>
        </Form>
    )
}

const mapStateToProps = ({ comments, blog }) => {
    const { message, showMessage, errorCode } = comments;
    const { posts } = blog;
    return { message, showMessage, errorCode, posts }
}

const mapDispatchToProps = {
    commentInsert,
    commentUpdate,
    commentDelete,
    resetShowMessage,
    getAllBlogs
}

export default connect(mapStateToProps, mapDispatchToProps)(CommentList)