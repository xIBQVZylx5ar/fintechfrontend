import React, { useEffect } from 'react';
import { LogoutOutlined } from '@ant-design/icons';
import { useHistory } from "react-router-dom";
import { connect } from "react-redux";
import { signOut } from 'redux/actions/Auth';
import { closeSession, isLogin } from 'configs/Session';

const SignOutOption = (props) => {

    const { signOut, redirect } = props;
    let history = useHistory();

    useEffect(() => {
        if (!isLogin() && redirect) {
            history.push(redirect)
        }
    });

    const signOutFunt = () => {
        closeSession();
        signOut();
    }

    return (
        <>
            <div onClick={signOutFunt}>
                <LogoutOutlined className="nav-icon mr-0" />
            </div>
        </>
    )
}

const mapStateToProps = ({ auth }) => {
    const { redirect } = auth;
    return { redirect };
}

const mapDispatchToProps = {
    signOut
}

export default connect(mapStateToProps, mapDispatchToProps)(SignOutOption)