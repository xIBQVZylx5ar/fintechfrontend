import {
  SHOW_MESSAGE,
  RESET_SHOW_MESSAGE,
  SHOW_AUTH_MESSAGE,
  TRXINSERT,
  TRXDELETE,
  GET_ALL_TRXS,
  RESPONSE_GET_ALL_TRXS,
  GET_TRX,
  RESPONSE_GET_TRX,
  TRXUPDATE,
  UPDATE_MESSAGE,
  GET_DESTINY_COMBO,
  GET_ORIGIN_COMBO,
  RESP_DESTINY_COMBO,
  RESP_ORIGIN_COMBO,
  GET_EXPENSE_COMBO,
  RESP_EXPENSE_COMBO,
  GET_RENTA,
  RESP_RENTA,
  GET_IVA,
  RESP_IVA,
  GET_CCSS,
  RESP_CCSS
} from '../constants/trx';

const initState = {
  loading: false,
  message: '',
  errorCode: '',
  showMessage: false,
  transactions: [],
  edtiabletrx: [],
  transaction: {},
  toUpdate: [],
  destinyCat: [],
  originCat: [],
  expenseCombo:[],
  renta:{},
  iva :{},
  ccss:{}

}

const trx = (state = initState, action) => {
  switch (action.type) {
    case TRXINSERT:
      return {
        ...state,
        message: action.message,
        showMessage: true,
        loading: true
      }

    case TRXDELETE:
      return {
        ...state,
        message: action.message,
        showMessage: true,
        loading: true
      }

    //para editar trxs 
    case TRXUPDATE:
      return {
        ...state,
        message: false,
        loading: true
      }

    case UPDATE_MESSAGE:
      return {
        ...state,
        message: action.response.successCode,
        showMessage: true,
        loading: false,
        errorCode: action.response.errorCode
      }

    //donde consulto
    case GET_ALL_TRXS:
      return {
        ...state,
        loading: true,
      }

    //donde me traigo la respuesta del server
    case RESPONSE_GET_ALL_TRXS:
      
      return {
        ...state,
        loading: false,
        transactions: action.payload
      }

    case GET_TRX:
      
      return {
        ...state,
        loading: true,
      }
    case GET_DESTINY_COMBO:
      return {
        ...state,
        loading: true,
      }
    case GET_ORIGIN_COMBO:
      return {
        ...state,
        loading: true,
      }
    case GET_EXPENSE_COMBO:
      return {
        ...state,
        loading: true,
      }
    case RESP_EXPENSE_COMBO:
      return {
        ...state,
        loading: false,
        expenseCombo: action.payload
      }
    case RESP_DESTINY_COMBO:
      return {
        ...state,
        loading: false,
        destinyCat: action.payload
      }
    case RESP_ORIGIN_COMBO:
      return {
        ...state,
        loading: false,
        originCat: action.payload
      }

    case RESPONSE_GET_TRX:
      return {
        ...state,
        loading: false,
        transaction: action.payload,
      }

    case SHOW_AUTH_MESSAGE:
      return {
        ...state,
        message: action.message,
        showMessage: true,
        loading: false
      }

    case SHOW_MESSAGE:
      return {
        ...state,
        message: action.response.successCode,
        showMessage: true,
        loading: false,
        errorCode: action.response.errorCode
      }

    case RESET_SHOW_MESSAGE:
      return {
        ...state,
        message: '',
        showMessage: true,
        loading: false,
        errorCode: ''
      }
      //taxation reducers
    case GET_RENTA:
      return {
        ...state,
        loading: true,
      }
    case RESP_RENTA:
      return {
        ...state,
        loading: false,
        renta: action.payload
      }

    case GET_IVA:
      return {
        ...state,
        loading: true,
      }

    case RESP_IVA:
      return {
        ...state,
        loading: false,
        iva: action.payload
      }

    case GET_CCSS:
      return {
        ...state,
        loading: true,
      }

    case RESP_CCSS:
      return {
        ...state,
        loading: false,
        ccss: action.payload
      }
    default:
      return state;
  }
}

export default trx

// commit para jose 