import {
  CATEGORIE_INSERT,
  RESET_SETTING_MESSAGE,
  SHOW_MESSAGE_CATEGORIES,
  CATEGORIE_CONSULT,
  RESP_CATEGORIE_CONSULT,
  GET_PROFILE,
  RESPONSE_GET_PROFILE,
  PROFILE_UPDATE,
  CHANGE_MAIL,
  SHOW_MESSAGE_SETTINGS,
  NEW_CATEGORY,
  GET_CATEGORIES,
  RESP_CATEGORIES,
  EDIT_CATEGORY,
  DETELE_CATEGORY

} from "../constants/settings";

const initState = {
  loadingSettings: false,
  message: "",
  errorCode: "",
  showMessage: false,
  redirect: "",
  categories: [],

};

const settings = (state = initState, action) => {
  switch (action.type) {

    case CATEGORIE_INSERT:

      return {
        ...state,
        message: "",
        showMessage: true,
        loadingSettings: true,
        errorCode: "",
      };

    case CATEGORIE_CONSULT:
      return {
        ...state,
        message: "",
        showMessage: true,
        loadingSettings: true,
        errorCode: "",
      };

    case RESP_CATEGORIE_CONSULT:
      return {
        ...state,
        message: action.response.successCode,
        showMessage: true,
        loadingSettings: false,
        errorCode: action.response.errorCode,
      };

    case RESET_SETTING_MESSAGE:
      return {
        ...state,
        message: "",
        showMessage: false,
        loadingSettings: false,
        errorCode: "",
      };

    case SHOW_MESSAGE_CATEGORIES:
      return {
        ...state,
        message: action.response.successCode,
        showMessage: true,
        loadingSettings: false,
        errorCode: action.response.errorCode,
      };

    case GET_PROFILE:
      return {
        ...state,
        message: '',
        showMessage: true,
        loadingSettings: true,
        errorCode: ''
      };

    case RESPONSE_GET_PROFILE:
      return {
        ...state,
        loadingSettings: false,
        profile: action.payload,
        //message: true
      };

    case PROFILE_UPDATE:
      return {
        ...state,
        message: '',
        showMessage: true,
        loadingSettings: true,
        errorCode: ''
      };

    case CHANGE_MAIL:
      return {
        ...state,
        message: '',
        showMessage: true,
        loadingSettings: true,
        errorCode: ''
      };

    case SHOW_MESSAGE_SETTINGS:
      return {
        ...state,
        message: action.response.successCode,
        showMessage: true,
        loadingSettings: false,
        errorCode: action.response.errorCode,
        profile: {}
      };

    /// lo nuevo sin andres

    case NEW_CATEGORY:

      return {
        ...state,
        message: "",
        showMessage: true,
        loadingSettings: true,
        errorCode: "",
      };
    case DETELE_CATEGORY:
      return {
        ...state,
        message: "",
        showMessage: true,
        loadingSettings: true,
        errorCode: "",
      };

    case EDIT_CATEGORY:

      return {
        ...state,
        message: "",
        showMessage: true,
        loadingSettings: true,
        errorCode: "",
      };

    case GET_CATEGORIES:

      return {
        ...state,
        loadingSettings: true,
      };

    case RESP_CATEGORIES:

      return {
        ...state,
        categories: action.payload,
        loadingSettings: false
      };

    default:
      return state;
  }
};

export default settings;
