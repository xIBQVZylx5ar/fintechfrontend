import {
    PAYROLLINSERT,
    PAYROLLUPDATE,
    PAYROLLDELETE,
    GET_ALL_PAYROLL,
    RESPONSE_GET_ALL_PAYROLL,
    GET_PAYROLL,
    RESPONSE_GET_PAYROLL,
    GET_ALL_COLLABORATORS,
    COLLABORATORINSERT,
    RESPONSE_GET_ALL_COLLABORATORS,
    SHOW_MESSAGE,
    RESET_SHOW_MESSAGE
} from "../constants/payroll"

const initState = {
    loading: false,
    message: '',
    errorCode: '',
    showMessage: false,
    redirect: '',
    data: {},
    payrolls: [],
    collaborators: [],
}

const payroll = (state = initState, action) => {
    switch(action.type){
        case PAYROLLINSERT:
            return {
                ...state,
                message: action.message,
                showMessage: true,
                loading: true
            }
        case PAYROLLUPDATE:
            return {
                ...state,
                message: action.message,
                showMessage: true,
                loading: true
            }
        case PAYROLLDELETE:
            return {
                ...state,
                message: action.message,
                showMessage: true,
                loading: true
            }
        case GET_ALL_PAYROLL: 
            return {
                ...state,
                loading: true
            }
        case RESPONSE_GET_ALL_PAYROLL:
            return {
                ...state,
                loading: false,
                payrolls: action.payload
            }
        case GET_PAYROLL:
            return {
                ...state,
                loading: true
            }
        case RESPONSE_GET_PAYROLL:
            return {
                ...state,
                loading: false,
                data: action.payload,
                message: true
            }
        case GET_ALL_COLLABORATORS:
            return {
                ...state,
                loading: true
            }
        case COLLABORATORINSERT:
            return {
                ...state,
                message: action.message,
                showMessage: true,
                loading: true
            }
        case RESPONSE_GET_ALL_COLLABORATORS:
            return {
                ...state,
                loading: false,
                collaborators: action.payload
            }
        case SHOW_MESSAGE: 
            return {
                ...state,
                message: action.response.successCode,
                showMessage: true,
                loading: false,
                errorCode : action.response.errorCode
            }
        case RESET_SHOW_MESSAGE: 
            return {
                ...state,
                message: '',
                showMessage: true,
                loading: false,
                errorCode : ''
            }
        default:
            return state
    }
}

export default payroll