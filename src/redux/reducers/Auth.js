import {
	SIGNIN,
	SIGNOUT,
	AUTHENTICATED,
	RESP_SIGNIN,
	HIDE_AUTH_MESSAGE,
	SIGNOUT_SUCCESS,
	SIGNUP_SUCCESS,
	SHOW_LOADING,
	REGISTER,
	RESP_REGISTER,
	SIGNIN_WITH_GOOGLE_AUTHENTICATED,
  	SIGNIN_WITH_FACEBOOK_AUTHENTICATED, 
	FORGOT_PASSWORD,
	SEND_MESSAGE, RESP_CHANGE_PASSWORD, CHANGE_PASSWORD, RESET_MESSAGE
} from '../constants/Auth';

const initState = {
	loading: false,
	message: '',
	showMessage: false,
	redirect: '',
	errorMessage: '',
	respInfo: '',
	psswdInfo: ''
}

const auth = (state = initState, action) => {
	switch (action.type) {
		case SIGNIN:
			return {
				...state,
				loading: true
			}
		case AUTHENTICATED:
			return {
				...state,
				loading: false,
				redirect: '/dashboards/home'
			}
		case SIGNOUT:
			return {
				...state,
				loading: false,
				redirect: '/auth/login',
				token: action.token
			}
		case RESP_SIGNIN: 
			return {
				...state,
				message: action.message,
				showMessage: true,
				loading: false
			}
		case HIDE_AUTH_MESSAGE: 
			return {
				...state,
				message: '',
				showMessage: false,
			}
		case SIGNOUT_SUCCESS: {
			return {
				...state,
				token: null,
				redirect: '/',
				loading: false
			}
		}
		case SIGNUP_SUCCESS: {
			return {
			  ...state,
			  loading: false,
			  token: action.token
			}
		}
		case SHOW_LOADING: {
			return {
				...state,
				loading: true
			}
		}
		case REGISTER: {
			return {
				...state,
				loading: true
			}
		}
		case RESP_REGISTER: 
			return {
				...state,
				message: action.message,
				errorMessage: action.errorMessage,
				redirect: 'dashboards/home',
				loading: false
			}
		case SIGNIN_WITH_GOOGLE_AUTHENTICATED: {
			return {
				...state,
				loading: false,
				token: action.token
			}
		}
		case SIGNIN_WITH_FACEBOOK_AUTHENTICATED: {
			return {
				...state,
				loading: false,
				token: action.token
			}
		}
		case FORGOT_PASSWORD: {
			return {
				...state, 
				loading : true
				
			}
		}
		case SEND_MESSAGE: {
			return {
				...state,
				loading: false,
				message: action.message.message,
				errorMessage: action.message.errorMessage
			}
		}
		case CHANGE_PASSWORD: {
			return {
				...state, 
				loading : true
				
			}
		}
		case RESP_CHANGE_PASSWORD: {
			return {
				...state,
				loading: false,
				message: action.message.message,
				errorMessage: action.message.errorMessage
			}
		}
		case RESET_MESSAGE: {
			return {
				loading: false,
				message: '',
				errorMessage: '',
			}
		}
		default:
			return state;
	}
}

export default auth