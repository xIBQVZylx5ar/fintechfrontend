
import { GET_BALANCE, RESP_BALANCE, RESET_BALANCE } from 'redux/constants/Balance'

const initState = {
  loading: false,
  databalance: {},
  psbalace: {}
}


const balance = (state = initState, action) => {
  switch (action.type) {
    case GET_BALANCE:
      return {
        ...state,
        loading: true
      }

    case RESP_BALANCE:
      return {
        ...state,
        loading: false,
        databalance: action.payload
      }

    case RESET_BALANCE:
      return {
        loading: false,
        databalance: {}
      }


    default:
      return state;
  }
}

export default balance;