import {
    BLOGINSERT,
    BLOGUPDATE,
    BLOGDELETE,
    GET_ALL_BLOGS,
    RESPONSE_GET_ALL_BLOGS,
    SHOW_MESSAGE,
    RESET_SHOW_MESSAGE,
} from "../constants/blog"

const initState = {
    loading: false,
    message: '',
    errorCode: '',
    showMessage: false,
    redirect: '',
    posts: [],
}

const blog = (state = initState, action) => {
    switch(action.type){
        case BLOGINSERT:
            return {
                ...state,
                message: action.message,
                showMessage: true,
                loading: true,
            }

        case BLOGUPDATE: 
            return {
                ...state,
                message: false,
                loading: true
            }

        case BLOGDELETE:
            return {
                ...state,
                message: action.message,
                showMessage: true,
                loading: true,
            }

        case GET_ALL_BLOGS:
            return {
                ...state,
                loading: true,
            }

        case RESPONSE_GET_ALL_BLOGS:  
            return {
                ...state,
                loading: false, 
                posts : action.payload
            }

        case SHOW_MESSAGE: 
            return {
                ...state,
                message: action.response.successCode,
                showMessage: true,
                loading: false,
                errorCode : action.response.errorCode
            }

        case RESET_SHOW_MESSAGE:
            return {
                ...state,
                message: '',
                showMessage: true,
                loading: false,
                errorCode : ''
            }

        default: 
            return state;
    }
}

export default blog;