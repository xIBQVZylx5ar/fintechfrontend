import { combineReducers } from 'redux';
import Auth from './Auth';
import Theme from './Theme';
import trx from './trx';
import settings from './settings';
import blog from './blog';
import comments from './comments';
import payroll from './payroll';
import balance from './Balance'


const reducers = combineReducers({
    theme: Theme,
    auth: Auth,
    trx: trx,
    settings: settings,
    blog: blog,
    comments: comments,
    payroll: payroll,
    balance : balance
});

export default reducers;