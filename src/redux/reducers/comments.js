import {
    COMMENTINSERT,
    COMMENTUPDATE,
    COMMENTDELETE,
    SHOW_MESSAGE,
    RESET_SHOW_MESSAGE,
} from "../constants/comments"

const initState = {
    loading: false,
    message: '',
    errorCode: '',
    showMessage: false,
    redirect: '',
}

const comments = (state = initState, action) => {
    switch(action.type){
        case COMMENTINSERT:
            return {
                ...state,
                message: action.message,
                showMessage: true,
                loading: true,
            }

        case COMMENTUPDATE: 
            return {
                ...state,
                message: false,
                loading: true
            }

        case COMMENTDELETE:
            return {
                ...state,
                message: action.message,
                showMessage: true,
                loading: true,
            }

        case SHOW_MESSAGE: 
            return {
                ...state,
                message: action.response.successCode,
                showMessage: true,
                loading: false,
                errorCode : action.response.errorCode
            }

        case RESET_SHOW_MESSAGE:
            return {
                ...state,
                message: '',
                showMessage: true,
                loading: false,
                errorCode : ''
            }

        default: 
            return state;
    }
}

export default comments;