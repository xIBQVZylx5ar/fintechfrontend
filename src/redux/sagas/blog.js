import { all, takeEvery, put, fork, call } from 'redux-saga/effects'; 
import { envBackend } from 'configs/EnvironmentConfig';
import { getAxiosHeaders } from 'configs/Session';
import axios from 'axios';

import { BLOGINSERT, BLOGUPDATE, BLOGDELETE, GET_ALL_BLOGS } from '../constants/blog';

import { showMessage, respGetAllBlog } from 'redux/actions/blog';

const endpoint = `${envBackend.API_ENDPOINT_URL}/blog`;


export function* insertNewBlog () {
    yield takeEvery(BLOGINSERT, function* ({ payload }) {
		let responseRedux = {
			'successCode' : undefined,
			'errorCode' : undefined
		}
		
        try{
            const responseCall = yield call(axios.post, `${endpoint}/new`, { ...payload }, getAxiosHeaders() )
            responseRedux['successCode'] = responseCall.data;
			yield put(showMessage(responseRedux));
        }catch(error){
            responseRedux['errorCode'] = error.response.data;
            yield put(showMessage(responseRedux));
        }
    })
}

export function* editBlog() {
	yield takeEvery(BLOGUPDATE, function* ({ payload }) {
		let responseRedux = {
			'successCode' : undefined,
			'errorCode' : undefined
		}

		try {
			const responseCall = yield call(axios.put, `${endpoint}/update`, { ...payload }, getAxiosHeaders() );

			responseRedux['successCode'] = responseCall.data;
			//yield put(showUptateMessage(responseRedux));
			yield put(showMessage(responseRedux));
		} catch (error) {
			responseRedux['errorCode'] = error.response.data;
			//yield put(showUptateMessage(responseRedux));
			yield put(showMessage(responseRedux));
		}
	});
}

export function* deleteBlog() {
	yield takeEvery(BLOGDELETE, function* ({ payload }) {
		let responseRedux = {
			'successCode' : undefined,
			'errorCode' : undefined
		}

		try {
			const responseCall = yield call(axios.delete, `${endpoint}/remove`, { params: {...payload} }, getAxiosHeaders() );

			responseRedux['successCode'] = responseCall.data;
			yield put(showMessage(responseRedux));
		} catch (error) {
			responseRedux['errorCode'] = error.response.data;
			yield put(showMessage(responseRedux));
		}
	});
}

export function* getAllBlogService() {
	yield takeEvery(GET_ALL_BLOGS, function* () {	

		try {
			const result = yield call(
				axios.get, `${endpoint}/`, null, getAxiosHeaders()
			);
			yield put(respGetAllBlog(result.data));
		} catch (error) {
			//yield put(showAuthMessage(error));
		}
	});
}

export default function* rootSaga(){
    yield all([
        fork(insertNewBlog),
		fork(editBlog),
		fork(deleteBlog),
        fork(getAllBlogService),
    ])
}