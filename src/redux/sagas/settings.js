import { all, takeEvery, put, fork, call } from "redux-saga/effects"; //call
import { envBackend } from "configs/EnvironmentConfig";
import { getAxiosHeaders, closeSession } from 'configs/Session'
import axios from "axios";

import { 
	GET_PROFILE,
	PROFILE_UPDATE,
	CHANGE_MAIL, 
	NEW_CATEGORY, 
	GET_CATEGORIES,
	EDIT_CATEGORY,
	DETELE_CATEGORY
} from "../constants/settings";

import {
  showMessageCategory,
  respUniqueProfile,
	showMessageSettings, 
	respCategories
} from "../actions/settings";
const endpoint = `${envBackend.API_ENDPOINT_URL}/settings`;





export function* getUniqueProfileService() {

	yield takeEvery(GET_PROFILE, function* ({ payload }) {
		try {
			const result = yield call(axios.get, `${endpoint}/profile/unique`, { params: null }, getAxiosHeaders());

			yield put(respUniqueProfile(result.data))
		} catch (error) {
		}
	})
}

export function* editProfileService() {
	yield takeEvery(PROFILE_UPDATE, function* ({ payload }) {
		let responseRedux = {
			"successCode": undefined,
			"errorCode": undefined
		}
		try {
			const responseCall = yield call(axios.put, `${endpoint}/profile/update`, { ...payload }, getAxiosHeaders);
			closeSession();
			responseRedux["successCode"] = responseCall.data;
			yield put(showMessageSettings(responseRedux))
		} catch (error) {
			responseRedux["errorCode"] = error.response.data;
			yield put(showMessageSettings(responseRedux))
		}
	})
}

export function* changeMailService() {
	yield takeEvery(CHANGE_MAIL, function* ({ payload }) {
		let responseRedux = {
			"successCode": undefined,
			"errorCode": undefined
		}

		try {
			const responseCall = yield call(axios.put, `${endpoint}/profile/change-mail`, { ...payload }, getAxiosHeaders());

			responseRedux["successCode"] = responseCall.data;
			yield put(showMessageSettings(responseRedux))
		} catch (error) {
			responseRedux["errorCode"] = error.response.data;
			yield put(showMessageSettings(responseRedux))
 		}
	})
}

// lo nuevo 

export function* insertNewCategory() {

	let responseRedux = {
		'successCode' : undefined,
		'errorCode' : undefined
	}
	yield takeEvery(NEW_CATEGORY, function* ({ payload }) {
		
		try {
			const responseCall = yield call(axios.post, `${endpoint}/categories/new`, { ...payload }, getAxiosHeaders());

			responseRedux['successCode'] = responseCall.data;
			yield put(showMessageCategory(responseRedux));
		} catch (error) {
			responseRedux['errorCode'] = error.response.data;
			yield put(showMessageCategory(responseRedux));
		}
	});
}
export function* deleteCategoryService() {

	let responseRedux = {
		'successCode' : undefined,
		'errorCode' : undefined
	}
	yield takeEvery(DETELE_CATEGORY, function* ({ payload }) {
		
		try {
			const responseCall = yield call(axios.delete, `${endpoint}/categories/remove`, { params: {...payload} }, getAxiosHeaders());
			responseRedux['successCode'] = responseCall.data;
			yield put(showMessageCategory(responseRedux));
		} catch (error) {
			responseRedux['errorCode'] = error.response.data;
			yield put(showMessageCategory(responseRedux));
		}
	});
}
export function* editCategoryService() {

	let responseRedux = {
		'successCode' : undefined,
		'errorCode' : undefined
	}
	yield takeEvery(EDIT_CATEGORY, function* ({ payload }) {
		try {
			
			const responseCall = yield call(axios.put, `${endpoint}/categories/update`, { ...payload }, getAxiosHeaders());
			responseRedux['successCode'] = responseCall.data;
			yield put(showMessageCategory(responseRedux));
		} catch (error) {
			responseRedux['errorCode'] = error.response.data;
			yield put(showMessageCategory(responseRedux));
		}
	});
}

export function* getCategoriesService(){
	yield takeEvery(GET_CATEGORIES, function*({userInfo}){
		
		try {
			const result = yield call(
				axios.get, `${endpoint}/categories/`, { params: userInfo } , getAxiosHeaders()
			);
			
			yield put(respCategories(result.data));
		} catch (error) {
			
			yield put(showMessageCategory(error));
		}
	});
 }

export default function* rootSaga() {
  yield all([
	fork(insertNewCategory),
    fork(getUniqueProfileService),
    fork(editProfileService),
    fork(changeMailService),
	fork(getCategoriesService),
	fork(editCategoryService),
	fork(deleteCategoryService)
  ]);
}
