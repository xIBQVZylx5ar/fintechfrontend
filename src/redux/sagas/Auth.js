import { all, takeEvery, put, fork, call } from 'redux-saga/effects';
import { envBackend } from 'configs/EnvironmentConfig';
import {
	SIGNIN,
	REGISTER,
	FORGOT_PASSWORD,
	CHANGE_PASSWORD

} from '../constants/Auth';
import {
	respSignIn,
	respRegister,
	authenticated,
	respRestore
} from "../actions/Auth";
import axios from 'axios';
import { createSession } from 'configs/Session';

const endpoint = `${envBackend.API_ENDPOINT_URL}/auth`

export function* signInWithEmail() {
	yield takeEvery(SIGNIN, function* ({ payload }) {
		try {

			const response = yield call(axios.post, `${endpoint}/login`, { ...payload });
			if (response.data.message) {
				yield put(respSignIn(response.data.message));
			} else {
				const dataResponse = response.data;
				createSession(dataResponse.tokens.token, dataResponse.profiles, dataResponse.user.user_type, dataResponse.user.id);
				yield put(authenticated());
			}
		} catch (err) {
			yield put(respSignIn(err.response.data.message));
		}
	});
}

export function* register() {

	let responseRedux = {
		'successCode': undefined,
		'errorCode': undefined
	}
	yield takeEvery(REGISTER, function* ({ payload }) {
		try {
			const response = yield call(axios.post, `${endpoint}/register`, { ...payload });

			const dataResponse = response.data;
			createSession(dataResponse.tokens.token, dataResponse.profiles, dataResponse.user.user_type, dataResponse.user.id);
			yield put(authenticated());
		} catch (error) {
			responseRedux['errorCode'] = error.response.data;
			yield put(respRegister(responseRedux));
		}
	});
}
export function* forgotPasswordService() {
	let responseRedux = {
		'message': undefined,
		'errorMessage': undefined
	}

	yield takeEvery(FORGOT_PASSWORD, function* ({ payload }) {
		try {
			const response = yield call(axios.post, `${endpoint}/restore/`, { ...payload });

			if (response) {

				responseRedux['message'] = response.data

				yield put(respRestore(responseRedux))
			}

		} catch (err) {
			responseRedux['errorMessage'] = err.response.data
			yield put(respRestore(responseRedux))
		}
	})
}
export function* changePasswordService() {
	let responseRedux = {
		'message': undefined,
		'errorMessage': undefined
	}
	yield takeEvery(CHANGE_PASSWORD, function* ({ payload }) {

		try {
			const response = yield call(axios.post, `${endpoint}/changepsswrd`, { ...payload });

			if (response) {
				responseRedux['message'] = response.data
				yield put(respRestore(responseRedux))
			}

		} catch (err) {
			responseRedux['errorMessage'] = err.response.data
			yield put(respRestore(responseRedux))
		}
	})
}





export default function* rootSaga() {
	yield all([
		fork(signInWithEmail),
		fork(register),
		fork(forgotPasswordService),
		fork(changePasswordService)
	]);
}
