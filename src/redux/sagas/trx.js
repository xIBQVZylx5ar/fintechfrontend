import { all, takeEvery, put, fork, call } from 'redux-saga/effects'; //call
import { envBackend } from 'configs/EnvironmentConfig';
import axios from 'axios';
import { getAxiosHeaders } from 'configs/Session'

import { TRXINSERT, GET_ALL_TRXS, GET_TRX, TRXUPDATE, TRXDELETE, GET_DESTINY_COMBO, GET_ORIGIN_COMBO, GET_EXPENSE_COMBO, GET_RENTA, GET_IVA, GET_CCSS } from '../constants/trx';

import { showAuthMessage, showMessage , respGetAllTrx, respUniqueTrx, showUptateMessage, resDestinyCombo, resOriginCombo, resExpenseCombo, respRenta, respIVA, respCCSS} from "../actions/trx"; // trxOk

const endpoint = `${envBackend.API_ENDPOINT_URL}/transaction`

export function* insertNewTrx() {

	let responseRedux = {
		'successCode' : undefined,
		'errorCode' : undefined
	}
	yield takeEvery(TRXINSERT, function* ({ payload }) {
		try {
			const responseCall = yield call(axios.post, `${endpoint}/new`, { ...payload }, getAxiosHeaders());

			responseRedux['successCode'] = responseCall.data;
			yield put(showMessage(responseRedux));
		} catch (error) {
			responseRedux['errorCode'] = error.response.data;
			yield put(showMessage(responseRedux));
		}
	});
}

export function* editTrx() {
	
	let responseRedux = {
		'successCode' : undefined,
		'errorCode' : undefined
	}
	yield takeEvery(TRXUPDATE, function* ({ payload }) {
		try {
			const responseCall = yield call(axios.put, `${endpoint}/update`, { ...payload }, getAxiosHeaders());

			responseRedux['successCode'] = responseCall.data;
			yield put(showUptateMessage(responseRedux));
		} catch (error) {
			responseRedux['errorCode'] = error.response.data;
			yield put(showUptateMessage(responseRedux));
		}
	});
}

export function* deleteTransaction() {

	let responseRedux = {
		'successCode' : undefined,
		'errorCode' : undefined
	}
	yield takeEvery(TRXDELETE, function* ({ payload }) {

		try {
			const responseCall = yield call(axios.delete, `${endpoint}/remove`, { params: {...payload} }, getAxiosHeaders());
			responseRedux['successCode'] = responseCall.data;
			yield put(showMessage(responseRedux));
		} catch (error) {
			responseRedux['errorCode'] = error.response.data;
			yield put(showMessage(responseRedux));
		}
	});
}

export function* getAllTrxService() {
	yield takeEvery(GET_ALL_TRXS, function* ({ userInfo }) {	
		
		try {
			const result = yield call(
				axios.get, `${endpoint}/`, { params: userInfo }, getAxiosHeaders()
			);
			
			yield put(respGetAllTrx(result.data));
		} catch (error) {
			
			yield put(showAuthMessage(error));
		}
	});
 }
export function* getDestinyComboService() {
	yield takeEvery(GET_DESTINY_COMBO, function* ({ userInfo }) {	
		

		try {
			const result = yield call(
				axios.get, `${endpoint}/combos`, { params: userInfo }, getAxiosHeaders()
			);
			
			yield put(resDestinyCombo(result.data));
		} catch (error) {
		
			yield put(showAuthMessage(error));
		}
	});
 }
export function* getExpenseComboService() {
	yield takeEvery(GET_EXPENSE_COMBO, function* ({ userInfo }) {	
		

		try {
			const result = yield call(
				axios.get, `${endpoint}/combos`, { params: userInfo }, getAxiosHeaders()
			);
			
			yield put(resExpenseCombo(result.data));
		} catch (error) {
		
			yield put(showAuthMessage(error));
		}
	});
 }
export function* getOriginComboService() {
	yield takeEvery(GET_ORIGIN_COMBO, function* ({ userInfo }) {	
		

		try {
			const result = yield call(
				axios.get, `${endpoint}/combos`, { params: userInfo }, getAxiosHeaders()
			);
			
			yield put(resOriginCombo(result.data));
		} catch (error) {
			
			yield put(showAuthMessage(error));
		}
	});
 }

 export function* getTrx() {
	 
	yield takeEvery(GET_TRX, function* ({ trxInfo }) {
		
	   try {
		  const result = yield call(
			 axios.get, `${endpoint}/unique`, { params: trxInfo }, getAxiosHeaders()
		  );
		  yield put(respUniqueTrx(result.data));
	   } catch (error) {
		  yield put(showAuthMessage(error));
	   }
	});
 }

 export function* genteRentaService() {
	yield takeEvery(GET_RENTA, function* ({ userInfo }) {	
		
		try {
			const result = yield call(
				axios.get, `${endpoint}/renta`, { params: userInfo }, getAxiosHeaders()
			);
			
			yield put(respRenta(result.data));
		} catch (error) {
			
			yield put(showAuthMessage(error));
		}
	});
 }
 export function* getIVAService() {
	yield takeEvery(GET_IVA, function* ({ userInfo }) {	
		
		try {
			const result = yield call(
				axios.get, `${endpoint}/iva`, { params: userInfo }, getAxiosHeaders()
			);
			
			yield put(respIVA(result.data));
		} catch (error) {
			
			yield put(showAuthMessage(error));
		}
	});
 }
 export function* getCCSSService() {
	yield takeEvery(GET_CCSS, function* ({ userInfo }) {	
		
		try {
			const result = yield call(
				axios.get, `${endpoint}/ccss`, { params: userInfo }, getAxiosHeaders()
			);
			
			yield put(respCCSS(result.data));
		} catch (error) {
			
			yield put(showAuthMessage(error));
		}
	});
 }

export default function* rootSaga() {
	yield all([
		fork(insertNewTrx),
		fork(getAllTrxService), 
		fork(getTrx),
		fork(editTrx),
		fork(deleteTransaction),
		fork(getDestinyComboService),
		fork(getOriginComboService),
		fork(getExpenseComboService),
		fork(genteRentaService),
		fork(getIVAService),
		fork(getCCSSService)
	]);
}

// commit para jose 