import { all } from 'redux-saga/effects';
import Auth from './Auth';
import trx from './trx';
import settings from './settings';
import blog from './blog';
import comments from './comments';
import payroll from './payroll';
import balance from './Balance'

export default function* rootSaga(getState) {
  yield all([
    Auth(),
    trx(),
    settings(),
    blog(),
    comments(),
    payroll(),
    balance()
  ]);
}
