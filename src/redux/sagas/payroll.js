import { all, takeEvery, put, fork, call } from 'redux-saga/effects'; 
import { envBackend } from 'configs/EnvironmentConfig';
import { getAxiosHeaders } from 'configs/Session';
import axios from 'axios';

import { 
    PAYROLLINSERT, 
    PAYROLLDELETE,
    PAYROLLUPDATE,
    GET_ALL_PAYROLL, 
    GET_PAYROLL,
    COLLABORATORINSERT,
    GET_ALL_COLLABORATORS 
} from '../constants/payroll';
import { respGetAllPayrolls, respUniquePayroll, respGetAllCollaborators, showMessage } from 'redux/actions/payroll';

const endpoint = `${envBackend.API_ENDPOINT_URL}/planilla`;

export function* insertNewPayroll(){
    yield takeEvery(PAYROLLINSERT, function* ({payload}){
        let responseRedux = {
            "successCode": undefined,
            "erorrCode": undefined,
        }

        try{
            const responseCall = yield call( axios.post, `${endpoint}/new`, { ...payload }, getAxiosHeaders() )
            responseRedux["successCode"] = responseCall.data;
            yield put(showMessage(responseRedux));
        } catch(error){
            responseRedux["erorrCode"] = error.response.data;
            yield put(showMessage(responseRedux));   
        }
    })   
}

export function* editPayroll(){
    yield takeEvery(PAYROLLUPDATE, function* ({payload}){
        let responseRedux = {
            "successCode": undefined,
            "erorrCode": undefined,
        }

        try{
            const responseCall = yield call( axios.put, `${endpoint}/update`, { ...payload }, getAxiosHeaders() )
            responseRedux["successCode"] = responseCall.data;
            yield put(showMessage(responseRedux));
        } catch(error){
            responseRedux["erorrCode"] = error.response.data;
            yield put(showMessage(responseRedux));   
        }
    })   
}

export function* deletePayroll(){
    yield takeEvery(PAYROLLDELETE, function* ({ payload }){
        let responseRedux = {
            "successCode": undefined,
            "errorCode": undefined
        }

        try{
            const responseCall = yield call( axios.delete, `${endpoint}/remove`, { params: {...payload} }, getAxiosHeaders() );

			responseRedux['successCode'] = responseCall.data;
			yield put(showMessage(responseRedux));
        } catch(error){
            responseRedux["errorCode"] = error.response.data;
            yield put(showMessage(responseRedux))
        }
    })
}

export function* getAllPayrollService() {
    yield takeEvery(GET_ALL_PAYROLL, function* ({ userInfo }) {

        try {
            const result = yield call ( axios.get, `${endpoint}/`, { params: userInfo }, getAxiosHeaders() )
            
            yield put(respGetAllPayrolls(result.data));
        } catch(error){
        }
    })
}

export function* getPayroll(){
    yield takeEvery(GET_PAYROLL, function* ({ payrollInfo }) {
        try {
            const result = yield call( axios.get, `${endpoint}/unique`, { params: payrollInfo }, getAxiosHeaders() );

           yield put(respUniquePayroll(result.data));
        } catch (error) {
           //yield put(showAuthMessage(error));
        }
     });
}

export function* insertNewCollaborator(){

    yield takeEvery(COLLABORATORINSERT, function* ({payload}){
        let responseRedux = {
            "successCode": undefined,
            "errorCode": undefined
        }

        try{
            const responseCall = yield call( axios.post, `${endpoint}/collaborator`, { ...payload }, getAxiosHeaders() )
            responseRedux["successCode"] = responseCall.data;
            yield put(showMessage(responseRedux));
        } catch(error){
            responseRedux["erorrCode"] = error.response.data;
            yield put(showMessage(responseRedux));   
        }
    })   
}

export function* getAllPayrollCollaborators() {
	yield takeEvery(GET_ALL_COLLABORATORS, function* ({ userInfo }) {	

		try {
			const result = yield call( axios.get, `${endpoint}/collaborator`, {params: userInfo}, getAxiosHeaders() );
			yield put(respGetAllCollaborators(result.data));
		} catch (error) {
			//yield put(showAuthMessage(error));
		}
	});
}

export default function* roorSaga(){
    yield all([
        fork(insertNewPayroll),
        fork(editPayroll),
        fork(deletePayroll),
        fork(getAllPayrollService),
        fork(getPayroll),
        fork(insertNewCollaborator),
        fork(getAllPayrollCollaborators)
    ])
}