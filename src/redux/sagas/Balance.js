import { all, takeEvery, put, fork, call } from 'redux-saga/effects'; 
import { envBackend } from 'configs/EnvironmentConfig';
import axios from 'axios';

import { GET_BALANCE } from '../constants/Balance';
import { resBalance } from '../actions/Balance';
import { getAxiosHeaders } from 'configs/Session'

const endpoint = `${envBackend.API_ENDPOINT_URL}/balance`;


export function* getlBalanceService() {
	yield takeEvery(GET_BALANCE, function* ({ userInfo }) {
	
		try {
			const result = yield call(
				axios.get, `${endpoint}/`, { params: userInfo }, getAxiosHeaders()
			);
			
			yield put(resBalance(result.data[0]));
		} catch (error) {
			//yield put(showAuthMessage(error));
		}
	});
}



export default function* rootSaga() {
	yield all([
		fork(getlBalanceService)
	]);
}
