import { all, takeEvery, put, fork, call } from 'redux-saga/effects'; 
import { envBackend } from 'configs/EnvironmentConfig';
import { getAxiosHeaders } from 'configs/Session';
import axios from 'axios';

import { COMMENTINSERT, COMMENTUPDATE, COMMENTDELETE } from '../constants/comments';

import { showMessage } from 'redux/actions/comments';

const endpoint = `${envBackend.API_ENDPOINT_URL}/comments`;

export function* insertNewComment () {
    yield takeEvery(COMMENTINSERT, function* ({ payload }) {
		let responseRedux = {
			'successCode' : undefined,
			'errorCode' : undefined
		}
		
        try{
            const responseCall = yield call(axios.post, `${endpoint}/new`, { ...payload }, getAxiosHeaders()  )
            responseRedux['successCode'] = responseCall.data;
			yield put(showMessage(responseRedux));
        }catch(error){
            responseRedux['errorCode'] = error.response.data;
            yield put(showMessage(responseRedux));
        }
    })
}

export function* editComment() {
	yield takeEvery(COMMENTUPDATE, function* ({ payload }) {
		let responseRedux = {
			'successCode' : undefined,
			'errorCode' : undefined
		}

		try {
			const responseCall = yield call(axios.put, `${endpoint}/update`, { ...payload }, getAxiosHeaders() );

			responseRedux['successCode'] = responseCall.data;
			//yield put(showUptateMessage(responseRedux));
			yield put(showMessage(responseRedux));
		} catch (error) {
			responseRedux['errorCode'] = error.response.data;
			//yield put(showUptateMessage(responseRedux));
			yield put(showMessage(responseRedux));
		}
	});
}

export function* deleteComment() {
	yield takeEvery(COMMENTDELETE, function* ({ payload }) {
		let responseRedux = {
			'successCode' : undefined,
			'errorCode' : undefined
		}
		
		try {
			const responseCall = yield call(axios.delete, `${endpoint}/remove`, { params: {...payload} }, getAxiosHeaders());

			responseRedux['successCode'] = responseCall.data;
			yield put(showMessage(responseRedux));
		} catch (error) {
			responseRedux['errorCode'] = error.response.data;
			yield put(showMessage(responseRedux));
		}
	});
}

export default function* rootSaga(){
    yield all([
        fork(insertNewComment),
		fork(editComment),
		fork(deleteComment),
    ])
}