
import {
  TRXINSERT, 
  GET_ALL_TRXS,
  GET_TRX,
  RESPONSE_GET_ALL_TRXS,
  RESPONSE_GET_TRX, 
  SHOW_AUTH_MESSAGE, 
  SHOW_MESSAGE, 
  RESET_SHOW_MESSAGE,
  TRXUPDATE, 
  UPDATE_MESSAGE, 
  TRXDELETE,
  GET_DESTINY_COMBO,
  GET_ORIGIN_COMBO,
  RESP_DESTINY_COMBO,
  RESP_ORIGIN_COMBO,
  GET_EXPENSE_COMBO,
  RESP_EXPENSE_COMBO, 
  GET_RENTA,
  RESP_RENTA,
  GET_IVA,
  RESP_IVA,
  GET_CCSS,
  RESP_CCSS
} from '../constants/trx';

export const trxInsert = (trxInfo) => {
  
  return {
    type: TRXINSERT,
    payload: trxInfo
  }
};

export const trxDelete = (payload) => {
  
  return {
    type: TRXDELETE,
    payload
  }
};

//llamado total de datos
export const getAllTrx = (userInfo) => {
  return {
    type: GET_ALL_TRXS,
    userInfo
  }
};

export const respGetAllTrx = (respTrx) => {
  return{
    type: RESPONSE_GET_ALL_TRXS,
    payload : respTrx
  }
};

// llamados para combobox
export const getDestinyCombo  =(userInfo) =>{
  
  return{
    type: GET_DESTINY_COMBO,
    userInfo
  }
}
export const getOriginCombo  =(userInfo) =>{
  return{
    type: GET_ORIGIN_COMBO,
    userInfo
  }
}
export const getExpenseCombo  =(userInfo) =>{
  return{
    type: GET_EXPENSE_COMBO,
    userInfo
  }
}
export const resExpenseCombo = (resExpense) =>{
  
  return{
    type: RESP_EXPENSE_COMBO,
    payload: resExpense
  }
}
export const resDestinyCombo = (resDestiny) =>{
  
  return{
    type: RESP_DESTINY_COMBO,
    payload: resDestiny
  }
}
export const resOriginCombo = (resOrigin) =>{
  return{
    type: RESP_ORIGIN_COMBO,
    payload: resOrigin
  }
}

//llamado de datos unicos 
 export const getUniqueTrx = (trxInfo) => {
   
  return {
    type: GET_TRX,
    trxInfo
  }
};
export const respUniqueTrx = (respTrx) => {
  
  return {
    type: RESPONSE_GET_TRX,
    payload : respTrx
  }
};

///Para actualizar

export const updateTrx = (updateInfo) => {
  return {
    type: TRXUPDATE,
    payload: updateInfo
  }
};

export const showUptateMessage = (response) => {
  return {
    type: UPDATE_MESSAGE,
    response
  }
}

export const showMessage = (response) => {
  return {
    type: SHOW_MESSAGE,
    response
  }
};

export const resetShowMessage = () => {
  return {
    type: RESET_SHOW_MESSAGE
  }
};

export const showAuthMessage = (message) => {
  return {
    type: SHOW_AUTH_MESSAGE,
    message
  };
};

export const getRenta =(userInfo) => {
  
  return {
    type: GET_RENTA,
    userInfo
  }
  
};

export const respRenta = (respTrx) => {
  return{
    type: RESP_RENTA,
    payload : respTrx
  }
}
export const getIVA =(userInfo) => {
  
  return {
    type: GET_IVA,
    userInfo
  }
  
};

export const respIVA = (respTrx) => {
  return{
    type: RESP_IVA,
    payload : respTrx
  }
}
export const getCCSS = (userInfo) => {  
  return {
    type: GET_CCSS,
    userInfo
  }
  
};

export const respCCSS = (respTrx) => {
  return{
    type: RESP_CCSS,
    payload : respTrx
  }
}



// commit para jose 