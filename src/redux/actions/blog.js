import { 
  BLOGINSERT, 
  BLOGUPDATE,
  BLOGDELETE,
  GET_ALL_BLOGS,
  RESPONSE_GET_ALL_BLOGS,
  SHOW_MESSAGE,
  RESET_SHOW_MESSAGE,
} from "../constants/blog";

export const blogInsert = (blogInfo) => {
  return {
      type: BLOGINSERT,
      payload: blogInfo
  }
}   

export const blogUpdate = (updateInfo) => {
  return {
    type: BLOGUPDATE,
    payload: updateInfo
  }
}

export const blogDelete = (payload) => {
  return {
    type: BLOGDELETE,
    payload
  }
}

export const getAllBlogs = (userInfo) => {
  return {
    type: GET_ALL_BLOGS,
    userInfo
  }
}

export const respGetAllBlog = (respBlog) => {
  return {
    type: RESPONSE_GET_ALL_BLOGS,
    payload: respBlog
  }
}

export const showMessage = (response) => {
  return {
    type: SHOW_MESSAGE,
    response
  }
};

export const resetShowMessage = () => {
    return {
      type: RESET_SHOW_MESSAGE
    }
};
  