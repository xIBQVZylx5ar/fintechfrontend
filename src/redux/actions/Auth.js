import {
  SIGNIN,
  AUTHENTICATED,
  SIGNOUT,
  SIGNOUT_SUCCESS,
  RESP_SIGNIN,
  HIDE_AUTH_MESSAGE,
  SIGNUP,
  SIGNUP_SUCCESS,
  SHOW_LOADING,
  REGISTER,
  RESP_REGISTER,
  SIGNIN_WITH_GOOGLE,
  SIGNIN_WITH_GOOGLE_AUTHENTICATED,
  SIGNIN_WITH_FACEBOOK,
  SIGNIN_WITH_FACEBOOK_AUTHENTICATED,
  FORGOT_PASSWORD,
  SEND_MESSAGE,
  CHANGE_PASSWORD, 
  RESP_CHANGE_PASSWORD, 
  RESET_MESSAGE
} from '../constants/Auth';

export const signIn = (user) => {
  return {
    type: SIGNIN,
    payload: user
  }
};

export const authenticated = () => {
  return {
    type: AUTHENTICATED
  }
};

export const signOut = () => {
  return {
    type: SIGNOUT
  };
};

export const signOutSuccess = () => {
  return {
    type: SIGNOUT_SUCCESS,
  }
};

export const signUp = (user) => {
  return {
    type: SIGNUP,
    payload: user
  };
};

export const signUpSuccess = (token) => {
  return {
    type: SIGNUP_SUCCESS,
    token
  };
};

export const signInWithGoogle = () => {
  return {
    type: SIGNIN_WITH_GOOGLE
  };
};

export const signInWithGoogleAuthenticated = (token) => {
  return {
    type: SIGNIN_WITH_GOOGLE_AUTHENTICATED,
    token
  };
};

export const signInWithFacebook = () => {
  return {
    type: SIGNIN_WITH_FACEBOOK
  };
};

export const signInWithFacebookAuthenticated = (token) => {
  return {
    type: SIGNIN_WITH_FACEBOOK_AUTHENTICATED,
    token
  };
};

export const respSignIn = (message) => {
  return {
    type: RESP_SIGNIN,
    message
  };
};

export const hideAuthMessage = () => {
  return {
    type: HIDE_AUTH_MESSAGE,
  };
};

export const showLoading = () => {
  return {
    type: SHOW_LOADING,
  };
};

export const register = (payload) => {
  return {
    type: REGISTER,
    payload
  };
}

export const respRegister = () => {
  return {
    type: RESP_REGISTER,
  };
}
export const restorePassword = (userInfo) => {
  return {
    type: FORGOT_PASSWORD, 
    payload: userInfo

  }
};

export const respRestore = (message) => {
  
  return{
    type: SEND_MESSAGE,
    message
  }
};
export const changePassword = (userInfo) => {
  
  return {
    type: CHANGE_PASSWORD, 
    payload: userInfo

  }
};

export const respchangePassword = (psswdInfo) => {
  
  return{
    type: RESP_CHANGE_PASSWORD,
    psswdInfo
  }
};
export const resetMessage = () => {
  
  return{
    type: RESET_MESSAGE,
    
  }
};
