import { 
    COMMENTINSERT, 
    COMMENTUPDATE,
    COMMENTDELETE,
    SHOW_MESSAGE,
    RESET_SHOW_MESSAGE,
  } from "../constants/comments";
  
  export const commentInsert = (commentInfo) => {
    return {
        type: COMMENTINSERT,
        payload: commentInfo
    }
  }   
  
  export const commentUpdate = (updateInfo) => {
    return {
      type: COMMENTUPDATE,
      payload: updateInfo
    }
  }
  
  export const commentDelete = (payload) => {
    return {
      type: COMMENTDELETE,
      payload
    }
  }
  
  export const showMessage = (response) => {
    return {
      type: SHOW_MESSAGE,
      response
    }
  };
  
  export const resetShowMessage = () => {
      return {
        type: RESET_SHOW_MESSAGE
      }
  };
    