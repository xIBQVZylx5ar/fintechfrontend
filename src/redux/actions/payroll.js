import {
    PAYROLLINSERT,
    PAYROLLUPDATE,
    PAYROLLDELETE,
    GET_ALL_PAYROLL,
    RESPONSE_GET_ALL_PAYROLL,
    GET_PAYROLL,
    RESPONSE_GET_PAYROLL,
    COLLABORATORINSERT,
    GET_ALL_COLLABORATORS,
    RESPONSE_GET_ALL_COLLABORATORS,
    SHOW_MESSAGE,
    RESET_SHOW_MESSAGE
} from "../constants/payroll"

export const payrollInsert = (payrollInfo) => {
    return {
        type: PAYROLLINSERT,
        payload: payrollInfo
    }
}

export const payrollUpdate = (updateInfo) => {
    return {
        type: PAYROLLUPDATE,
        payload: updateInfo
    }
}

export const payrollDelete = (payload) => {
    return {
        type: PAYROLLDELETE,
        payload
    }
}

export const getAllPayrolls = (userInfo) => {
    
    return {
        type: GET_ALL_PAYROLL,
        userInfo
    }
}

export const respGetAllPayrolls = (respPayrolls) => {
    return {
        type: RESPONSE_GET_ALL_PAYROLL,
        payload: respPayrolls
    }
}

export const getUniquePayroll = (payrollInfo) => {
    return {
        type: GET_PAYROLL,
        payrollInfo
    }
}

export const respUniquePayroll = (respPayroll) => {
    return {
        type: RESPONSE_GET_PAYROLL,
        payload: respPayroll
    }
}

export const collaboratorInsert = (collaboratorInfo) => {
    return {
        type: COLLABORATORINSERT,
        payload: collaboratorInfo
    }
}

export const getAllCollaborators = (userInfo) => {
    return {
        type: GET_ALL_COLLABORATORS,
        userInfo
    }
}

export const respGetAllCollaborators = (respCollaborators) => {
    return {
        type: RESPONSE_GET_ALL_COLLABORATORS,
        payload: respCollaborators
    }
}

export const showMessage = (response) => {
    return {
      type: SHOW_MESSAGE,
      response
    }
  };
  
  export const resetShowMessage = () => {
      return {
        type: RESET_SHOW_MESSAGE
      }
  };