import {GET_BALANCE, RESP_BALANCE, RESET_BALANCE} from "../constants/Balance";


export const getBalance = (userInfo) => {
  
  return {
    type: GET_BALANCE,
    userInfo
  }
};

export const resBalance = (respBalance) =>{
  return{
    type: RESP_BALANCE,
    payload: respBalance
  }
};

export const resetBalance = () =>{
  return{
    type: RESET_BALANCE
  }
}