
import { 
        RESET_SETTING_MESSAGE,
        SHOW_MESSAGE_CATEGORIES,
        GET_PROFILE,
        RESPONSE_GET_PROFILE,
        PROFILE_UPDATE,
        CHANGE_MAIL,
        SHOW_MESSAGE_SETTINGS,
        NEW_CATEGORY,
        GET_CATEGORIES,
        RESP_CATEGORIES, 
        EDIT_CATEGORY,
        DETELE_CATEGORY
      }
 from '../constants/settings';




export const showMessageCategory = (response) => {
  return {
    type: SHOW_MESSAGE_CATEGORIES,
    response
  }
};

export const resetSettingMessage = () => {
    return {
      type: RESET_SETTING_MESSAGE
    }
};

export const getProfile = () => {
  return {
    type: GET_PROFILE
  }
}

export const respUniqueProfile = (response) => {
  return {
    type: RESPONSE_GET_PROFILE,
    payload: response
  }
}

export const updateProfile = (info) => {
  return {
    type: PROFILE_UPDATE,
    payload: info
  }
}

export const changeMail = (info) => {
  return {
    type: CHANGE_MAIL,
    payload: info
  }
}

export const showMessageSettings = (response) => {
  return {
    type: SHOW_MESSAGE_SETTINGS,
    response
  }
}

///LO NUEVO 

export const newCategory = (trxInfo) => {
  return {
    type: NEW_CATEGORY,
    payload: trxInfo
  }
};

export const getCategories = () => {
  
  return {
    type: GET_CATEGORIES,
    
  }
};

export const respCategories = (respCat) => {
  return{
    type: RESP_CATEGORIES,
    payload : respCat
  }
};

export const editCategory = (category_info) => {
  return{
    type: EDIT_CATEGORY,
    payload : category_info
  }
}

export const deteleCategory = (trxInfo) => {
  return {
    type: DETELE_CATEGORY,
    payload: trxInfo
  }
};