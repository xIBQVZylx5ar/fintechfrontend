//Trx son la transacciones o movimiento realizados por los usuarios
//insercion de datos
export const TRXINSERT = 'TRXINSERT';
//ACTUALIZACION DE DATOS
export const TRXUPDATE = 'TRXUPDATE';
export const UPDATE_MESSAGE= 'UPDATE_MESSAGE'
//llamado de datos completo
export const GET_ALL_TRXS = 'GET_ALL_TRXS';
export const RESPONSE_GET_ALL_TRXS = 'RESPONSE_GET_ALL_TRXS';
//llamado de datos de una sola trax

// Eliminación de una transacción
export const TRXDELETE = 'TRXDELETE';


// Llamado a varias trx
export const GET_TRX ='GET_TRX';
export const RESPONSE_GET_TRX ='RESPONSE_GET_TRX';


//Llamado para completar combox 
export const GET_ORIGIN_COMBO = 'GET_ORIGIN_COMBO';
export const RESP_ORIGIN_COMBO = 'RESP_ORIGIN_COMBO';
export const GET_DESTINY_COMBO= 'GET_DESTINY_COMBO';
export const RESP_DESTINY_COMBO= 'RESP_DESTINY_COMBO';
export const GET_EXPENSE_COMBO = 'GET_EXPENSE_COMBO';
export const RESP_EXPENSE_COMBO= 'RESP_EXPENSE_COMBO';

export const SHOW_AUTH_MESSAGE = 'SHOW_AUTH_MESSAGE';
export const SHOW_MESSAGE = 'SHOW_MESSAGE';
export const RESET_SHOW_MESSAGE = 'RESET_SHOW_MESSAGE';
export const TOEDITTRX = 'TOEDITTRX';
export const CONSULT_TRANSATION_LIST_BY_DATE = 'CONSULT_TRANSATION_LIST_BY_DATE';

export const CREATE_TRANSACTION = 'CREATE_TRANSACTION';
export const DELETE_TRANSACTION = 'DELETE_TRANSACTION';
export const UPDATE_TRANSACTION = 'UPDATE_TRANSACTION';


// CONSTANS DE TAXATION
export const GET_RENTA = 'GET_RENTA';
export const RESP_RENTA = 'RESP_RENTA'

export const GET_IVA = 'GET_IVA';
export const RESP_IVA = 'RESP_IVA'

export const GET_CCSS = 'GET_CCSS';
export const RESP_CCSS = 'RESP_CCSS'