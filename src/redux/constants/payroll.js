export const PAYROLLINSERT = 'PAYROLLINSERT';

export const PAYROLLUPDATE = 'PAYROLLUPDATE';

export const PAYROLLDELETE = 'PAYROLLDELETE';

export const GET_ALL_PAYROLL = 'GET_ALL_PAYROLL';
export const RESPONSE_GET_ALL_PAYROLL = 'RESPONSE_GET_ALL_PAYROLL';

export const GET_PAYROLL = 'GET_PAYROLL';
export const RESPONSE_GET_PAYROLL = 'RESPONSE_GET_PAYROLL';

export const COLLABORATORINSERT = 'COLLABORATORINSERT';

export const GET_ALL_COLLABORATORS = 'GET_ALL_COLLABORATORS';
export const RESPONSE_GET_ALL_COLLABORATORS = 'RESPONSE_GET_ALL_COLLABORATORS';

export const SHOW_MESSAGE = 'SHOW_MESSAGE';
export const RESET_SHOW_MESSAGE = 'RESET_SHOW_MESSAGE';