// Blog son los post que publicas
// inserción de datos
export const BLOGINSERT = 'BLOGINSERT'

//  actualización de datos
export const BLOGUPDATE = 'BLOGUPDATE';

// eleminación de dato
export const BLOGDELETE = 'BLOGDELETE';

// llamada de todos los datos
export const GET_ALL_BLOGS = 'GET_ALL_BLOGS'
export const RESPONSE_GET_ALL_BLOGS = 'RESPONSE_GET_ALL_BLOGS'

export const SHOW_MESSAGE = 'SHOW_MESSAGE';
export const RESET_SHOW_MESSAGE = 'RESET_SHOW_MESSAGE';