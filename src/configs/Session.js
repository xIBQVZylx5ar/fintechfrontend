import { AUTH_TOKEN, USER_PROFILES, USER_ROL, USER_ID } from 'constants/AuthConstant';
import axios from 'axios';


const isLogin = () => {
    const token = localStorage.getItem(AUTH_TOKEN);
    const profiles = localStorage.getItem(USER_PROFILES);
    return (token === "null" && profiles === "null" ? false : true);
}

const isProfileAuthorized = ( profileParam ) => {
    const profiles = localStorage.getItem(USER_PROFILES);
    if(profiles){
        const profileList = profiles.split(',').map(x => +x);
        return profileList.includes(profileParam);
    } else {
        return false;
    }
}

const isUserTypeAuthorized = ( userParam ) => {
    return ( localStorage.getItem(USER_ROL) === userParam ? true : false );
}

const getAuthToken = () => {
    return localStorage.getItem(AUTH_TOKEN);
}

const getUserProfiles = () => {
    return localStorage.getItem(USER_PROFILES).split(';');
}

const createSession = ( tokenParam, profilesParam, rolParam, userIdParam ) => {
    localStorage.setItem(AUTH_TOKEN, tokenParam);
    localStorage.setItem(USER_PROFILES, profilesParam);
    localStorage.setItem(USER_ROL, rolParam);
    localStorage.setItem(USER_ID, userIdParam);
}

const closeSession = () => {
    localStorage.setItem(AUTH_TOKEN, null);
    localStorage.setItem(USER_PROFILES, null);
    localStorage.setItem(USER_ROL, null);
    localStorage.setItem(USER_ID, null);
}

const getAxiosHeaders = () => {
    axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem(AUTH_TOKEN)}`
    return axios;
}

export {
	isLogin,
    isProfileAuthorized,
    isUserTypeAuthorized,
    getAuthToken,
    getUserProfiles,
    createSession,
    closeSession,
    getAxiosHeaders
}