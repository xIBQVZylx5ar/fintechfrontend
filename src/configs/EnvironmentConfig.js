const dev = {
  API_ENDPOINT_URL: 'http://localhost:3001'
};

const prod = {
  API_ENDPOINT_URL: 'https://api.prod.com'
};

const test = {
  API_ENDPOINT_URL: 'https://api.test.com'
};

const getEnv = () => {
	switch (process.env.NODE_ENV) {
		case 'development':
			return dev
		case 'production':
			return prod
		case 'test':
			return test
		default:
			break;
	}
}

const devBackEnd = {
	API_ENDPOINT_URL: 'http://localhost:3000'
  };

const getEnvBackEnd = () => {
	switch (process.env.NODE_ENV) {
		case 'development':
			return devBackEnd;
	
		default:
			break;
	}
}

//export const env = getEnv()
module.exports = {
	env : getEnv(),
	envBackend : getEnvBackEnd()
}
