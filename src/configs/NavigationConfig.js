import {
  DashboardOutlined,
  UserOutlined,
  LineChartOutlined,
  WalletOutlined,
  ScheduleOutlined,
  FlagOutlined,
  BarcodeOutlined,
  BankOutlined,
  UsergroupAddOutlined,
  SolutionOutlined,
  ShopOutlined,
  SettingOutlined,
  UnorderedListOutlined,
  ProfileOutlined,
} from '@ant-design/icons';
import { APP_PREFIX_PATH } from 'configs/AppConfig'
import { isProfileAuthorized } from 'configs/Session';
import { TYPE_PROFILE_PERSONAL_TRX, TYPE_PROFILE_PROFESSIONAL_TRX, TYPE_PROFILE_BUSINESS_TRX } from 'constants/TransactionInfoConstant';

const getNavigationByProfile = () => {
  const navigations = [];

  if (isProfileAuthorized(TYPE_PROFILE_PERSONAL_TRX)) {
    navigations.push(
      {
        key: 'dashboards-personal',
        path: `${APP_PREFIX_PATH}/dashboards/personal`,
        title: 'sidenav.dashboard.personal',
        icon: UserOutlined,
        breadcrumb: false,
        submenu: [
          {
            key: 'dashboards-personal-generalbalance',
            path: `${APP_PREFIX_PATH}/dashboards/personal/general-balance`,
            title: 'sidenav.dashboard.personal.generalbalance',
            icon: LineChartOutlined,
            breadcrumb: false,
            submenu: []
          },
          {
            key: 'dashboards-personal-revenue',
            path: `${APP_PREFIX_PATH}/dashboards/personal/revenue/revenue-monthly`,
            title: 'sidenav.dashboard.personal.revenue',
            icon: WalletOutlined,
            breadcrumb: false,
            submenu: []
          },
          {
            key: 'dashboards-personal-expenses',
            path: `${APP_PREFIX_PATH}/dashboards/personal/expense/expense-monthly`,
            title: 'sidenav.dashboard.personal.expenses',
            icon: DashboardOutlined,
            breadcrumb: false,
            submenu: []
          }
        ]
      }
    );
  }

  if (isProfileAuthorized(TYPE_PROFILE_PROFESSIONAL_TRX)) {
    navigations.push(
      {
        //servicios 
        key: 'dashboards-professionalservices',
        path: `${APP_PREFIX_PATH}/dashboards/professional-services`,
        title: 'sidenav.dashboard.professionalservices',
        icon: DashboardOutlined,
        breadcrumb: false,
        submenu: [
          //submenu servicios
          {
            key: 'dashboards-professionalservices-generalbalance',
            path: `${APP_PREFIX_PATH}/dashboards/professional-services/general-balance`,
            title: 'sidenav.dashboard.professionalservices.generalbalance',
            icon: LineChartOutlined,
            breadcrumb: false,
            submenu: []
          },
          {
            key: 'dashboards-professionalservices-revenue',
            path: `${APP_PREFIX_PATH}/dashboards/professional-services/revenue`,
            title: 'sidenav.dashboard.professionalservices.revenue',
            icon: WalletOutlined,
            breadcrumb: false,
            submenu: []
          },
          {
            key: 'dashboards-professionalservices-cxc',
            path: `${APP_PREFIX_PATH}/dashboards/professional-services/cxc`,
            title: 'sidenav.dashboard.professionalservices.cxc',
            icon: ScheduleOutlined,
            breadcrumb: false,
            submenu: []
          },
          {
            key: 'dashboards-professionalservices-cxp',
            path: `${APP_PREFIX_PATH}/dashboards/professional-services/cxp`,
            title: 'sidenav.dashboard.professionalservices.cxp',
            icon: FlagOutlined,
            breadcrumb: false,
            submenu: []
          },
          {
            key: 'dashboards-professionalservices-cogs',
            path: `${APP_PREFIX_PATH}/dashboards/professional-services/cogs`,
            title: 'sidenav.dashboard.professionalservices.cogs',
            icon: BarcodeOutlined,
            breadcrumb: false,
            submenu: []
          },
          {
            key: 'dashboards-professionalservices-sga',
            path: `${APP_PREFIX_PATH}/dashboards/professional-services/sga`,
            title: 'sidenav.dashboard.professionalservices.sga',
            icon: BankOutlined,
            breadcrumb: false,
            submenu: []
          },
          {
            key: 'dashboards-professionalservices-payroll',
            path: `${APP_PREFIX_PATH}/dashboards/professional-services/payroll`,
            title: 'sidenav.dashboard.professionalservices.payroll',
            icon: UsergroupAddOutlined,
            breadcrumb: false,
            submenu: []
          },
          {
            key: 'dashboards-professionalservices-capex',
            path: `${APP_PREFIX_PATH}/dashboards/professional-services/capex`,
            title: 'sidenav.dashboard.professionalservices.capex',
            icon: ShopOutlined,
            breadcrumb: false,
            submenu: []
          },
          {
            key: 'dashboards-professionalservices-taxation',
            path: `${APP_PREFIX_PATH}/dashboards/professional-services/taxation`,
            title: 'sidenav.dashboard.professionalservices.taxation',
            icon: SolutionOutlined,
            breadcrumb: false,
            submenu: []
          }
        ]
      }
    );
  }

  if (isProfileAuthorized(TYPE_PROFILE_BUSINESS_TRX)) {
    navigations.push(
      {
        //empresa
        key: 'dashboards-business',
        path: `${APP_PREFIX_PATH}/dashboards/business`,
        title: 'sidenav.dashboard.business',
        icon: DashboardOutlined,
        breadcrumb: false,
        submenu: [
          //submenu empresa 
          {
            key: 'dashboards-business-generalbalance',
            path: `${APP_PREFIX_PATH}/dashboards/business/generalbalance`,
            title: 'sidenav.dashboard.business.generalbalance',
            icon: LineChartOutlined,
            breadcrumb: false,
            submenu: []
          },
          {
            key: 'dashboards-business-revenue',
            path: `${APP_PREFIX_PATH}/dashboards/business/revenue`,
            title: 'sidenav.dashboard.business.revenue',
            icon: WalletOutlined,
            breadcrumb: false,
            submenu: []
          },
          {
            key: 'dashboards-business-cxc',
            path: `${APP_PREFIX_PATH}/dashboards/business/cxc`,
            title: 'sidenav.dashboard.business.cxc',
            icon: ScheduleOutlined,
            breadcrumb: false,
            submenu: []
          },
          {
            key: 'dashboards-business-cxp',
            path: `${APP_PREFIX_PATH}/dashboards/business/cxp`,
            title: 'sidenav.dashboard.business.cxp',
            icon: FlagOutlined,
            breadcrumb: false,
            submenu: []
          },
          {
            key: 'dashboards-business-cogs',
            path: `${APP_PREFIX_PATH}/dashboards/business/cogs`,
            title: 'sidenav.dashboard.business.cogs',
            icon: BarcodeOutlined,
            breadcrumb: false,
            submenu: []
          },
          {
            key: 'dashboards-business-sga',
            path: `${APP_PREFIX_PATH}/dashboards/business/sga`,
            title: 'sidenav.dashboard.business.sga',
            icon: BankOutlined,
            breadcrumb: false,
            submenu: []
          },
          {
            key: 'dashboards-business-payroll',
            path: `${APP_PREFIX_PATH}/dashboards/business/payroll`,
            title: 'sidenav.dashboard.business.payroll',
            icon: UsergroupAddOutlined,
            breadcrumb: false,
            submenu: []
          },
          {
            key: 'dashboards-business-capex',
            path: `${APP_PREFIX_PATH}/dashboards/business/capex`,
            title: 'sidenav.dashboard.business.capex',
            icon: ShopOutlined,
            breadcrumb: false,
            submenu: []
          },
          {
            key: 'dashboards-business-taxation',
            path: `${APP_PREFIX_PATH}/dashboards/business/taxation`,
            title: 'sidenav.dashboard.business.taxation',
            icon: SolutionOutlined,
            breadcrumb: false,
            submenu: []
          }
        ]
      }
    );
  }

  navigations.push(
    {
      key: 'dashboards-blog',
      path: `${APP_PREFIX_PATH}/dashboards/blog`,
      title: 'sidenav.dashboard.blog',
      icon: UsergroupAddOutlined,
      breadcrumb: false,
      submenu: []

    },

    // Settings
    {
      key: 'dashboards-settings',
      path: `${APP_PREFIX_PATH}/dashboards/settings`,
      title: 'sidenav.dashboard.settings',
      icon: SettingOutlined,
      breadcrumb: false,
      submenu: [{
        key: 'dashboards-settings-categories',
        path: `${APP_PREFIX_PATH}/dashboards/settings/categories`,
        title: 'sidenav.dashboard.settings.categories',
        icon: UnorderedListOutlined,
        breadcrumb: false,
        submenu: []
      },
      {
        key: 'dashboards-settings-edit-profile',
        path: `${APP_PREFIX_PATH}/dashboards/settings/edit-profile`,
        title: 'sidenav.dashboard.settings.edit-profile',
        icon: ProfileOutlined,
        breadcrumb: false,
        submenu: []
      }]
    }/*,
    {
      key: 'dashboards-settings-change-mail',
      path: `${APP_PREFIX_PATH}/dashboards/settings/change-mail`,
      title: 'sidenav.dashboard.settings.change-mail',
      icon: MailOutlined,
      breadcrumb: false,
      submenu: []
    }*/
  );

  return navigations;
}

const dashBoardNavTree = [{
  key: 'dashboards',
  path: `${APP_PREFIX_PATH}/dashboards`,
  title: 'sidenav.dashboard',
  icon: DashboardOutlined,
  breadcrumb: false,
  submenu: getNavigationByProfile()
}]


const navigationConfig = [
  ...dashBoardNavTree
]

export default navigationConfig;