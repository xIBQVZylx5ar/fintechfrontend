export const TYPE_PERSONAL_REVENUE_TRX = "Revenue";
export const TYPE_PERSONAL_EXPENSES_TRX = "Expenses";

export const TYPE_PROFESSIONAL_BUSINESS_REVENUE_TRX = "RevenuePSB";
export const TYPE_PROFESSIONAL_BUSINESS_COGS_TRX = "COGS";
export const TYPE_PROFESSIONAL_BUSINESS_SGA_TRX = "SG&A";
export const TYPE_PROFESSIONAL_BUSINESS_CAPEX_TRX = "CAPEX";
export const TYPE_PROFESSIONAL_BUSINESS_CXC_TRX = "CXC";
export const TYPE_PROFESSIONAL_BUSINESS_CXP_TRX = "CXP";
export const TYPE_PROFESSIONAL_BUSINESS_PAYROLL_TRX = "Payroll";
export const TYPE_PROFESSIONAL_BUSINESS_TAXATION_TRX = "Taxation";
export const TYPE_PROFESSIONAL_BUSINESS_COLLABORATOR_TRX = "Collaborator";

export const TYPE_PROFILE_PERSONAL_TRX = 1;
export const TYPE_PROFILE_PROFESSIONAL_TRX = 2;
export const TYPE_PROFILE_BUSINESS_TRX = 3;

export const TYPE_USER = 'user';
export const TYPE_ADMIN = 'admin';

// Types of expenses
export const TYPE_PERSONAL_EXPENSES = 1;
export const TYPE_COGS_EXPENSE = 2;
export const TYPE_SGA_EXPENSES = 3;
export const TYPE_CAPEX_EXPENSES = 4;

// Type of accounts payable and receivable
export const TYPE_CXC = 1;
export const TYPE_CXP = 2;

// Type Valid Categories
export const TYPE_CATEGORIES_REVENUE_TRX = "Revenues";
export const TYPE_CATEGORIES_EXPENSES_TRX = "Expenses";
export const TYPE_CATEGORIES_DESTINATION_TRX = "Destination";

// Profile user id
export const PROFILE_USER_ID = 65