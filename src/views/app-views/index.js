import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { APP_PREFIX_PATH } from 'configs/AppConfig';
import Dashboards from './dashboards';

export const AppViews = () => {
  return (
    <Switch>
      <Route path={`${APP_PREFIX_PATH}/dashboards`} component={Dashboards} />
      <Redirect from={`${APP_PREFIX_PATH}`} to={`${APP_PREFIX_PATH}/dashboards`} />
    </Switch>
  )
}

export default React.memo(AppViews);