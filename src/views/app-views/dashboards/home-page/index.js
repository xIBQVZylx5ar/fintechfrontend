import React from 'react'
import PageHeaderAlt from 'components/layout-components/PageHeaderAlt'
import { Row, Col, Card} from 'antd';
import Logo from 'components/layout-components/Logo';
import { isProfileAuthorized } from 'configs/Session';
import { useHistory } from "react-router-dom";
import { TYPE_PROFILE_PERSONAL_TRX, TYPE_PROFILE_PROFESSIONAL_TRX, TYPE_PROFILE_BUSINESS_TRX } from 'constants/TransactionInfoConstant'

const HomePage = () => {
	let history = useHistory();

	const redirectHome = ( profile ) => {
		switch (profile) {
			case 1:
				history.push('./personal/general-balance')
				break;
	
			case 2:
				history.push('./professional-services/general-balance')
				break;
	
			case 3:
				history.push('./business/general-balance')
				break;
		
			default:
				break;
		}
	}

	return (
		<>
			<Card>
			<PageHeaderAlt className="bg-primary" overlap>
			<Logo/>
				<div className="container text-center">
					<div className="py-lg-4">
						<h1 className="display-4">Menú principal</h1>
					</div>
				</div>
				<br></br>
				<br></br>
				<br></br>
				<br></br>
			</PageHeaderAlt>
			<div className="container my-4" >
				<Row gutter={16} className="d-flex justify-content-center">
					{ isProfileAuthorized(TYPE_PROFILE_PERSONAL_TRX) && (
						<Col xs={24} sm={24} md={8}>
							<Card 
								hoverable 
								onClick={() => redirectHome(1)}>
								<div className="text-center">
									<img className="img-fluid" src={'/img/others/img-4.png'} alt={'Perfil personal'} />
									<h3 className="mt-4">{'Perfil personal'}</h3>
								</div>
							</Card>
						</Col>
					)}
					
					{ isProfileAuthorized(TYPE_PROFILE_PROFESSIONAL_TRX) && (
						<Col xs={24} sm={24} md={8}>
							<Card 
								hoverable 
								onClick={() => redirectHome(2)}>
								<div className="text-center">
									<img className="img-fluid" src={'/img/others/img-4.png'} alt={'Perfil servicios profesionales'} />
									<h3 className="mt-4">{'Perfil servicios profesionales'}</h3>
								</div>
							</Card>
						</Col>
					) }
					
					{ isProfileAuthorized(TYPE_PROFILE_BUSINESS_TRX) && (
						<Col xs={24} sm={24} md={8}>
							<Card 
								hoverable 
								onClick={() => redirectHome(3)}>
								<div className="text-center">
									<img className="img-fluid" src={'/img/others/img-4.png'} alt={'Perfil empresa'} />
									<h3 className="mt-4">{'Perfil empresa'}</h3>
								</div>
							</Card>
						</Col>
					) }
					
				</Row>
			</div>
			</Card>
		</>
	);
}

export default HomePage
