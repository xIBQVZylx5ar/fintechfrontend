import React from "react";
import { Redirect, Route, Switch } from 'react-router-dom';
import { TYPE_PROFILE_PERSONAL_TRX, TYPE_PROFILE_PROFESSIONAL_TRX, TYPE_PROFILE_BUSINESS_TRX } from 'constants/TransactionInfoConstant'
import { isLogin, isProfileAuthorized } from 'configs/Session';
import Personal from './personal';
import Business from './business';
import ProfessionalServices from './professional-services';
import Blog from './blog';
import Settings from './settings';
import ErrorNotFoundPage from 'views/errors/error-page';
import HomePage from './home-page';

const Dashboards = ({ match }) => {
  const isProfilePersonal = isProfileAuthorized(TYPE_PROFILE_PERSONAL_TRX);
  const isProfileProfessional = isProfileAuthorized(TYPE_PROFILE_PROFESSIONAL_TRX);
  const isProfileBusiness = isProfileAuthorized(TYPE_PROFILE_BUSINESS_TRX);

  return(
    <div>
      { isLogin() && ( 
        <Switch>
          <Route path={`${match.url}/home`} component={HomePage} />
          { isProfilePersonal ? ( <Route path={`${match.url}/personal`} component={Personal} /> ) : null }
          { isProfileProfessional ? ( <Route path={`${match.url}/professional-services`} component={ProfessionalServices} /> ) : null }
          { isProfileBusiness ? ( <Route path={`${match.url}/business`} component={Business} /> ) : null }
          <Route path={`${match.url}/settings`} component={Settings} />
          <Route path={`${match.url}/blog`} component={Blog} />
          <Route path={`${match.url}/error`} component={ErrorNotFoundPage} />
          <Redirect from={`${match.url}`} to={`${match.url}/error`} />
        </Switch>
      ) }
    </div>
      
  )
};

export default Dashboards;