import React, { useEffect } from "react";
import { Card, Table, Input, Button, Spin, Row, Col, Modal, Form, Select, notification, Divider, Popconfirm, Skeleton } from "antd";
import { PlusCircleOutlined, EditOutlined, QuestionCircleOutlined, DeleteOutlined } from "@ant-design/icons";
import { isProfileAuthorized } from 'configs/Session'
import { connect } from 'react-redux';
import { newCategory, getCategories, resetSettingMessage, editCategory, deteleCategory } from 'redux/actions/settings';
import { TYPE_PROFILE_PERSONAL_TRX, TYPE_PROFILE_PROFESSIONAL_TRX, TYPE_PROFILE_BUSINESS_TRX } from 'constants/TransactionInfoConstant';

const { Option } = Select;

function Categories(props) {
    const { newCategory, getCategories, categories, message, errorCode, editCategory, resetSettingMessage, deteleCategory, loadingSettings } = props;
    const [ediTableForm, setEdiTableForm] = React.useState(false);

    useEffect(() => {
        getCategories()
    }, []);
    useEffect(() => {
        if (message) {

            notification['success']({
                message: 'Notificación',
                description: message,
            });
            resetSettingMessage();
            getCategories();
        }

        if (errorCode) {
            notification['error']({
                message: 'Notificación',
                description: errorCode,
            });
            resetSettingMessage();
            getCategories();
        }

    });

    const RevenueCategory = (info) => {
        const { orinData } = info;
        const [revVisible, revSetVisible] = React.useState(false);
        const [cat_ID, setCatID] = React.useState(0);
        const [RevenueForm] = Form.useForm();
        const revColumns = [
            {
                title: 'Descripcion',
                width: 100,
                dataIndex: 'catrevdes',
                key: 'catrevdes',
                fixed: 'left',
            },
            {
                title: 'Profile',
                width: 100,
                dataIndex: 'catprofrev',
                key: 'catprofrev',
                fixed: 'left',
            },
            {
                title: 'Accion',
                key: 'deletecatrev',
                fixed: 'center',
                width: 100,
                render: (info) => (
                    <div>
                        <Button
                            type="primary"
                            icon={<EditOutlined />}
                            onClick={() => toEdit(info.key)}
                        ></Button>
                        <Popconfirm title={'¿Desea elimiar?'} onConfirm={() => toDelete(info.key)} icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                            <Button
                                icon={<DeleteOutlined />}
                                type="primary"
                                danger
                            ></Button>
                        </Popconfirm>
                    </div>
                ),
            },
        ];

        const revCatData = [];

        orinData.map(function (data) {
            let perfil;
            if (data.profile_id == 1) {
                perfil = 'Personal'
            } else if (data.profile_id == 2) {
                perfil = 'Servicios Profesionales'
            } else {
                perfil = 'Empresa'
            }
            return revCatData.push({
                key: data.origin_id,
                catrevdes: data.description,
                catprofrev: perfil

            })
        });


        const showModalRev = () => {
            setEdiTableForm(false)
            revSetVisible(true)
        }

        const addRevCat = (values) => {
            values['category_type'] = "Revenues";
            values['origin_id'] = cat_ID;
            if (ediTableForm) {
                editCategory(values)

            } else {

                newCategory(values)
            }
            revSetVisible(false)
            //getCategories()
        }

        const toEdit = (cat) => {
            revSetVisible(true)
            setEdiTableForm(true)
            let result = orinData.find(({ origin_id }) => origin_id === cat)
            RevenueForm.setFieldsValue({

                description: result.description,
                profile_id: result.profile_id
            })
            setCatID(result.origin_id);
        }
        let new_values
        const newVal = () => {
            new_values = RevenueForm.getFieldsValue();
            RevenueForm.setFieldsValue(new_values);

        }
        const toDelete = (cat) => {
            let deleteInfo = {};
            deleteInfo['origin_id'] = cat
            deleteInfo['category_type'] = 'Revenues'
            deteleCategory(deleteInfo)
            //getCategories()
        }

        let btmAddRevCat = (ediTableForm) ? 'Editar' : 'Agregar'
        return (
            <div>
                <Card>
                    <center><p>Agregar categoría de ingreso</p></center>
                    <Button
                        onClick={showModalRev}
                        type="primary"
                        icon={<PlusCircleOutlined />}
                        block
                    />
                    <Divider />
                    <Card>
                        <Table columns={revColumns} sortDirections dataSource={revCatData} scroll={{ y: 300 }} pagination={false} />
                    </Card>
                </Card>
                <Modal
                    visible={revVisible}
                    width={1000}
                    footer={null}
                    forceRenderen
                    onCancel={() => revSetVisible(false)}
                    getContainer={false}
                >
                    <>
                        <Form
                            layout="vertical"
                            form={RevenueForm}
                            name="modal-form"
                            onFinish={addRevCat}
                            className="modal-form"
                            onChange={newVal}
                            forcerenderen={"true"}

                        >
                            <Form.Item name={'description'}>
                                <Input placeholder={'Descripcion'}></Input>
                            </Form.Item>
                            <Form.Item name="profile_id" label="Seleccione Un Perfil:">
                                <Select style={{ width: "100%" }}>
                                    {isProfileAuthorized(TYPE_PROFILE_PERSONAL_TRX) && (<Option key={1} value={1}>{'Personal'}</Option>)}
                                    {isProfileAuthorized(TYPE_PROFILE_PROFESSIONAL_TRX) && (<Option key={2} value={2}>{'Servicios Profesionales'}</Option>)}
                                    {isProfileAuthorized(TYPE_PROFILE_BUSINESS_TRX) && (<Option key={3} value={3}>{'Empresa'}</Option>)}
                                </Select>
                            </Form.Item>
                            <Button htmlType="submit" type="primary">
                                {btmAddRevCat}
                            </Button>
                        </Form>
                    </>
                </Modal>
            </div>
        )

    }

    const ExpensesCategory = (info) => {
        const { expData } = info;
        const [expVisible, expSetVisible] = React.useState(false);
        const [cat_ID, setCatID] = React.useState(0);
        const [ExpenseForm] = Form.useForm();
        const expColumns = [
            {
                title: 'Descripcion',
                width: 100,
                dataIndex: 'catexpdes',
                key: 'catexpdes',
                fixed: 'left',
            },
            {
                title: 'Accion',
                key: 'deletecatrev',
                fixed: 'center',
                width: 100,
                render: (info) => (
                    <div>
                        <Button
                            type="primary"
                            icon={<EditOutlined />}
                            onClick={() => toEdit(info.key)}
                        ></Button>
                        <Popconfirm title={'¿Desea elimiar?'} onConfirm={() => toDelete(info.key)} icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                            <Button
                                icon={<DeleteOutlined />}
                                type="primary"
                                danger
                            ></Button>
                        </Popconfirm>
                    </div>
                ),
            },
        ];

        const expCatData = [];

        expData.map(function (data) {
            return expCatData.push({
                key: data.id_exp_cat,
                catexpdes: data.description,

            })
        });


        const showModalRev = () => {
            setEdiTableForm(false)
            expSetVisible(true)
        }

        const addExpCat = (values) => {
            values['category_type'] = "Expenses";
            values['id_exp_cat'] = cat_ID;
            if (ediTableForm) {
                editCategory(values)

            } else {

                newCategory(values)
            }
            expSetVisible(false)
            //getCategories()
        }

        const toEdit = (cat) => {
            expSetVisible(true)
            setEdiTableForm(true)
            let result = expData.find(({ id_exp_cat }) => id_exp_cat === cat)
            ExpenseForm.setFieldsValue({

                description: result.description,
                profile_id: result.profile_id
            })
            setCatID(result.id_exp_cat);
        }
        let new_values
        const newVal = () => {
            new_values = ExpenseForm.getFieldsValue();
            ExpenseForm.setFieldsValue(new_values);

        }
        const toDelete = (cat) => {
            let deleteInfo = {};
            deleteInfo['id_exp_cat'] = cat
            deleteInfo['category_type'] = 'Expenses'
            deteleCategory(deleteInfo)
            //getCategories()
        }

        let btmAddExpCat = (ediTableForm) ? 'Editar' : 'Agregar'
        return (
            <div>
                <Card>
                    <center><p>Agregar categoría de egreso (Personal)</p></center>
                    <Button
                        onClick={showModalRev}
                        type="primary"
                        icon={<PlusCircleOutlined />}
                        block
                    />
                    <Divider />
                    <Card>
                        <Table columns={expColumns} sortDirections dataSource={expCatData} scroll={{ y: 300 }} pagination={false} />
                    </Card>
                </Card>
                <Modal
                    visible={expVisible}
                    width={1000}
                    footer={null}
                    forceRenderen
                    onCancel={() => expSetVisible(false)}
                    getContainer={false}
                >
                    <>
                        <Form
                            layout="vertical"
                            form={ExpenseForm}
                            name="modal-form"
                            onFinish={addExpCat}
                            className="modal-form"
                            onChange={newVal}
                            forcerenderen={"true"}

                        >
                            <Form.Item name={'description'}>
                                <Input placeholder={'Descripcion'}></Input>
                            </Form.Item>

                            <Button htmlType="submit" type="primary">
                                {btmAddExpCat}
                            </Button>
                        </Form>
                    </>
                </Modal>
            </div>
        )

    }

    const DestinyCategory = (info) => {
        const { destData } = info;
        const [destVisible, destSetVisible] = React.useState(false);
        const [cat_ID, setCatID] = React.useState(0);
        const [DestinyForm] = Form.useForm();
        const destColumns = [
            {
                title: 'Descripcion',
                width: 100,
                dataIndex: 'catdest',
                key: 'catdest',
                fixed: 'left',
            },
            {
                title: 'Profile',
                width: 100,
                dataIndex: 'catprofdest',
                key: 'catprofdest',
                fixed: 'left',
            },
            {
                title: 'Accion',
                key: 'deletecatdest',
                fixed: 'center',
                width: 100,
                render: (info) => (
                    <div>
                        <Button
                            type="primary"
                            icon={<EditOutlined />}
                            onClick={() => toEdit(info.key)}
                        ></Button>
                        <Popconfirm title={'¿Desea elimiar?'} onConfirm={() => toDelete(info.key)} icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                            <Button
                                icon={<DeleteOutlined />}
                                type="primary"
                                danger
                            ></Button>
                        </Popconfirm>
                    </div>
                ),
            },
        ];

        const destCatData = [];

        destData.map(function (data) {
            let perfil;
            if (data.profile_id == 1) {
                perfil = 'Personal'
            } else if (data.profile_id == 2) {
                perfil = 'Servicios Profesionales'
            } else {
                perfil = 'Empresa'
            }
            return destCatData.push({
                key: data.destination_id,
                catdest: data.description,
                catprofdest: perfil

            })
        });


        const showModalRev = () => {
            setEdiTableForm(false)
            destSetVisible(true)
        }

        const addDestCat = (values) => {
            values['category_type'] = "Destination";
            values['destination_id'] = cat_ID;
            if (ediTableForm) {
                editCategory(values)
            } else {

                newCategory(values)
            }
            destSetVisible(false)
            //getCategories()
        }

        const toEdit = (cat) => {
            destSetVisible(true)
            setEdiTableForm(true)
            let result = destData.find(({ destination_id }) => destination_id === cat)
            DestinyForm.setFieldsValue({

                description: result.description,
                profile_id: result.profile_id
            })
            setCatID(result.destination_id);
        }
        let new_values
        const newVal = () => {
            new_values = DestinyForm.getFieldsValue();
            DestinyForm.setFieldsValue(new_values);

        }
        const toDelete = (cat) => {
            let deleteInfo = {};
            deleteInfo['destination_id'] = cat
            deleteInfo['category_type'] = 'Destination'
            deteleCategory(deleteInfo)
            //getCategories()
        }

        let btmAddDestCat = (ediTableForm) ? 'Editar' : 'Agregar'
        return (
            <div>
                <Card>
                    <center><p>Agregar categoría de métodos de pago / Destinos</p></center>
                    <Button
                        onClick={showModalRev}
                        type="primary"
                        icon={<PlusCircleOutlined />}
                        block
                    />
                    <Divider />
                    <Card>
                        <Table columns={destColumns} sortDirections dataSource={destCatData} scroll={{ y: 300 }} pagination={false} />
                    </Card>
                </Card>
                <Modal
                    visible={destVisible}
                    width={1000}
                    footer={null}
                    forceRenderen
                    onCancel={() => destSetVisible(false)}
                    getContainer={false}
                >
                    <>
                        <Form
                            layout="vertical"
                            form={DestinyForm}
                            name="modal-form"
                            onFinish={addDestCat}
                            className="modal-form"
                            onChange={newVal}
                            forcerenderen={"true"}

                        >
                            <Form.Item name={'description'}>
                                <Input placeholder={'Descripcion'}></Input>
                            </Form.Item>
                            <Form.Item name="profile_id" label="Seleccione Un Perfil:">
                                <Select style={{ width: "100%" }}>
                                    {isProfileAuthorized(TYPE_PROFILE_PERSONAL_TRX) && (<Option key={1} value={1}>{'Personal'}</Option>)}
                                    {isProfileAuthorized(TYPE_PROFILE_PROFESSIONAL_TRX) && (<Option key={2} value={2}>{'Servicios Profesionales'}</Option>)}
                                    {isProfileAuthorized(TYPE_PROFILE_BUSINESS_TRX) && (<Option key={3} value={3}>{'Empresa'}</Option>)}
                                </Select>
                            </Form.Item>
                            <Button htmlType="submit" type="primary">
                                {btmAddDestCat}
                            </Button>
                        </Form>
                    </>
                </Modal>
            </div>
        )

    }



    if (Object.keys(categories).length === 0) {

        return (
            <Skeleton active={true} />
        )
    } else {

        let orinData = (categories.origin === null || undefined) ? [] : categories.origin
        let expData = (categories.expcat === null || undefined) ? [] : categories.expcat
        let destData = (categories.destination == null || undefined) ? [] : categories.destination

        return (
            <Spin spinning={loadingSettings}>
                <div>
                    <Row gutter={16}>
                        <Col xs={24} sm={24} md={24} lg={8}>
                            <RevenueCategory orinData={orinData} />
                        </Col>

                        <Col xs={24} sm={24} md={24} lg={8}>
                            <DestinyCategory destData={destData} />
                        </Col>
                        {isProfileAuthorized(TYPE_PROFILE_PERSONAL_TRX) && (
                            <Col xs={24} sm={24} md={24} lg={8}>
                                <ExpensesCategory expData={expData} />
                            </Col>
                        )}

                    </Row>
                </div>
            </Spin>
        )

    }


}


const mapStateToProps = ({ settings }) => {
    const { message, showMessage, redirect, categories, errorCode, loadingSettings } = settings;
    return { message, showMessage, redirect, categories, errorCode, loadingSettings };
};

const mapDispatchToProps = {
    newCategory, getCategories, editCategory, resetSettingMessage, deteleCategory
};

export default connect(mapStateToProps, mapDispatchToProps)(Categories);