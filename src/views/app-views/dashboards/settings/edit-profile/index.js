import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { connect } from "react-redux";
import { Form, Button, Input, Row, Col, notification, Spin, Card } from "antd";

import { ROW_GUTTER } from "constants/ThemeConstant";
import { getProfile, resetSettingMessage, updateProfile } from "redux/actions/settings";
import { signOut } from "redux/actions/Auth";

const EditProfile = (props) => {

  const [form] = Form.useForm();
  let history = useHistory()
  const { 
    profile,
    messageResponse,
    errorCode,
    getProfile,
    updateProfile,
    resetSettingMessage,
    auth,
    loadingSettings
  } = props;

  useEffect(() => {
    if (messageResponse) {
      notification['success']({
          message: 'Notificación',
          description: messageResponse,
      });
      resetSettingMessage();
      
      setTimeout(() => {
        history.push(auth.redirect)
      }, 8000);

      
    }

    if (errorCode) {
      notification['error']({
         message: 'Notificación',
         description: errorCode,
      });
      resetSettingMessage();
    }
  });


  useEffect(()=>{
    getProfile()
  }, [])

  const onFinish = (values) => {
    const info = {
      user_id: profile.id,
      name: values.name,
      last_name: values.lastname1,
      email: values.email,
      password: values.currentPassword
    }
    updateProfile(info)

  };
  
  return (
    <>
    <Spin spinning={loadingSettings}>
      {
        profile && (
          <Card>
            <div>
          <div className="mt-4">
            <Form
              name="basicInformation"
              form={form}
              layout="vertical"
              initialValues={{
                name: profile.name,
                email: profile.email,
                lastname1: profile.last_name,
                lastname2:  profile.second_last_name,
              }}
              onFinish={onFinish}
            >
              <Row>
                <Col xs={24} sm={24} md={24} lg={16}>
                  <Row gutter={ROW_GUTTER}>
                    <Col xs={24} sm={24} md={12}>
                      <Form.Item
                        label="Nombre"
                        name="name"
                        rules={[
                          {
                            required: true,
                            message: "¡Por favor, introduzca su nombre!",
                          },
                        ]}
                      >
                        <Input />
                      </Form.Item>
                    </Col>
                    <Col xs={24} sm={24} md={12}>
                      <Form.Item
                        label="Apellidos"
                        name="lastname1"
                        rules={[
                          {
                            required: true,
                            message: "¡Por favor, introduzca su primer apellido!",
                          },
                        ]}
                      >
                        <Input />
                      </Form.Item>
                    </Col>
                    <Col xs={24} sm={24} md={12}>
                      <Form.Item
                        label="Correo electrónico"
                        name="email"
                        rules={[
                          {
                            type: "email",
                            required: true,
                            message: "¡Por favor, introduzca su correo nuevo! ",
                          },
                        ]}
                      >
                        <Input />
                      </Form.Item>
                    </Col>
                    <Col xs={24} sm={24} md={12}>
                      <Form.Item
                        label="Ingrese su contraseña actual para guardar"
                        name="currentPassword"
                        rules={[
                          {
                            required: true,
                            message: "¡Por favor, introduzca su contraseña actual!",
                          },
                        ]}
                      >
                        <Input.Password />
                      </Form.Item>
                    </Col>
                  </Row>
                  <Button type="primary" htmlType="submit">
                    Guardar Cambios
                  </Button>
                </Col>
              </Row>
            </Form>
            </div>
          </div>
        </Card>
        )
      }
      </Spin>
    </>
  );
};

const mapStateToProps = ({ settings, auth }) => {
  const { profile, message, errorCode, loadingSettings } = settings;
  const messageResponse = message;
  return { profile, messageResponse, errorCode, auth, loadingSettings }
}

const mapDispatchToProps = {
  getProfile,
  resetSettingMessage,
  updateProfile,
  signOut
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);
