import React, { useEffect } from "react";
import { connect } from "react-redux"
import { Form, Button, Input, Row, Col, notification } from "antd";

import { changeMail, getProfile, resetSettingMessage } from "redux/actions/settings"
import { PROFILE_USER_ID } from "constants/TransactionInfoConstant"

const ChangeMail = (props) => {
  const {
    messageResponse,
    errorCode,
    profile,
    changeMail,
    getProfile,
    resetSettingMessage,
  } = props;
  
  useEffect(() => {
    
    if (messageResponse) {
      onReset()
      notification['success']({
          message: 'Notificación',
          description: messageResponse 
      });
      resetSettingMessage();
      getProfile({
        user_id: PROFILE_USER_ID
      })
    }

    if (errorCode) {
      notification['error']({
         message: 'Notificación',
         description: errorCode 
      });
      resetSettingMessage();
    }
  });

  useEffect(()=>{
    getProfile({
      user_id: PROFILE_USER_ID
    })
    
  }, [])

  const changeMailFormRef =  React.createRef();
  

 const onFinish = (values) => {
    const info = {
      email: values.newMail,
      password: values.currentPassword,
      user_id: profile.id
    }
    changeMail(info)
    /*message.success({ content: "Correo cambiado exitosamente. Requerimos la verificación de tu correo electrónico. Por favor revisa tu buzón de correo y sigue las instrucciones enviadas, de lo contrario no se tomaran los cambios realizados",
    duration: 20 });
    onReset();*/
  };
  
  const onReset = () => {
    changeMailFormRef.current.resetFields();
  };

  return (
    <>
      <h2 className="mb-4">Cambiar Correo</h2>
      <Row>
        <Col xs={24} sm={24} md={24} lg={8}>
          <Form
            name="changeMailForm"
            layout="vertical"
            ref={changeMailFormRef}
            onFinish={onFinish}
          >
            <Form.Item
              label="Nuevo correo"
              name="newMail"
              rules={[
                {
                  type: "email",
                  required: true,
                  message: "¡Por favor, introduzca su correo nuevo! ",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Contraseña Actual"
              name="currentPassword"
              rules={[
                {
                  required: true,
                  message: "¡Por favor, introduzca su contraseña actual!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              label="Confirme su Contraseña"
              name="confirmPassword"
              rules={[
                {
                  required: true,
                  message: "¡Por favor, confirme su contraseña!",
                },
                ({ getFieldValue }) => ({
                  validator(rule, value) {
                    if (!value || getFieldValue("currentPassword") === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject("¡La contraseña no coincide!");
                  },
                }),
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Button type="primary" htmlType="submit">
              Cambiar Correo
            </Button>
          </Form>
        </Col>
      </Row>
    </>
  );
};

const mapStateToProps = ({ settings }) => {
  const { message, errorCode, profile } = settings;
  const messageResponse = message;
  return { messageResponse, errorCode, profile }
}

const mapDispacthToProps ={
  changeMail,
  getProfile,
  resetSettingMessage
}

export default connect(mapStateToProps, mapDispacthToProps)(ChangeMail);