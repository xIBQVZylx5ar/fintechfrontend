import React from "react";
import { Redirect, Route, Switch } from 'react-router-dom';
import Categories from './categories';
import EditProfile from './edit-profile';


const Settings = ({ match }) => {
  return(
    <Switch>
        <Route path={`${match.url}/categories`} component={() => <Categories/>} />
        <Route path={`${match.url}/edit-profile`} component={() => <EditProfile/>} />
        {/*<Route path={`${match.url}/change-mail`} component={() => <ChangeMail/>} />*/}

		    <Redirect from={`${match.url}`} to={`${match.url}/categories`} />
    </Switch>
  )
};

export default Settings;