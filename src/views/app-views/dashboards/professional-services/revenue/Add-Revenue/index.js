import AddForm from 'components/dynamic-components/addForm';
import { TYPE_PROFESSIONAL_BUSINESS_REVENUE_TRX, TYPE_PROFILE_PROFESSIONAL_TRX }  from 'constants/TransactionInfoConstant.js'


const AddRevenue = () => {
    return (<AddForm 
                componentsView={TYPE_PROFESSIONAL_BUSINESS_REVENUE_TRX}
                profileType={TYPE_PROFILE_PROFESSIONAL_TRX}
            />)
}

export default AddRevenue;