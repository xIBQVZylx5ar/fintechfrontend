import React from "react";
import { Redirect, Route, Switch } from 'react-router-dom';
import EditRevenue from './Edit-Revenue'
import AddRevenue from './Add-Revenue';
import RevenueDetail from './Revenue-Detail';

const Revenues = ({ match }) => {
  return(
      <Switch>
        <Route path={`${match.url}/edit-revenue`} component={() => <EditRevenue/>} />
        <Route path={`${match.url}/add-revenue`} component={() => <AddRevenue/>} />
        <Route path={`${match.url}/revenue-detail`} component={() => <RevenueDetail />} />
        <Redirect from={`${match.url}`} to={`${match.url}/revenue-detail`} />
      </Switch>
  )
};

export default Revenues;