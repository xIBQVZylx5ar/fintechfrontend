import AddForm from 'components/dynamic-components/addForm';
import { TYPE_PROFESSIONAL_BUSINESS_SGA_TRX, TYPE_PROFILE_PROFESSIONAL_TRX, TYPE_SGA_EXPENSES }  from 'constants/TransactionInfoConstant.js'


const AddCapex = () => {
    return (<AddForm 
                componentsView={TYPE_PROFESSIONAL_BUSINESS_SGA_TRX}
                profileType={TYPE_PROFILE_PROFESSIONAL_TRX}
                exp_type={TYPE_SGA_EXPENSES}
            />)
}

export default AddCapex;