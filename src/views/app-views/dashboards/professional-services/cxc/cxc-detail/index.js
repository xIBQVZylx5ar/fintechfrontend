import { useHistory } from "react-router-dom";
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Card, DatePicker, Button, Form, Spin, notification } from "antd";
import { SearchOutlined, PlusCircleOutlined } from '@ant-design/icons';

import TransactionList from 'components/dynamic-components/transaction-dropdown/'
import { TYPE_PROFESSIONAL_BUSINESS_CXC_TRX, TYPE_PROFILE_PROFESSIONAL_TRX } from 'constants/TransactionInfoConstant.js'
import { getAllTrx } from 'redux/actions/trx';

const Expense = (props) => {
    const { getAllTrx, loading } = props;
    const [form] = Form.useForm();

    useEffect(() => {
        const today = new Date();

        getAllTrx({
            transaction_type: TYPE_PROFESSIONAL_BUSINESS_CXC_TRX,
            profile_id: TYPE_PROFILE_PROFESSIONAL_TRX,
            start_date: new Date(today.getFullYear(), '01', '01'),
            end_date: new Date(today.getFullYear(), '12', '31'),
        });
    }, []);
    const searchByDates = (values) => {
        const sDate = values.starDate;
        const eDate = values.endDate;

        if (!sDate || !eDate) {
            notification['error']({
              message: 'Notificación',
              description: 'Las fechas son requeridas',
            });
        } else if (new Date(eDate) < new Date(sDate)) {
            notification['error']({
                message: 'Notificación',
                description: 'La fecha inicial debe ser previa a la fecha final',
            });
        } else {
            getAllTrx({
                transaction_type: TYPE_PROFESSIONAL_BUSINESS_CXC_TRX,
                profile_id: TYPE_PROFILE_PROFESSIONAL_TRX,
                start_date: new Date(sDate),
                end_date: new Date(eDate),
            });
        }
    }

    let history = useHistory();
    const title = 'Cuentas por cobrar (CXC)';

    const addProduct = () => {
        history.push('./add-cxc');
    };
    
    return (
        <>
            <Spin spinning={loading}>
                <Form
                    layout="vertical"
                    form={form}
                    name="advanced_search"
                    onFinish={searchByDates}
                    className="ant-advanced-search-form">
                    <Card >
                        <h1>{title}</h1>
    
                        <Card >
    
                            <div className="ant-row">
                                <div className="ant-col ant-col-6">
                                    <label>Fecha de inicio</label>
                                    <Form.Item name='starDate'>
                                        <DatePicker
                                        ></DatePicker>
                                    </Form.Item>
    
                                </div>
                                <div className="ant-col ant-col-6">
                                    <label>Fecha final</label>
                                    <Form.Item name='endDate'>
                                        <DatePicker
                                        ></DatePicker>
                                    </Form.Item>
                                </div>
    
                                <div className="ant-col ant-col-6">
    
                                    <Button htmlType="submit" icon={<SearchOutlined />}>Consultar fechas</Button>
    
    
                                </div>
                                <div className="ant-col ant-col-6">
    
                                    <Button
                                        onClick={addProduct}
                                        type="primary"
                                        icon={<PlusCircleOutlined />}
                                        block
                                    >Añadir cuenta por cobrar
                                    </Button>
                                </div>
                            </div>
                        </Card>
    
    
                        <div className="ant-row">
                            <div className="ant-col ant-col-24">
                                <Card >
                                    < TransactionList type_list={TYPE_PROFESSIONAL_BUSINESS_CXC_TRX} profile={TYPE_PROFILE_PROFESSIONAL_TRX} />
                                </Card>
                            </div>
                        </div>
                    </Card>
                </Form>
            </Spin>
        </>
    )
}

const mapStateToProps = ({ trx }) => {
    const { transactions, loading } = trx;
    return { transactions, loading };
};

const mapDispatchToProps = {
    getAllTrx
};


export default connect(mapStateToProps, mapDispatchToProps)(Expense)