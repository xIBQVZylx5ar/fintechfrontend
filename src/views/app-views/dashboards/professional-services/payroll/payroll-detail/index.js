import { useHistory } from "react-router-dom";
import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Card, DatePicker, Button, Spin } from "antd";
import { SearchOutlined, PlusCircleOutlined } from "@ant-design/icons";

import PayrollList from "components/dynamic-components/payroll-dropdown";
import { TYPE_PROFILE_PROFESSIONAL_TRX } from "constants/TransactionInfoConstant.js";
import { getAllPayrolls } from "redux/actions/payroll";

const Payroll = (props) => {
  const { loading, getAllPayrolls } = props;

  let history = useHistory();
  const title = "Planilla";

  const addProduct = () => {
    history.push("./add-payroll");
  };

  const addCollaborator = () => {
    history.push("./add-collaborator");
  };

  useEffect(() => {
    getAllPayrolls({
      profile_id: TYPE_PROFILE_PROFESSIONAL_TRX,
    });
  }, []);

  return (
    <Spin spinning={loading}>
      <Card>
        <h1>{title}</h1>

        <Card>
          <div className="ant-row ant-row-middle">
            <div className="ant-col ant-col-6">
              <label>Fecha de inicio</label>
              <DatePicker></DatePicker>
            </div>
            <div className="ant-col ant-col-6">
              <label>Fecha final</label>
              <DatePicker></DatePicker>
            </div>
            <div className="ant-col ant-col-6">
              <Button icon={<SearchOutlined />}>Consultar fechas</Button>
            </div>
            <div className="ant-col ant-col-6">
              <Button
                onClick={addCollaborator}
                type="primary"
                icon={<PlusCircleOutlined />}
                block
              >
                Añadir colaborador
              </Button>
              <Button
                onClick={addProduct}
                type="primary"
                icon={<PlusCircleOutlined />}
                block
                className="mt-2"
              >
                Añadir planilla
              </Button>
            </div>
          </div>
        </Card>

        <div className="ant-row">
          <div className="ant-col ant-col-24">
            <Card>
              <PayrollList />
            </Card>
          </div>
        </div>
      </Card>
    </Spin>
  );
};

const mapStateToProps = ({ payroll }) => {
  const { payrolls, loading } = payroll;
  return { payrolls, loading };
};

const mapDispatchToPropps = {
  getAllPayrolls,
};

export default connect(mapStateToProps, mapDispatchToPropps)(Payroll);
