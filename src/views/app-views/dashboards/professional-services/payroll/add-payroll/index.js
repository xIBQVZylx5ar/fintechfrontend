import AddPayrollForm from "components/dynamic-components/addPayrollForm"
import { TYPE_PROFESSIONAL_BUSINESS_PAYROLL_TRX, TYPE_PROFILE_PROFESSIONAL_TRX } from "constants/TransactionInfoConstant"

const AddPayroll = () => {
    return (
        <AddPayrollForm
            componentsView={TYPE_PROFESSIONAL_BUSINESS_PAYROLL_TRX}
            profileType={TYPE_PROFILE_PROFESSIONAL_TRX}
        />
    )
}

export default AddPayroll