import AddForm from 'components/dynamic-components/addForm';
import { TYPE_PROFESSIONAL_BUSINESS_CAPEX_TRX, TYPE_PROFILE_PROFESSIONAL_TRX, TYPE_CAPEX_EXPENSES }  from 'constants/TransactionInfoConstant.js'


const AddCapex = () => {
    return (<AddForm 
                componentsView={TYPE_PROFESSIONAL_BUSINESS_CAPEX_TRX}
                profileType={TYPE_PROFILE_PROFESSIONAL_TRX}
                exp_type={TYPE_CAPEX_EXPENSES}
            />)
}

export default AddCapex;