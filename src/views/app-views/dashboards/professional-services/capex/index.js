import React from "react";
import { Redirect, Route, Switch } from 'react-router-dom';
import EditCapex from './edit-capex'
import AddCapex from './add-capex';
import CapexDetail from './capex-detail';

const Capex = ({ match }) => {
  return(
      <Switch>
        <Route path={`${match.url}/capex-detail`} component={() => <CapexDetail />} />
        <Route path={`${match.url}/edit-capex`} component={() => <EditCapex/>} />
        <Route path={`${match.url}/add-capex`} component={() => <AddCapex/>} />
        <Redirect from={`${match.url}`} to={`${match.url}/capex-detail`} />
      </Switch>
  )
};

export default Capex;