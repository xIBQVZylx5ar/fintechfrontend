import React from "react";
import { Redirect, Route, Switch } from 'react-router-dom';
import EditCogs from './edit-cogs';
import AddCogs from './add-cogs';
import CogsDetail from './cogs-detail';

const Cogs = ({ match }) => {
  return(
      <Switch>
        <Route path={`${match.url}/cogs-detail`} component={() => <CogsDetail />} />
        <Route path={`${match.url}/edit-cogs`} component={() => <EditCogs/>} />
        <Route path={`${match.url}/add-cogs`} component={() => <AddCogs/>} />
        <Redirect from={`${match.url}`} to={`${match.url}/cogs-detail`} />
      </Switch>
  )
};

export default Cogs;