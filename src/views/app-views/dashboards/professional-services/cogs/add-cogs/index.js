import AddForm from 'components/dynamic-components/addForm';
import { TYPE_PROFESSIONAL_BUSINESS_COGS_TRX, TYPE_PROFILE_PROFESSIONAL_TRX, TYPE_COGS_EXPENSE }  from 'constants/TransactionInfoConstant.js'


const AddCapex = () => {
    return (<AddForm 
                componentsView={TYPE_PROFESSIONAL_BUSINESS_COGS_TRX}
                profileType={TYPE_PROFILE_PROFESSIONAL_TRX}
                exp_type={TYPE_COGS_EXPENSE}
            />)
}

export default AddCapex;