import AddForm from 'components/dynamic-components/addForm';
import { TYPE_PROFESSIONAL_BUSINESS_CXP_TRX, TYPE_PROFILE_PROFESSIONAL_TRX, TYPE_CXC }  from 'constants/TransactionInfoConstant.js'


const AddCxp = () => {
    return (<AddForm 
                componentsView={TYPE_PROFESSIONAL_BUSINESS_CXP_TRX}
                profileType={TYPE_PROFILE_PROFESSIONAL_TRX}
                exp_type={TYPE_CXC}
            />)
}

export default AddCxp;