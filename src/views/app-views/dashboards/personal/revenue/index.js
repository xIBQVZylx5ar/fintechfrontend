import React from "react";
import { Redirect, Route, Switch } from 'react-router-dom';
import RevenueDetail from './revenue-detail';
import AddRevenue from './Add-Revenue';

const Revenue = ({ match }) => {
  return(
      <Switch>  
        <Route path={`${match.url}/revenue-detail`} component={() => <RevenueDetail/>} />
        <Route path={`${match.url}/add-revenue`} component={() => <AddRevenue/>} />
        <Redirect from={`${match.url}`} to={`${match.url}/revenue-detail`} />
      </Switch>
  )
};

export default Revenue;