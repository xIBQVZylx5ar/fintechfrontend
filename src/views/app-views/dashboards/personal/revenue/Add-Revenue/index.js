import AddForm from 'components/dynamic-components/addForm';
import { TYPE_PERSONAL_REVENUE_TRX, TYPE_PROFILE_PERSONAL_TRX }  from 'constants/TransactionInfoConstant.js'


const AddRevenue = () => {
    return (<AddForm 
                componentsView={TYPE_PERSONAL_REVENUE_TRX}
                profileType={TYPE_PROFILE_PERSONAL_TRX}
            />)
}

export default AddRevenue;