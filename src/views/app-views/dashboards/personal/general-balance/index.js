import { Row, Col, Card, Skeleton } from 'antd';
import React, { useEffect } from 'react'
import { connect } from 'react-redux';
import { getBalance , resetBalance } from 'redux/actions/Balance';
import { COLORS } from 'constants/ChartConstant';
import Flex from 'components/shared-components/Flex'
import ChartWidget from 'components/shared-components/ChartWidget';
import { COLOR_2, COLOR_4, COLOR_3 } from 'constants/ChartConstant';
import { Doughnut } from 'react-chartjs-2';
import Chart from "react-apexcharts";

const nameMonths = [
	'Enero',
	'Febrero',
	'Marzo',
	'Abril',
	'Mayo',
	'Junio',
	'Julio',
	'Agosto',
	'Setiembre',
	'Octubre',
	'Noviembre',
	'Diciembre',
];


const ChartExpenses = (dataExpenses) => {
	let history = (dataExpenses.dataExpenses.historyExpenses === undefined) ? [] : dataExpenses.dataExpenses.historyExpenses;
	let amounts = [];
	let months = [];
	history.map(data => {
		
		months.push(`${nameMonths[(data.date_part-1)]}`)
		
		amounts.push(data.sum)

		
	})

	const chartInfo = {
		series: [{
			name: 'Ingresos',
			data: amounts
		}],
		options: {
			plotOptions: {
				bar: {
					horizontal: false,
					columnWidth: '55%',
					endingShape: 'rounded'
				},
			},
			colors: [COLOR_3],
			dataLabels: {
				enabled: false
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			xaxis: {
				categories: months,
			},
			fill: {
				opacity: 1
			}
		}

	};
	return (<Card>
		<Row gutter={16}>
			<Col xs={24} sm={24} md={24} lg={8}>
				<Flex className="h-100" flexDirection="column" justifyContent="between">
					<div>
						<h4 className="mb-0">Egresos</h4>

					</div>
					<div className="mb-4">
						<h1 className="font-weight-bold">₡ {dataExpenses.dataExpenses.actualExpenses}</h1>
						<p>Monto total de egresos</p>
					</div>
				</Flex>
			</Col>
			<Col xs={24} sm={24} md={24} lg={16}>
				<p>Historico</p>
				<Chart
					options={chartInfo.options}
					series={chartInfo.series}
					height={300}
					type="bar"
				/>
			</Col>
		</Row>
	</Card>)
}
const ChartBalance = (currentBalance) => {
	let history = (currentBalance.dataBalance.data === undefined) ? [] : currentBalance.dataBalance.data;
	let amounts = [];
	let months = [];
	history.map(data => {
		let formmartMonth = new Date(data.month)
		formmartMonth.setMinutes(formmartMonth.getMinutes() + formmartMonth.getTimezoneOffset())
		months.push(`${nameMonths[formmartMonth.getMonth()]}`)
		amounts.push(data.balance)
	})
	const data = {
		series: [
			{
				name: 'Monto',
				data: amounts
			}
		],
		categories: months
	}
	return (<Card>
		<Row gutter={16}>
			<Col xs={24} sm={24} md={24} lg={8}>
				<Flex className="h-100" flexDirection="column" justifyContent="between">
					<div>
						<h4 className="mb-0">Balance general</h4>

					</div>
					<div className="mb-4">
						<h1 className="font-weight-bold">₡ {currentBalance.dataBalance.actualBalance.balance}</h1>
						<p>Monto del balance general al mes actual.</p>
					</div>
				</Flex>
			</Col>
			<Col xs={24} sm={24} md={24} lg={16}>
				<p>Histórico</p>
				<ChartWidget
					card={false}
					series={data.series}
					xAxis={data.categories}
					height={250}
					type="bar"
					customOptions={{ colors: COLOR_4 }}
					title="resumeBalance"
				/>
			</Col>
		</Row>
	</Card>)
}
const ChartRevenue = (dataRevenue) => {

	let history = (dataRevenue.dataRevenue.historyRevenue === undefined) ? [] : dataRevenue.dataRevenue.historyRevenue;


	let amounts = [];
	let months = [];
	history.map(data => {

		months.push(`${nameMonths[(data.date_part - 1)]}`)
		amounts.push(data.sum)
	})

	const state = {
		series: [{
			name: 'Ingresos',
			data: amounts
		}],
		options: {
			plotOptions: {
				bar: {
					horizontal: false,
					columnWidth: '55%',
					endingShape: 'rounded'
				},
			},
			colors: [COLOR_2],
			dataLabels: {
				enabled: false
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			xaxis: {
				categories: months,
			},
			fill: {
				opacity: 1
			}
		}

	};
	return (<Card>
		<Row gutter={16}>
			<Col xs={24} sm={24} md={24} lg={8}>
				<Flex className="h-100" flexDirection="column" justifyContent="between">
					<div>
						<h4 className="mb-0">Ingresos</h4>

					</div>
					<div className="mb-4">
						<h1 className="font-weight-bold">₡ {dataRevenue.dataRevenue.actualRevenue}</h1>
						<p>Monto total de ingresos.</p>
					</div>
				</Flex>
			</Col>
			<Col xs={24} sm={24} md={24} lg={16}>
				<p>Histórico</p>
				<>
					<Chart
						options={state.options}
						series={state.series}
						height={300}
						type="bar"
					/>
				</>
			</Col>
		</Row>
	</Card>)
}


const ComparativeChart = (dataChart) => {


	let history = (dataChart.dataChart.data === undefined) ? [] : dataChart.dataChart.data;


	let months = [];
	let revenueHistory = []
	let expensesHistory = []
	let balanceHistory = []
	history.map(data => {
		let formmartMonth = new Date(data.month)
		formmartMonth.setMinutes(formmartMonth.getMinutes() + formmartMonth.getTimezoneOffset())
		months.push(`${nameMonths[formmartMonth.getMonth()]}`)
		revenueHistory.push(parseFloat(data.revenue_total))
		expensesHistory.push(parseFloat(data.expenses_total))
		balanceHistory.push(parseFloat(data.balance))
	})

	
 let	dataLine = {
		series: [{
				name: "Ingresos",
				data: revenueHistory
			},
			{
				name: "Egresos",
				data: expensesHistory
			},
			{
				name: 'Balance',
				data: balanceHistory
			}
		],
		options: {
			chart: {
				height: 350,
				type: 'line',
				zoom: {
					enabled: false
				},
			},
			colors: [...COLORS],
			dataLabels: {
				enabled: false
			},
			stroke: {
				width: [3, 5, 3],
				curve: 'straight',
				dashArray: [0, 8, 5]
			},
			markers: {
				size: 0,
				hover: {
					sizeOffset: 6
				}
			},
			xaxis: {
				categories: months
			},
			grid: {
				borderColor: '#f1f1f1',
			}
		},
	};


	return (
		<Chart
				options={dataLine.options}
				series={dataLine.series}
				height = {360}
			/>
	)
}

const CategoryChart = (dataChart) => {
	
	let info = (dataChart.dataChart === undefined) ? [] : dataChart.dataChart;
	let categories = [];
	let amounts = [];
	info.map(data =>{
		categories.push(data.description);
		amounts.push(data.sum)
	})

	

	const data = {
		labels: categories,
		datasets: [
			{
				data: amounts,
				backgroundColor: [...COLORS],
				pointBackgroundColor: [...COLORS]
			}
		]
	}
	

	return(
		<Doughnut  data ={data} height={375} options={{responsive: true, maintainAspectRatio: false}}/>
	)
}


const GeneralBalance = (props) => {
	const { getBalance, databalance , resetBalance} = props;
	useEffect(() => {
		resetBalance();
		getBalance({
			user: 8,
			profile_id: 1
		})
	}, []);




	if (Object.keys(databalance).length === 0) {

		return (
			<Skeleton active={true} />
		)
	} else {

		

		const dataRevenue = {
			actualRevenue: (databalance.currentMonth.revenue_total ===undefined ) ? [] : databalance.currentMonth.revenue_total,
			historyRevenue: (databalance.historyRevenue === undefined) ? [] : databalance.historyRevenue
		}

		const dataExpenses = {
			actualExpenses: (databalance.currentMonth.expenses_total === undefined) ? [] : databalance.currentMonth.expenses_total,
			historyExpenses: (databalance.historyExpenses === undefined) ? [] : databalance.historyExpenses
		}



		const currentBalance = {
			actualBalance: (databalance.currentMonth === undefined) ? [] : databalance.currentMonth,
			data: (databalance.history === undefined) ? [] : databalance.history
		}

		

		const categoryResume = (databalance.categoryResume=== undefined) ? [] : databalance.categoryResume;

		
		


		return (
			<>
				<Row gutter={32}>
					<Col xs={32} sm={32} md={32} lg={24} xl={24} xxl={24}>
						<ChartBalance dataBalance={currentBalance} />
					</Col>
				</Row>
				<Row gutter={16}>
					<Col xs={24} sm={24} md={24} lg={12} xl={12} xxl={12}>
						<ChartRevenue dataRevenue={dataRevenue} />
					</Col>
					<Col xs={24} sm={24} md={24} lg={12} xl={12} xxl={12}>
						<ChartExpenses dataExpenses={dataExpenses} />
					</Col>
				</Row>
				<Row gutter={16}>
					<Col xs={24} sm={24} md={24} lg={12} xl={12} xxl={12} >
						<Card title ={"Comparativa histórica"}>
							<ComparativeChart dataChart={currentBalance} />
						</Card>
					</Col>
					<Col xs={24} sm={24} md={24} lg={12} xl={12} xxl={12}>
						<Card title= {'Egresos por categorías'}>
							<CategoryChart dataChart = {categoryResume}/>
						</Card>
					</Col>
				</Row>
			</>
		)
	}
}

const mapStateToProps = ({ balance }) => {

	const { databalance } = balance;

	return { databalance };
}

const mapDispatchToProps = {
	getBalance , resetBalance
};

export default connect(mapStateToProps, mapDispatchToProps)(GeneralBalance);




