import AddForm from 'components/dynamic-components/addForm';
import { TYPE_PERSONAL_EXPENSES_TRX, TYPE_PROFILE_PERSONAL_TRX, TYPE_PERSONAL_EXPENSES }  from 'constants/TransactionInfoConstant.js'


const AddExpense = () => {
    return (<AddForm 
                componentsView={TYPE_PERSONAL_EXPENSES_TRX}
                profileType={TYPE_PROFILE_PERSONAL_TRX}
                exp_type={TYPE_PERSONAL_EXPENSES}
            />)
}

export default AddExpense;