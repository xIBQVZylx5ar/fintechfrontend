import React from "react";
import { Redirect, Route, Switch } from 'react-router-dom';
import ExpenseDetail from './expense-detail';
import AddExpense from './add-expense';

const Expense = ({ match }) => {
  return(
      <Switch>  
        <Route path={`${match.url}/expense-detail`} component={() => <ExpenseDetail/>} />
        <Route path={`${match.url}/add-expense`} component={() => <AddExpense/>} />
        <Redirect from={`${match.url}`} to={`${match.url}/expense-detail`} />
      </Switch>
  )
};

export default Expense;