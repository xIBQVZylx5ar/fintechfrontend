import React, { useState, useEffect } from 'react';
import { connect } from "react-redux";
import { Input, Row, Col, Card, Collapse, Form, Button, notification, Spin } from 'antd';
import { RightOutlined } from '@ant-design/icons';

import PageHeaderAlt from 'components/layout-components/PageHeaderAlt';
import Flex from "components/shared-components/Flex";
import BlogList from 'components/dynamic-components/blog-dropdown';
import CommentList from "components/dynamic-components/comments-list"
import { faqCategories, faqList } from './faqData';
import { blogInsert, getAllBlogs, resetShowMessage } from 'redux/actions/blog';
import { commentInsert } from 'redux/actions/comments';
import { isUserTypeAuthorized } from 'configs/Session'
import { TYPE_USER } from 'constants/TransactionInfoConstant';

const { Panel } = Collapse;

const Faq = (props) => {
	const [ curentCategory ] = useState('faq-1');
	const [form] = Form.useForm();

  	const { 
		blogInsert,
		getAllBlogs,
		resetShowMessage,
		message, 
		errorCode, 
		loading
		
	} = props;

	
	const onAdd = (values) => {
		blogInsert(values)
	}


	const rules = {
		content: [
			{
			  required: true,
			  message: "Por favor ingrese un contenido",
			},
		],
	}

	useEffect(() => {
		if (message) {
			form.resetFields();
			notification['success']({
				message: 'Notificación',
				description: message,
			});
			resetShowMessage();
			getAllBlogs();
		}
		
		if (errorCode) {
			form.resetFields()
			notification['error']({
				message: 'Notificación',
				description: errorCode,
			});
			resetShowMessage();
		}
	})

	useEffect(() => {
		getAllBlogs()
	}, [])

	return (
		<>
		<Spin spinning={loading}>
			<PageHeaderAlt className="bg-primary" overlap>
				<div className="container text-center">
					<div className="py-lg-4">
						<h1 className="text-white display-4">Blog Informativo</h1>
						<Row type="flex" justify="center">
							<Col xs={24} sm={24} md={12}>
								<p className="text-white w-75 text-center mt-2 mb-4 mx-auto">
									Aqui se encuentran temas de ayuda para el uso de esta aplicacion, asi como asuntos sobre educacion financiera.
								</p>
							</Col>
						</Row>
						<br></br>
						
					</div>
				</div>
			</PageHeaderAlt>
			<div className="container my-4">
				<Row gutter={16}>
					{faqCategories.map(elm => (
						<Col xs={24} sm={24} md={8} key={elm.key}>
								<br></br>
						</Col>
					))}
				</Row>
				<br></br>
				{isUserTypeAuthorized(TYPE_USER)
				? 
					<div className="mt-4">
						<CommentList/>
					</div>
				:
				<>
					<Card className="mt-4">
					<Collapse
						accordion
						defaultActiveKey={'faq-1-1'}
						bordered={false}
						expandIcon={({ isActive }) => <RightOutlined className="text-primary" type="right" rotate={isActive ? 90 : 0} />}

					>
						{faqList.filter(elm => elm.id === curentCategory)[0].data.map(elm => (
							<Panel header={elm.title} key={elm.key}>
								<p>{elm.desc}</p>
							</Panel>
						))}


					</Collapse>
					
					<Form
						layout="vertical"
						form={form}
						name="advanced_search"
						onFinish={onAdd}
						className="ant-advanced-search-form"
					>
						<div className="ant-row">
							<div className="ant-col ant-col-24">
								<Form.Item
									name="content"
									rules={rules.content}
								>
									<Input.TextArea rows={4} />
								</Form.Item>
							</div>
						</div>

						<Flex
							className="py-2"
							mobileFlex={false}
							justifyContent="between"
							alignItems="center"
						>
							<Button
								type="primary"
								htmlType="submit"
							>
								Publicar
							</Button>

              			</Flex>
					</Form>
				</Card>
				
				<BlogList/>
				</>
				}
			</div>
		</Spin>
		</>
	);
}

const mapStateToProps = ({ blog }) => {
	const { message, showMessage, errorCode, loading, posts } = blog;
  	return { message, showMessage, errorCode, loading, posts}
}

const mapDispatchToProps = {
	blogInsert,
	commentInsert,
	getAllBlogs,
	resetShowMessage
}

export default connect(mapStateToProps, mapDispatchToProps)(Faq)
