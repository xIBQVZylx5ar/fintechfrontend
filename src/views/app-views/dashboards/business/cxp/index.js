import React from "react";
import { Redirect, Route, Switch } from 'react-router-dom';
import EditCxp from './edit-cxp';
import AddCxp from './add-cxp';
import CxpDetail from './cxp-detail';

const CXP = ({ match }) => {
  return(
      <Switch>
        <Route path={`${match.url}/cxp-detail`} component={() => <CxpDetail />} />
        <Route path={`${match.url}/edit-cxp`} component={() => <EditCxp/>} />
        <Route path={`${match.url}/add-cxp`} component={() => <AddCxp/>} />
        <Redirect from={`${match.url}`} to={`${match.url}/cxp-detail`} />
      </Switch>
  )
};

export default CXP;