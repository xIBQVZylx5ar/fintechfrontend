import React from "react";
import { Redirect, Route, Switch } from 'react-router-dom';
import EditSga from './edit-sga'
import AddSga from './add-sga';
import SgaDetail from './sga-detail';

const Sga = ({ match }) => {
  return(
      <Switch>
        <Route path={`${match.url}/sga-detail`} component={() => <SgaDetail />} />
        <Route path={`${match.url}/edit-sga`} component={() => <EditSga/>} />
        <Route path={`${match.url}/add-sga`} component={() => <AddSga/>} />
        <Redirect from={`${match.url}`} to={`${match.url}/sga-detail`} />
      </Switch>
  )
};

export default Sga;