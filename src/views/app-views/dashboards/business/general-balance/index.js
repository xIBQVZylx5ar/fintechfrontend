import { Row, Col, Card, Skeleton, Statistic } from 'antd';
import React, { useEffect } from 'react'
import { connect } from 'react-redux';
import { getBalance, resetBalance } from 'redux/actions/Balance';
import Flex from 'components/shared-components/Flex'
import ChartWidget from 'components/shared-components/ChartWidget';
import { COLOR_2, COLOR_4, COLORS } from 'constants/ChartConstant';
import { Doughnut } from 'react-chartjs-2';
import Chart from "react-apexcharts";
import { UpOutlined, ShopOutlined, GoldOutlined, HeartOutlined, PercentageOutlined, BankOutlined, ShoppingCartOutlined, UserOutlined } from '@ant-design/icons';
const nameMonths = [
	'Enero',
	'Febrero',
	'Marzo',
	'Abril',
	'Mayo',
	'Junio',
	'Julio',
	'Agosto',
	'Setiembre',
	'Octubre',
	'Noviembre',
	'Diciembre',
];


const ChartExpenses = (dataExpenses) => {


	const { actualBalance } = dataExpenses.dataExpenses;

	let currentMonth = (!actualBalance[0]) ? [] : actualBalance[0];

	const { sga_total, capex_total, cogs_total, payroll_total } = currentMonth
	let amounts = [parseFloat(sga_total), parseFloat(capex_total), parseFloat(cogs_total), parseFloat(payroll_total)];
	let categories = ['SG&A', 'CAPEX', 'COGS', 'PLANILLA'];


	let expenses_total = parseFloat(sga_total) + parseFloat(capex_total) + parseFloat(cogs_total) + parseFloat(payroll_total);

	const chartInfo = {
		series: [{
			name: 'Gastos',
			data: amounts
		}],
		options: {
			plotOptions: {
				bar: {
					horizontal: false,
					columnWidth: '55%',
					endingShape: 'rounded'
				},
			},
			colors: COLORS,
			dataLabels: {
				enabled: false
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			xaxis: {
				categories: categories,
			},
			fill: {
				opacity: 1
			}
		}

	};
	return (<Card>
		<Row gutter={16}>
			<Col xs={24} sm={24} md={24} lg={8}>
				<Flex className="h-100" flexDirection="column" justifyContent="between">
					<div>
						<h4 className="mb-0">Egresos</h4>
					</div>
					<div className="mb-4">
						<h1 className="font-weight-bold">₡ {expenses_total}</h1>
						<p>Monto total de egresos</p>
					</div>
				</Flex>
			</Col>
			<Col xs={24} sm={24} md={24} lg={16}>
				<p>Gastos actuales</p>
				<Chart
					options={chartInfo.options}
					series={chartInfo.series}
					height={300}
					type="bar"
				/>
			</Col>
		</Row>
	</Card>)
}
const ChartBalance = (currentBalance) => {

	let amounts = [];
	let months = [];

	const { actualBalance, history } = currentBalance.dataBalance;
	let currentMonth = (!actualBalance[0]) ? [] : actualBalance[0];

	history.map(data => {
		let formmartMonth = new Date(data.month)
		formmartMonth.setMinutes(formmartMonth.getMinutes() + formmartMonth.getTimezoneOffset())
		months.push(`${nameMonths[formmartMonth.getMonth()]}`)
		amounts.push(data.balance)
	})
	const data = {
		series: [
			{
				name: 'Monto',
				data: amounts
			}
		],
		categories: months
	}
	return (<Card>
		<Row gutter={16}>
			<Col xs={24} sm={24} md={24} lg={8}>
				<Flex className="h-100" flexDirection="column" justifyContent="between">
					<div>
						<h4 className="mb-0">Balance general</h4>

					</div>
					<div className="mb-4">
						<h1 className="font-weight-bold">₡ {currentMonth.balance}</h1>
						<p>Monto del balance general al mes actual.</p>
					</div>
				</Flex>
			</Col>
			<Col xs={24} sm={24} md={24} lg={16}>
				<p>Histórico</p>
				<ChartWidget
					card={false}
					series={data.series}
					xAxis={data.categories}
					height={300}
					type="bar"
					customOptions={{ colors: COLOR_4 }}
					title="resumeBalance"
				/>
			</Col>
		</Row>
	</Card>)
}
const ChartRevenue = (dataRevenue) => {
	let amounts = [];
	let months = [];
	const { actualBalance, history } = dataRevenue.dataRevenue;
	let currentMonth = (!actualBalance[0]) ? [] : actualBalance[0];

	history.map(data => {
		let formmartMonth = new Date(data.month)
		formmartMonth.setMinutes(formmartMonth.getMinutes() + formmartMonth.getTimezoneOffset())
		months.push(`${nameMonths[formmartMonth.getMonth()]}`)
		amounts.push(data.revenue_total)
	})




	const state = {
		series: [{
			name: 'Ingresos',
			data: amounts
		}],
		options: {
			plotOptions: {
				bar: {
					horizontal: false,
					columnWidth: '55%',
					endingShape: 'rounded'
				},
			},
			colors: [COLOR_2],
			dataLabels: {
				enabled: false
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			xaxis: {
				categories: months,
			},
			fill: {
				opacity: 1
			}
		}

	};
	return (<Card>
		<Row gutter={16}>
			<Col xs={24} sm={24} md={24} lg={8}>
				<Flex className="h-100" flexDirection="column" justifyContent="between">
					<div>
						<h4 className="mb-0">Ingresos</h4>

					</div>
					<div className="mb-4">
						<h1 className="font-weight-bold">₡ {currentMonth.revenue_total}</h1>
						<p>Monto total de ingresos.</p>
					</div>
				</Flex>
			</Col>
			<Col xs={24} sm={24} md={24} lg={16}>
				<p>Histórico</p>
				<>
					<Chart
						options={state.options}
						series={state.series}
						height={300}
						type="bar"
					/>
				</>
			</Col>
		</Row>
	</Card>)
}

const CategoryChart = (dataChart) => {

	let info = (dataChart.dataChart === undefined) ? [] : dataChart.dataChart;
	const { history } = info;
	let sga = 0;
	let capex = 0;
	let cogs = 0;
	let payroll = 0;
	history.forEach(data => {
		sga = sga + parseFloat(data.sga_total);
		capex = capex + parseFloat(data.capex_total);
		cogs = cogs + parseFloat(data.cogs_total);
		payroll = payroll + parseFloat(data.payroll)

	});

	const data = {
		labels: ['SG&A', 'CAPEX', 'COGS', 'PLANILLA'],
		datasets: [
			{
				data: [sga, capex, cogs, payroll],
				backgroundColor: [...COLORS],
				pointBackgroundColor: [...COLORS]
			}
		]
	}


	return (


		<Card title={'Historico de gastos'}>
			<Doughnut data={data} height={315} options={{ responsive: true, maintainAspectRatio: false }} />
		</Card>



	)
}

const DispayInfo = (info) => {

	const { actualBalance } = info.info;
	let revenue_total
	let ccss_total
	let iva_total
	let renta_total
	let sga_total
	let cogs_total
	let capex_total
	let payroll
	if (!actualBalance[0]) {
		revenue_total = 0
		ccss_total = 0
		iva_total = 0
		renta_total = 0
		sga_total = 0
		cogs_total = 0
		capex_total = 0
		payroll = 0

	} else {

		revenue_total = actualBalance[0].revenue_total
		ccss_total = actualBalance[0].ccss_total
		iva_total = actualBalance[0].iva_total
		renta_total = actualBalance[0].renta_total
		sga_total = actualBalance[0].sga_total
		cogs_total = actualBalance[0].cogs_total
		capex_total = actualBalance[0].capex_total
		payroll = actualBalance[0].payroll_total

	}
	return (
		<div>
			<Row gutter={32}>
				<Col span={6}> <Card>	<Statistic title="Ingresos" value={`₡ ${revenue_total}`} prefix={<GoldOutlined style={{ color: '#2ECC71' }} />} /> </Card> </Col>
				<Col span={6}> <Card>	<Statistic title="CCSS" value={`₡ ${ccss_total}`} prefix={<HeartOutlined style={{ color: '#E74C3C' }} />} /> </Card> </Col>
				<Col span={6}> <Card>	<Statistic title="IVA" value={`₡ ${iva_total}`} prefix={<PercentageOutlined style={{ color: '#E67E22' }} />} /> </Card> </Col>
				<Col span={6}> <Card>	<Statistic title="Renta" value={`₡ ${renta_total}`} prefix={<BankOutlined style={{ color: '#F1948A' }} />} /> </Card> </Col>
			</Row>
			<Row gutter={32}>
				<Col span={6}> <Card>	<Statistic title="SG&A" value={`₡ ${sga_total}`} prefix={<ShopOutlined style={{ color: '#BB8FCE' }} />} /> </Card> </Col>
				<Col span={6}> <Card>	<Statistic title="COGS" value={`₡ ${cogs_total}`} prefix={<UpOutlined style={{ color: '#76D7C4' }} />} /> </Card> </Col>
				<Col span={6}> <Card>	<Statistic title="CAPEX" value={`₡ ${capex_total}`} prefix={<ShoppingCartOutlined style={{ color: '#F5B041' }} />} /> </Card> </Col>
				<Col span={6}> <Card>	<Statistic title="Planilla" value={`₡ ${payroll}`} prefix={<UserOutlined style={{ color: '#7FB3D5' }} />} /> </Card> </Col>

			</Row>
		</div>


	)
}

const GeneralBalance = (props) => {
	const { getBalance, databalance, resetBalance } = props;
	useEffect(() => {
		resetBalance();
		getBalance({
			profile_id: 3
		})
	}, []);




	if (Object.keys(databalance).length === 0) {

		return (
			<Skeleton active={true} />
		)
	} else {

		const currentBalance = {
			actualBalance: (databalance.currentMonth === undefined) ? [] : databalance.currentMonth,
			history: (databalance.balanceHistory === undefined) ? [] : databalance.balanceHistory
		}


		return (
			<>
				<Card>
					<DispayInfo info={currentBalance} />
				</Card>
				<Row gutter={16}>
					<Col xs={24} sm={24} md={24} lg={12} xl={12} xxl={12}>
						<Card>
							<ChartRevenue dataRevenue={currentBalance} />
						</Card>
					</Col>
					<Col xs={24} sm={24} md={24} lg={12} xl={12} xxl={12}>
						<Card>
							<ChartBalance dataBalance={currentBalance} />
						</Card>
					</Col>
				</Row>
				<Row gutter={16}>
					<Col xs={24} sm={24} md={24} lg={12} xl={12} xxl={12} >
						<Card>
							<ChartExpenses dataExpenses={currentBalance} />
						</Card>
					</Col>
					<Col xs={24} sm={24} md={24} lg={12} xl={12} xxl={12}>
						<Card>
							<CategoryChart dataChart={currentBalance} />
						</Card>
					</Col>
				</Row>
			</>
		)
	}
}

const mapStateToProps = ({ balance }) => {

	const { databalance } = balance;

	return { databalance };
}

const mapDispatchToProps = {
	getBalance, resetBalance
};

export default connect(mapStateToProps, mapDispatchToProps)(GeneralBalance);




