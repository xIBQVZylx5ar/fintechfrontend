import React from "react";
import { Redirect, Route, Switch } from 'react-router-dom';
import EditCxc from './edit-cxc';
import AddCxc from './add-cxc';
import CxcDetail from './cxc-detail';

const CXC = ({ match }) => {
  return(
      <Switch>
        <Route path={`${match.url}/cxc-detail`} component={() => <CxcDetail />} />
        <Route path={`${match.url}/edit-cxc`} component={() => <EditCxc/>} />
        <Route path={`${match.url}/add-cxc`} component={() => <AddCxc/>} />
        <Redirect from={`${match.url}`} to={`${match.url}/cxc-detail`} />
      </Switch>
  )
};

export default CXC;