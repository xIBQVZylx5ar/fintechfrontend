import AddForm from 'components/dynamic-components/addForm';
import { TYPE_PROFESSIONAL_BUSINESS_REVENUE_TRX, TYPE_PROFILE_BUSINESS_TRX }  from 'constants/TransactionInfoConstant.js'



const EditRevenue = () => {
    return (<AddForm 
                componentsView={TYPE_PROFESSIONAL_BUSINESS_REVENUE_TRX}
                profileType={TYPE_PROFILE_BUSINESS_TRX}
                editableForm ={true}
            />)
}

export default EditRevenue;