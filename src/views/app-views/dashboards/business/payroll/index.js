import React from "react";
import { Redirect, Route, Switch } from 'react-router-dom';
import EditPayroll from './edit-payroll'
import AddPayroll from './add-payroll';
import PayrollDetail from './payroll-detail';
import AddCollaborator from './add-collaborator';

const Payroll = ({ match }) => {
  return(
      <Switch>
        <Route path={`${match.url}/payroll-detail`} component={() => <PayrollDetail />} />
        <Route path={`${match.url}/edit-payroll`} component={() => <EditPayroll/>} />
        <Route path={`${match.url}/add-payroll`} component={() => <AddPayroll/>} />
        <Route path={`${match.url}/add-collaborator`} component={() => <AddCollaborator/>} />
        <Redirect from={`${match.url}`} to={`${match.url}/payroll-detail`} />
      </Switch>
  )
};

export default Payroll;