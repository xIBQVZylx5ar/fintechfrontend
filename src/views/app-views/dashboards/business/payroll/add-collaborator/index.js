import AddPayrollForm from "components/dynamic-components/addPayrollForm"
import { TYPE_PROFESSIONAL_BUSINESS_COLLABORATOR_TRX } from "constants/TransactionInfoConstant"

const AddCollaborator = () => {
    return (
        <AddPayrollForm 
            componentsView={TYPE_PROFESSIONAL_BUSINESS_COLLABORATOR_TRX}
        />
    )
}

export default AddCollaborator