import React from "react";
import { Redirect, Route, Switch } from 'react-router-dom';
import generalBalance from './general-balance/';
import revenue from './revenue';
import capex from './capex';
import sga from './sga';
import cogs from './cogs';
import cxc from './cxc';
import cxp from './cxp';
import AddForm from "components/dynamic-components/addForm";
import payroll from './payroll';

const Business = ({ match }) => {
  return(
      <Switch>
        <Route path={`${match.url}/general-balance`} component={generalBalance} />
        <Route path={`${match.url}/revenue`} component={revenue} />
        <Route path={`${match.url}/capex`} component={capex} />
        <Route path={`${match.url}/sga`} component={sga} />
        <Route path={`${match.url}/cogs`} component={cogs} />
        <Route path={`${match.url}/cxc`} component={cxc} />
        <Route path={`${match.url}/cxp`} component={cxp} />
        <Route path={`${match.url}/taxation`} component={() => <AddForm componentsView={'Taxation'} profileType={3} />} />
        <Route path={`${match.url}/payroll`} component={payroll} />
        <Redirect from={`${match.url}`} to={`${match.url}/general-balance`} />
      </Switch>
  )
};

export default Business;