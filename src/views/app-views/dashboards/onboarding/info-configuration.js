import React from 'react';
import { Form, Card, InputNumber, Button } from "antd";

const RequestInfo = props => {

    const [form] = Form.useForm();

    const FinishRequestInfo = () => {

    }

    const rules = {
        amount: [
          {
            required: true,
            message: "profesional.services.input.number",
          },
        ]
    }

	return (
		<div className="main">
            <Card>
                <Form form={form} layout="vertical" name="request-info-form" onFinish={FinishRequestInfo}>
                    <h2>Agredecemos que nos indíques cuánto dinero tienes a tu favor</h2>
                    <Form.Item
                    name="amount"
                    label="Monto"
                    rules={rules.amount}
                    >
                        <InputNumber style={{ width: "75%" }}></InputNumber>
                    </Form.Item>

                    <Form.Item>
                        <Button type="primary" htmlType="submit" block>
                            Guardar
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
        </div>
	)
}

export default RequestInfo