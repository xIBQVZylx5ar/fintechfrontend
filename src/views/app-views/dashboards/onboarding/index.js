import React from 'react'
import { Row, Col } from "antd";
import { useSelector } from 'react-redux'
import RequestInfo from './info-configuration';

const backgroundURL = '/img/others/img-17.jpg'
const backgroundStyle = {
	backgroundImage: `url(${backgroundURL})`,
	backgroundRepeat: 'no-repeat',
	backgroundSize: 'cover'
}

const Onboarding = props => {
	const theme = useSelector(state => state.theme.currentTheme)

	return (
		<div className={`h-100 ${theme === 'light' ? 'bg-white' : ''}`}>
			<Row justify="center" className="align-items-stretch h-100">
            <   Col xs={0} sm={0} md={0} lg={8}>
					<div className="d-flex flex-column justify-content-between h-100 px-4" style={backgroundStyle}>
						<div className="text-right">
							<img src="/img/logo-white.png" alt="logo"/>
						</div>
						<Row justify="center">
							<Col xs={0} sm={0} md={0} lg={20}>
								<img className="img-fluid mb-5" src="/img/others/img-19.png" alt=""/>
								<h1 className="text-white">Introducción</h1>
								<p className="text-white">Ayúdanos a completar información importante para que puedas iniciar en la plataforma de la mejor forma.</p>
							</Col>
						</Row>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
					</div>
				</Col>
				<Col xs={20} sm={20} md={24} lg={16}>
					<div className="container d-flex flex-column justify-content-center h-100">
						<Row justify="center">
							<Col xs={24} sm={24} md={20} lg={12} xl={8}>
								<div className="mt-4">
									<RequestInfo></RequestInfo>
								</div>
							</Col>
						</Row>
					</div>
				</Col>
			</Row>
		</div>
	)
}

export default Onboarding