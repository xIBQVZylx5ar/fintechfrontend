import React, { useState, useEffect } from 'react'
import { Card, Row, Col, Form, Input, Button , notification  } from "antd";
import { MailOutlined } from '@ant-design/icons';
import { connect } from "react-redux";
import {restorePassword , resetMessage} from 'redux/actions/Auth'
const backgroundStyle = {
	backgroundImage: 'url(/img/others/img-17.jpg)',
	backgroundRepeat: 'no-repeat',
	backgroundSize: 'cover'
}

const ForgotPassword = (props) => {
	const {restorePassword, message, errorMessage  , resetMessage} = props;
	const [form] = Form.useForm();
	const [loading, setLoading] = useState(false);
	useEffect(()=>{
		if (message) {
			setLoading(false)
      notification['success']({
        message: 'Notificación',
        description: message,
      });
			resetMessage();
    }

    if (errorMessage) {
			setLoading(false)
      notification['error']({
        message: 'Notificación',
        description: errorMessage,
      });
			resetMessage(); 
    }		
	})
	const onSend = async (values) => {
		restorePassword(values)
	 	setLoading(true);		
		 resetMessage(); 
  };
	
	return (
		<div className="h-100" style={backgroundStyle}>
			<div className="container d-flex flex-column justify-content-center h-100">
				<Row justify="center">
					<Col xs={20} sm={20} md={20} lg={9}>
						<Card>
							<div className="my-2">
								<div className="text-center">
									<img className="img-fluid" src="/img/logo.png" alt="" />
									<h3 className="mt-3 font-weight-bold">¿Olvidaste tu contraseña?</h3>
									<p className="mb-4">Ingresá tu email de registro</p>
								</div>
								<Row justify="center">
									<Col xs={24} sm={24} md={20} lg={20}>
										<Form form={form} layout="vertical" name="forget-password" onFinish={onSend}>
											<Form.Item 
												name="email" 
												rules={
													[
														{ 
															required: true,
															message: 'Por favor ingrese su correo'
														},
														{ 
															type: 'email',
															message: 'Por favor ingrese un correo valido'
														}
													]
												}>
												<Input placeholder="Email" prefix={<MailOutlined className="text-primary" />}/>
											</Form.Item>
											<Form.Item>
												<Button loading={loading} type="primary" htmlType="submit" block>{loading? 'Sending' : 'Enviar'}</Button>
											</Form.Item>
										</Form>
									</Col>
								</Row>
							</div>
						</Card>
					</Col>
				</Row>
			</div>
		</div>
	)
}


const mapStateToProps = ({auth}) => {
	const { respInfo, message, errorMessage} = auth;
  	return {respInfo , message, errorMessage}
}

const mapDispatchToProps = {
	restorePassword, resetMessage
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword)






