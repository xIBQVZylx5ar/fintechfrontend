import React from 'react'
import { Card, Row, Col } from "antd";
import { connect } from "react-redux";
import {restorePassword} from 'redux/actions/Auth'
import ChangePasswordForm from '../../components/ChangePasswordForm'
const backgroundStyle = {
	backgroundImage: 'url(/img/others/img-17.jpg)',
	backgroundRepeat: 'no-repeat',
	backgroundSize: 'cover'
}

const ChangePassword = (props) => {

	return (
		<div className="h-100" style={backgroundStyle}>
			<div className="container d-flex flex-column justify-content-center h-100">
				<Row justify="center">
					<Col xs={20} sm={20} md={20} lg={9}>
						<Card>
							<div className="my-2">
								<div className="text-center">
									<img className="img-fluid" src="/img/logo.png" alt="" />
									<h3 className="mt-3 font-weight-bold">Recupera tu contraseña</h3>
								</div>
								<Row justify="center">
									<Col xs={24} sm={24} md={20} lg={20}>
									<ChangePasswordForm/>
									</Col>
								</Row>
							</div>
						</Card>
					</Col>
				</Row>
			</div>
		</div>
	)
}

//export default ForgotPassword



const mapStateToProps = ({auth}) => {
	const { respInfo} = auth;
  	return {respInfo}
}

const mapDispatchToProps = {
	restorePassword
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword)






