import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { AUTH_PREFIX_PATH } from 'configs/AppConfig';
import Login from './authentication/login';
import Register from './authentication/register';
import ForgotPassword from './authentication/forgot-password';
import Onboarding from '../app-views/dashboards/onboarding';
import  ChangePasswordForm from './authentication/change-password';

export const AuthViews = () => {
  return (
      <Switch>
        <Route path={`${AUTH_PREFIX_PATH}/login`} component={Login} />
        <Route path={`${AUTH_PREFIX_PATH}/register`} component={Register} />
        <Route path={`${AUTH_PREFIX_PATH}/onboarding`} component={Onboarding} />
        <Route path={`${AUTH_PREFIX_PATH}/forgot-password`} component={ForgotPassword} />
        <Route path ={`${AUTH_PREFIX_PATH}/change-password`} component={ChangePasswordForm} />
        <Redirect from={`${AUTH_PREFIX_PATH}`} to={`${AUTH_PREFIX_PATH}/login`} />
      </Switch>
  )
}

export default AuthViews;

