import React , { useEffect } from 'react';
import { useHistory } from "react-router-dom";
import { connect } from "react-redux";
import { Button, Form, Input, notification } from "antd";
import { LockOutlined } from '@ant-design/icons';
import PropTypes from 'prop-types';
import {changePassword , resetMessage} from 'redux/actions/Auth';
import { motion } from "framer-motion"



export const ChangePasswordForm = (props) => {
	const history = useHistory();
	const {
		changePassword , message, errorMessage , resetMessage
	} = props 
	useEffect(()=>{
		if (message) {
			
      notification['success']({
        message: 'Notificación',
        description: message,
      });
			resetMessage();
			history.push('/auth')
    }

    if (errorMessage) {
		
      notification['error']({
        message: 'Notificación',
        description: errorMessage,
      });
			resetMessage(); 
    }		
	});
	const onSend = values => {
		const queryString = window.location.search;
		const urlParams = new URLSearchParams(queryString);
		var code = urlParams.get('code');

		const { password1, password2 } = values;
		if (password1 === password2) {
			const info = {
				code: code,
				password: values.password1
			}
			
			changePassword(info)
		} else {
			setTimeout(() => {
				notification['warning']({
					message: 'UPS !!',
					description:
						'Las contraseñas no coinciden',
				});
			}, 1000);
		}
	};
	return (
		<>
			<motion.div
				initial={{ opacity: 0, marginBottom: 0 }}
				animate={{

				}}>

			</motion.div>
			<Form
				layout="vertical"
				name="login-form"
				onFinish={onSend}
			>
				<Form.Item
					name="password1"
					label={'Ingresa tu nuevo password'}
					rules={[
						{
							required: true,
							message: 'Por favor ingresa la contraseña',
						}
					]}
				>
					<Input.Password prefix={<LockOutlined className="text-primary" />} />
				</Form.Item>
				<Form.Item
					name="password2"
					label={'Confirma tu nuevo contraseña'}
					rules={[
						{
							required: true,
							message: 'Por favor ingresa la contraseña',
						}
					]}
				>
					<Input.Password prefix={<LockOutlined className="text-primary" />} />
				</Form.Item>
				<Form.Item>
					<Button type="primary" htmlType="submit" >
						Cambiar contraseña
					</Button>
				</Form.Item>

			</Form>
		</>
	)
}

ChangePasswordForm.propTypes = {
	otherSignIn: PropTypes.bool,
	showForgetPassword: PropTypes.bool,
	extra: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.element
	]),
};

ChangePasswordForm.defaultProps = {
	otherSignIn: true,
	showForgetPassword: false
};

const mapStateToProps = ({ auth }) => {
	const { psswdInfo , message, errorMessage} = auth;
	return { psswdInfo ,message, errorMessage}
}

const mapDispatchToProps = {
	changePassword , resetMessage
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangePasswordForm)
