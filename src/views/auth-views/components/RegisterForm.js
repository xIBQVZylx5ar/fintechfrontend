import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { LockOutlined, MailOutlined } from '@ant-design/icons';
import { Button, Form, Input, Alert, Checkbox, Card, InputNumber, message as messageNotification, Spin} from "antd";
import { register, showLoading, hideAuthMessage, authenticated } from 'redux/actions/Auth';
import { TYPE_PROFILE_PERSONAL_TRX, TYPE_PROFILE_PROFESSIONAL_TRX, TYPE_PROFILE_BUSINESS_TRX } from 'constants/TransactionInfoConstant'
import { motion } from "framer-motion";
import { isLogin } from 'configs/Session';

const STEP_1 = 'STEP-1';
const STEP_2 = 'STEP-2';
const STEP_3 = 'STEP-3';

const rules = {
	name: [
		{ 
			required: true,
			message: 'Favor ingrese su nombre'
		},
		{
			max: 30,
			message: 'El nombre debe poseer como máximo 30 caracteres'
		},
	],
	last_name: [
		{ 
			required: true,
			message: 'Favor ingrese el primer apellido'
		},
		{
			max: 30,
			message: 'El apellido debe poseer como máximo 30 caracteres'
		},
	],
	email: [
		{ 
			required: true,
			message: 'Favor ingrese un correo electrónico'
		},
		{ 
			type: 'email',
			message: 'Favor ingrese un correo electrónico válido'
		}
	],
	password: [
		{ 
			required: true,
			message: 'Favor ingrese una contraseña'
		},
		{
			min: 8,
			message: 'La contraseña debe poseer al menos 8 caracteres'
		},
		{
			max: 30,
			message: 'La contraseña debe poseer como máximo 30 caracteres'
		},
		{
			pattern: '^(?=.*[a-zA-Z])(?=.*[0-9])[A-Za-z0-9]+$',
			message: 'Debe contener números y letras'
		}
	],
	confirm: [
		{ 
			required: true,
			message: 'Favor confirme la contraseña'
		},
		({ getFieldValue }) => ({
			validator(rule, value) {
				if (!value || getFieldValue('password') === value) {
					return Promise.resolve();
				}
				return Promise.reject('Las contraseñas no coinciden');
			},
		})
	],
	amount: [
		{
		  required: true,
		  message: "El campo de monto es requerido",
		},
	  ]
}

export const RegisterForm = (props) => {

	const { loading, redirect, message, showMessage, register } = props
	const [ form ] = Form.useForm();
	const [ form_info_config ] = Form.useForm();
	const [ state, setState ] = React.useState(STEP_1);
	const [ valuesform, setValuesform ] = React.useState();

	useEffect(() => {
    	if (isLogin() && redirect) {
			window.location.href = '../' + redirect
		}
  	});

  	const StepFirstForm = () => {
		return (
		<div hidden={state===STEP_1 ? false : true}>
			<Form.Item 
				name="name" 
				label="Nombre"
				rules={rules.name}
				hasFeedback
			>
				<Input className="text-primary"/>
			</Form.Item>
			<Form.Item 
				name="last_name" 
				label="Primer Apellido"
				rules={rules.last_name}
				hasFeedback
			>
				<Input className="text-primary"/>
			</Form.Item>
			<Form.Item 
				name="email" 
				label="Correo electrónico" 
				rules={rules.email}
				hasFeedback
			>
				<Input prefix={<MailOutlined className="text-primary" />}/>
			</Form.Item>
			<Form.Item 
				name="password" 
				label="Contraseña" 
				rules={rules.password}
				hasFeedback
			>
				<Input.Password autoComplete={'off'} prefix={<LockOutlined className="text-primary" />}/>
			</Form.Item>
			<Form.Item 
				name="confirm" 
				label="Confirmar contraseña" 
				rules={rules.confirm}
				hasFeedback
			>
				<Input.Password autoComplete={'off'} prefix={<LockOutlined className="text-primary" />}/>
			</Form.Item>
			<Form.Item>
				<Button type="primary" htmlType="submit" block loading={loading}>
					Continuar
				</Button>
			</Form.Item>
		</div>)
	}

	const StepSecondForm = () => {
		return (
			<div hidden={state===STEP_2 ? false : true}>
				<br></br>
				<Card>

					<Form.Item>
						<h2>¿Qué perfil te gustaría tener en tu cuenta?</h2>

						<Checkbox onChange={() => chooseProfile(TYPE_PROFILE_PERSONAL_TRX)}>Personal</Checkbox> <br></br>
						<Checkbox onChange={() => chooseProfile(TYPE_PROFILE_PROFESSIONAL_TRX)}>Servicios profesionales</Checkbox> <br></br>
						<Checkbox onChange={() => chooseProfile(TYPE_PROFILE_BUSINESS_TRX)}>Empresa</Checkbox> <br></br>
					</Form.Item>
				</Card>
				<br></br>
				
				<div className="d-flex justify-content-between">
					<Button type="link" onClick={goBackStep}>Regresar</Button>
					
					<Form.Item>
						<Button type="primary" onClick={FinishSecondStep} loading={loading}>Continuar</Button>
					</Form.Item>
				</div>
			</div>
		);
	}

	const StepThirdForm = () => {
		return (
			<div hidden={state===STEP_3 ? false : true}>
				<Card>
					<h2>Agredecemos que nos indíques cuánto dinero tienes a tu favor</h2>
					<br></br>
					{
						(valuesform !== undefined && valuesform['personal_profile']) ? (
							<Form.Item
								name="personal_amount"
								label="Monto para perfil de Personal"
								rules={rules.amount}
								hasFeedback
							>
								<InputNumber style={{ width: "75%" }}></InputNumber>
							</Form.Item>
						): null
					}
					{
						(valuesform !== undefined && valuesform['professional_profile']) ? (
							<Form.Item
								name="professional_amount"
								label="Monto para perfil de Servicios profesionales"
								rules={rules.amount}
								hasFeedback
							>
								<InputNumber style={{ width: "75%" }}></InputNumber>
							</Form.Item>
						) : null
					}
					{
						(valuesform !== undefined && valuesform['business_profile']) ? (
							<Form.Item
								name="business_amount"
								label="Monto para perfil de Empresa"
								rules={rules.amount}
								hasFeedback
							>
								<InputNumber style={{ width: "75%" }}></InputNumber>
							</Form.Item>
						) : null
					}
					

					<div className="d-flex justify-content-between">
						<Button type="link" onClick={goBackStep}>Regresar</Button>

						<Form.Item>
							<Button type="primary" htmlType="submit" block>Finalizar registro</Button>
						</Form.Item>
					</div>
				</Card>
			</div>
		);
	}

	const FinishFirstStep = ( values ) => {
		values['personal_profile'] = false;
		values['professional_profile'] = false;
		values['business_profile'] = false;
		setValuesform(values);
		setState(STEP_2);
	}

	const FinishSecondStep = () => {
		if (!valuesform['personal_profile'] && !valuesform['professional_profile'] && !valuesform['business_profile']) {
			messageNotification.warning('Favor elige un perfil para continuar');
			return false;
		}
		valuesform['personal_amount'] = 0;
		valuesform['professional_amount'] = 0;
		valuesform['business_amount'] = 0;
		setState(STEP_3);
	}


	const FinishRegistration = ( values ) => {
		if (valuesform) {
			if (valuesform['personal_profile']) {
				(values.personal_amount > 0 ? 
					valuesform['personal_amount'] = values.personal_amount : 
					messageNotification.warning('Favor digite un monto inicial válido para el perfil personal')
				)
				
			}
			if (values['professional_profile']) {
				(values.professional_amount > 0 ? 
					valuesform['professional_amount'] = values.professional_amount : 
					messageNotification.warning('Favor digite un monto inicial válido para el perfil profesional')
				)
				valuesform['professional_amount'] = values.professional_amount;
			}
			if (values['business_profile']) {
				(values.business_amount > 0 ? 
					valuesform['business_amount'] = values.business_amount : 
					messageNotification.warning('Favor digite un monto inicial válido para el perfil de empresa')
				)
				valuesform['business_amount'] = values.business_amount;
			}
			
			register(valuesform);
		}
		
	}

	const chooseProfile = ( profileType ) => {
		if (valuesform) {
			switch (profileType) {
				case TYPE_PROFILE_PERSONAL_TRX:
					valuesform['personal_profile'] = !valuesform['personal_profile']; 
					break;
	
				case TYPE_PROFILE_PROFESSIONAL_TRX:
					valuesform['professional_profile'] = !valuesform['professional_profile']; 
					break;
	
				case TYPE_PROFILE_BUSINESS_TRX:
					valuesform['business_profile'] = !valuesform['business_profile']; 
					break;
	
				default:
					break;
			}
		}
	}


	const goBackStep = () => {
		switch (state) {

			case STEP_2:
				setState(STEP_1);
				break;

			case STEP_3:
				setState(STEP_2);
				break;
		
			default:
				break;
		}

		valuesform['personal_profile'] = false;
		valuesform['professional_profile'] = false;
		valuesform['business_profile'] = false;
		valuesform['personal_amount'] = 0;
		valuesform['professional_amount'] = 0;
		valuesform['business_amount'] = 0;
	}
	
	return (
		<>
			<Spin spinning={loading}>
				<div className="main">
					<motion.div 
						initial={{ opacity: 0, marginBottom: 0 }} 
						animate={{ 
							opacity: showMessage ? 1 : 0,
							marginBottom: showMessage ? 20 : 0 
						}}> 
						<Alert type="error" showIcon message={message}></Alert>
					</motion.div>
						<Form form={form} layout="vertical" name="register-form" onFinish={FinishFirstStep}>
							<StepFirstForm></StepFirstForm>
						</Form>
						<StepSecondForm></StepSecondForm>

						<Form form={form_info_config} layout="vertical" name="request-info-form" onFinish={FinishRegistration}>
							<StepThirdForm></StepThirdForm>
						</Form>
				</div>
			</Spin>
		</>
	)
}

const mapStateToProps = ({auth}) => {
	const { loading, message, showMessage, redirect } = auth;
	return { loading, message, showMessage, redirect }
}

const mapDispatchToProps = {
	register,
	hideAuthMessage,
	showLoading,
	authenticated
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterForm)
