import React from 'react'
import { Row, Col } from "antd";
import { useSelector } from 'react-redux';

const ErrorNotFoundPage = () => {
	const theme = useSelector(state => state.theme.currentTheme)
	return (
		<div className={`h-100 ${theme === 'light' ? 'bg-white' : ''}`}>
			<div className="container-fluid d-flex flex-column justify-content-between h-100 px-md-4 pb-md-4 pt-md-1">
				<div>
					<img className="img-fluid" src={`/img/${theme === 'light' ? 'logo.png': 'logo-white.png'}`} alt="" />
				</div>
				<div className="container">
					<Row align="middle">
						<Col xs={24} sm={24} md={8}>
							<h1 className="font-weight-bold mb-4 display-4">Página no encontrada</h1>
							<p className="font-size-md mb-4">Nos hemos dado cuenta de que te has perdido, no te preocupes, te ayudaremos a encontrar el camino correcto.</p>
							
						</Col>
						<Col xs={24} sm={24} md={{ span: 14, offset: 2 }}>
							<img className="img-fluid mt-md-0 mt-4" src="/img/others/img-20.png" alt="" />
						</Col>
					</Row>
				</div>
			</div>
		</div>
	)
}

export default ErrorNotFoundPage
